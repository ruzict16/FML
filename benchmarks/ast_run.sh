#!/bin/bash
# valgrind --leak-check=no -s $(readlink -f ./build/fml) "$@"

# MYFML="$(readlink -f ./build/fml)"
# REFML="$(readlink -f /cfml/build/fml)"
MYFML="/dir/fml"
REFML="/dir/cfml"

# cp $REFML /FMLtest/fml

TMPDIR="temporary/fml/"

mkdir -p "$TMPDIR"

ARGS=( "$@" )
POSARGS=()
OPTARGS=()

for (( i=0; i < ${#ARGS[@]}; i++ ))
do
        if [[ "${ARGS[$i]}" == "--"* ]]; then
                OPTARGS+=( "${ARGS[$i]}" )
                (( i++ ))
                OPTARGS+=( "${ARGS[$i]}" )
                continue
        fi
        POSARGS+=( "${ARGS[$i]}" )
done

if [ "${POSARGS[0]}" == "run" ]; then
        "$MYFML" ast_interpret "${POSARGS[@]:1}" "${OPTARGS[@]}"
        # "$REFML" ast_interpret "${POSARGS[@]:1}" "${OPTARGS[@]}"
else
        echo Unknown subcommand "$1"
        echo "Usage: $0 {reference,ast_interpret,bc_interpret,bc_compile,run} <file_path> [<option> <value> ...]"
        exit 2
fi
