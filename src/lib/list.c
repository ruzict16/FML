#include "list.h"

#include <stdio.h>
#include <stdlib.h>

#include "debuglog.h"

list_node_t * new_node(void* data) {
    // printf(" - new_node\n");
    list_node_t * node = malloc(sizeof(list_node_t));
    if (node == NULL) {
        printf("Error: out of memory\n");
        exit(1);
    }
    node->data = data;
    node->prev = NULL;
    node->next = NULL;
    return node;
}

list_t * new_list() {
    // printf("new_list\n");
    list_t * list = malloc(sizeof(list_t));
    if (list == NULL) {
        printf("Error: out of memory\n");
        exit(1);
    }
    list->head = NULL;
    list->tail = NULL;
    list->cursor = NULL;
    list->size = 0;
    list->cursor_pos = 0;
    return list;
}


void add_head(list_t * list, void * data) {
    // printf("add_head\n");
    list_node_t * node = new_node(data);
    if (list->size == 0) {
        list->head = node;
        list->tail = node;
    } else {
        node->next = list->head;
        list->head->prev = node;
        list->head = node;
    }
    list->size++;
}

void add_tail(list_t * list, void * data) {
    // printf("add_tail\n");
    list_node_t * node = new_node(data);
    // printf("add_tail 2\n");
    if (list->size == 0) {
        // printf("add_tail 3\n");
        list->head = node;
        // printf("add_tail 5\n");
        list->tail = node;
        // printf("add_tail 7\n");
    } else {
        // printf("add_tail 4\n");
        node->prev = list->tail;
        // printf("add_tail 6\n");
        list->tail->next = node;
        // printf("add_tail 8\n");
        list->tail = node;
        // printf("add_tail 10\n");
    }
    // printf("add_tail 42\n");
    list->size++;
}

void * remove_head(list_t * list) {
    // printf("remove_head\n");
    if (list->size == 0) {
        printf("Error: list is empty\n");
        exit(1);
    }
    list_node_t * node = list->head;
    void* data = node->data;
    if (list->size == 1) {
        list->head = NULL;
        list->tail = NULL;
    } else {
        list->head = node->next;
        list->head->prev = NULL;
    }
    free(node);
    list->size--;
    return data;
}

void * remove_tail(list_t * list) {
    // printf("remove_tail\n");
    if (list->size == 0) {
        printf("Error: list is empty\n");
        exit(1);
    }
    list_node_t * node = list->tail;
    void* data = node->data;
    if (list->size == 1) {
        list->head = NULL;
        list->tail = NULL;
    } else {
        list->tail = node->prev;
        list->tail->next = NULL;
    }
    free(node);
    list->size--;
    return data;
}

void free_list(list_t * list, void (*free_func)(void*)) {
    // printf("free_list\n");
    while (list->size > 0) {
            void* data = remove_head(list);
            (*free_func)(data);
    }
    free(list);
}

void cursor_ensure_init(list_t * list) {
    // printf("cursor_ensure_init\n");
    if (list->size == 0) {
        list->cursor = NULL;
        list->cursor_pos = 0;
        return;
    }
    if (list->cursor == NULL) {
        list->cursor = list->head;
        list->cursor_pos = 0;
    }
}

void cursor_to_head(list_t * list) {
    // printf("cursor_to_head\n");
    list->cursor = list->head;
    list->cursor_pos = 0;
}

void cursor_to_tail(list_t * list) {
    // printf("cursor_to_tail\n");
    list->cursor = list->tail;
    if (list->size == 0) {
        list->cursor_pos = 0;
    }
    else {
        list->cursor_pos = list->size - 1;
    }
}

void cursor_fwd(list_t * list) {
    // printf("cursor_fwd\n");
    cursor_ensure_init(list);
    if (list->cursor == NULL) return;
    list->cursor = list->cursor->next;
    (list->cursor_pos)++;
}

void cursor_rwd(list_t * list) {
    // printf("cursor_rwd\n");
    cursor_ensure_init(list);
    if (list->cursor == NULL) return;
    list->cursor = list->cursor->prev;
    (list->cursor_pos)--;
}

uint64_t distance(uint64_t a, uint64_t b) {
    // printf("distance\n");
    return (a > b) ? (a - b) : (b - a);
}

void cursor_move(list_t * list, int64_t difference) {
    // printf("cursor_move\n");
    // printf(" - cursor_move begin: %ld + (%ld)\n", list->cursor_pos, difference);
    cursor_ensure_init(list);
    if (list->cursor == NULL) return;
    if (difference == 0) return;
    int forward = (difference > 0);
    int direction = (forward << 1) - 1;
    int dist = direction * difference;
    // printf(" - cursor_move distance: %d\n", dist);
    void (*step)(list_t *) = forward ? cursor_fwd : cursor_rwd;

    while (dist > 0) {
        step(list);
        dist--;
    }
}

void cursor_seek(list_t * list, uint64_t i) {
    // printf("cursor_seek\n");
    cursor_ensure_init(list);
    // printf("cursor_seek begin: %ld\n", i);
    if (list->size == 0) return;
    // printf("cursor_seek passed size\n");
    if (list->cursor_pos == i) return;
    // printf("cursor_seek passed position\n");
    uint64_t diff_h = i;
    uint64_t diff_t = distance((list->size) - 1, i);
    uint64_t diff_c = distance(list->cursor_pos, i);

    if (diff_t < diff_h && diff_t < diff_c) {
        cursor_to_tail(list);
    }
    else if (diff_h < diff_c && diff_h < diff_t) {
        cursor_to_head(list);
    }

    cursor_move(list, i - list->cursor_pos);
}

void * cursor_data(list_t * list) {
    if (list->cursor == NULL) {
        // debug("no cursor data\n");
        return NULL;
    };
    return list->cursor->data;
}