#pragma once

#include <stdint.h>

#include "str.h"

typedef uint64_t string_id;

void init_strids();
void free_strids();

string_id get_strid(Str name);
string_id get_strid_c(const char * name);
Str get_str(string_id id);
char * get_cstr(string_id id);

void bake();