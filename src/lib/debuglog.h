#pragma once

#include <stdio.h>
#include "feature_flags.h"

#ifdef DEBUGLOG
#define debug(...) fprintf (stderr, __VA_ARGS__)
#else
#define debug(...)
#endif

#define eprintf(...) fprintf (stderr, __VA_ARGS__)

#define mark(x) debug(x "\n");

#ifdef WARNING_AS_EXIT
#define warn(...) eprintf(__VA_ARGS__); exit(40)
#else
#define warn(...) debug(__VA_ARGS__)
#endif

