// Str structure
// Michal Vlasák, FIT CTU, 2023

#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

// `Str` is a pair of pointer and length and represents an immutable string. It
// can be constructed from C string literals with the below `STR` macro.
typedef struct {
	const uint8_t *str;
	size_t len;
} Str;
#define STR(lit) (Str) { .str = (const uint8_t *) lit, .len = sizeof(lit) - 1 }

#define Str_from_lit(lit) (Str) { .str = (const uint8_t *) lit, .len = sizeof(lit) - 1 }
#define Str_from_cstr(cstr) (Str) { .str = (const uint8_t *) cstr, .len = strlen(cstr) }

// Compare equality of strings, returns true if equal, false otherwise.
bool str_eq(Str a, Str b);

// Lexicographically compare two strings. Returns:
//  - 0 if the strings are equal,
//  - a negative number if the first string is less than the second
//  - a positive number if the first string is greater than the second
int str_cmp(Str a, Str b);

