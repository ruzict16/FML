#include "string_id.h"

#include <string.h>
#include <stdio.h>

#include "list.h"
#include "str.h"

list_t * strids = NULL;

void init_strids() {
        // printf("init_strids\n");
        strids = new_list();
}

void free_strids() {
        // printf("free_strids\n");
        free_list(strids, free);
}

string_id get_strid(Str name) {
        // printf("get_strid %.*s\n", (int)name.len, (char *)name.str);
        uint8_t * cur;
        for (cursor_to_tail(strids); (cur = cursor_data(strids)); cursor_rwd(strids)) {
                // printf("get_strid lookup\n");
                Str found = Str_from_cstr((char *)cur);
                if(str_eq(name, found)) {
                        // printf("get_strid found\n");
                        return strids->cursor_pos;
                }
        }
        // printf("get_strid not found\n");
        uint8_t * data = malloc(name.len + 1);
        strncpy((char *)data, (char *)name.str, name.len);
        data[name.len] = 0;
        add_tail(strids, data);
        // printf("get_strid created\n");
        return strids->size - 1;
}

string_id get_strid_c(const char *name)
{
        return get_strid(Str_from_cstr(name));
}

Str get_str(string_id id) {
        // printf("get_str %ld\n", id);
        cursor_seek(strids, id);
        uint8_t * str = cursor_data(strids);
        return Str_from_cstr((char *)str);
}

char * get_cstr(string_id id)
{
        // printf("get_str %ld\n", id);
        cursor_seek(strids, id);
        return cursor_data(strids);
}

void bake() {
        
}