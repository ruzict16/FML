#include "load_file.h"

#include "common.h"
#include <stdio.h>

#include "debuglog.h"

char * load_file(const char *file)
{
        FILE* fp = fopen(file, "r");
        if (fp == NULL) {
                eprintf("Failed to open file\n");
                return NULL;
        }

        fseek(fp, 0, SEEK_END);
        long size = ftell(fp);
        #ifdef ENABLE_CHECKERS
                if (size < 0) {
                        eprintf("ftell returned negative number\n");
                        exit(6);
                }
        #endif
        fseek(fp, 0, SEEK_SET);

        char* buffer = (char*) malloc(size + 1);
        size_t read_bytes = fread(buffer, size, 1, fp);
        #ifdef ENABLE_CHECKERS
                if (read_bytes != (size_t)size) {
                        eprintf("Could not read the expected ammount of bytes\n");
                        exit(7);
                }
        #else
                is_used(read_bytes);
        #endif
        buffer[size] = '\0';

        fclose(fp);

        return buffer;
}
