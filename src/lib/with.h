#pragma once

#define macro_concat_(a,b) a ## b
#define macro_concat(a,b) macro_concat_(a,b)
#define macro_var(name) macro_concat( name, __LINE__)

#define with(start,end) for ( int macro_var(_i_) = (start,0) ; ! macro_var(_i_) ; ( macro_var(_i_) += 1, end) )
#define exit_with(x) x; continue
