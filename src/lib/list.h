#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

/* Doubly linked list node */
typedef struct list_node_t {
    void * data;
    struct list_node_t * prev;
    struct list_node_t * next;
} list_node_t;

/* Doubly linked list */
typedef struct {
    list_node_t * head;
    list_node_t * tail;
    list_node_t * cursor;
    uint64_t size;
    uint64_t cursor_pos;
} list_t;

/* Function to create a new node */
// list_node_t * new_node(void* data);

/* Function to initialize a new list */
list_t * new_list();
/* Function to add a new node to the head of the list */
void add_head(list_t * list, void * data);
/* Function to add a new node to the tail of the list */
void add_tail(list_t * list, void * data);
/* Function to remove a node from the head of the list */
void* remove_head(list_t * list);
/* Function to remove a node from the tail of the list */
void* remove_tail(list_t * list);
/* Function to free a list, and each data field with a custom function */
void free_list(list_t * list, void (*free_func)(void*));

void cursor_to_head(list_t * list);
void cursor_to_tail(list_t * list);
void cursor_fwd(list_t * list);
void cursor_rwd(list_t * list);
void cursor_seek(list_t * list, uint64_t i);
void * cursor_data(list_t * list);
