#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

void free_string(void  * data) {
        free(data);
}

void free_once_nested_list(void * data) {
        free_list(data, free_string);
}

int main() {
        list_t * list = new_list();
        list_t * list2 = new_list();
        add_head(list2, strdup("one-one"));
        add_head(list2, strdup("one-two"));
        add_tail(list2, strdup("one-three"));
        add_tail(list, list2);

        list2 = new_list();
        add_head(list2, strdup("two-one"));
        add_head(list2, strdup("two-two"));
        add_tail(list2, strdup("two-three"));
        add_tail(list, list2);

        list2 = new_list();
        add_head(list2, strdup("three-one"));
        add_head(list2, strdup("three-two"));
        add_tail(list2, strdup("three-three"));
        add_tail(list, list2);

        free_list(list, free_once_nested_list);
}

