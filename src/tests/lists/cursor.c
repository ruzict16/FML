#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "list.h"

void free_string(void  * data) {
        // printf("List string\n");
        free(data);
}

int main() {
        list_t * list = new_list();

        add_head(list, strdup("one-one"));
        add_head(list, strdup("one-two"));
        add_tail(list, strdup("one-three"));
        add_head(list, strdup("two-one"));
        add_head(list, strdup("two-two"));
        add_tail(list, strdup("two-three"));
        add_head(list, strdup("three-one"));
        add_head(list, strdup("three-two"));
        add_tail(list, strdup("three-three"));

        const char * expected[] = {
                "three-two",
                "three-one",
                "two-two",
                "two-one",
                "one-two",
                "one-one",
                "one-three",
                "two-three",
                "three-three"
        };

        size_t i = 0;
        const char * str = NULL;
        for (cursor_to_head(list); (str = cursor_data(list)); cursor_fwd(list)) {
                printf("%s == %s\n", str, expected[i]);
                if(0 != strcmp(expected[i++], str)) {
                        return 1;
                }
        }
        for (cursor_to_tail(list); (str = cursor_data(list)); cursor_rwd(list)) {
                printf("%s == %s\n", str, expected[--i]);
                if(0 != strcmp(expected[i], str)) {
                        return 1;
                }
        }


        free_list(list, free_string);
}

