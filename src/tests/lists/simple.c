#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

void free_string(void  * data) {
        free(data);
}

void free_once_nested_list(void * data) {
        free_list(data, free_string);
}

int main() {
        list_t * list = new_list();
        add_head(list, strdup("one"));
        add_head(list, strdup("two"));
        add_tail(list, strdup("three"));

        free_list(list, free_string);
}

