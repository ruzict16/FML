#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

void nofree(void * _) {
        (void)_; // [[maybe_unused]]
}

int main() {
        list_t * list = new_list();

        const size_t is_length = 15;
        uint64_t is[15] = {
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                3, 1, 4, 8, 2
        };
        char * strs[10] = {
                "0", "1", "2", "3", "4",
                "5", "6", "7", "8", "9"
        };

        add_tail(list, strs[0]);
        add_tail(list, strs[1]);
        add_tail(list, strs[2]);
        add_tail(list, strs[3]);
        add_tail(list, strs[4]);
        add_tail(list, strs[5]);
        add_tail(list, strs[6]);
        add_tail(list, strs[7]);
        add_tail(list, strs[8]);
        add_tail(list, strs[9]);

        size_t ii = 0;
        for (ii = 0; ii < is_length; ii++) {
                size_t i = is[ii];
                cursor_seek(list, i);
                const char *str = cursor_data(list);
                if (str != strs[i]) {
                        printf("Expected %s, got: %s, at position %ld\n", strs[i], str, i);
                        return 1;
                }
        }

        free_list(list, nofree);
}

