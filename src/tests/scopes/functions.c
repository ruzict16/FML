#include <stdio.h>

#include "ast_interpret/scopes.h"
#include "ast_interpret/heap.h"
#include "string_id.h"

int main() {
        ast_init_heap();
        init_strids();

        string_id a = get_strid_c("a");
        string_id b = get_strid_c("b");
        string_id c = get_strid_c("c");
        string_id d = get_strid_c("d");

        value_t * va = ast_alloc_integer();
        ((integer_t *)va)->value = 1;

        value_t * vb = ast_alloc_integer();
        ((integer_t *)vb)->value = 2;

        value_t * vc = ast_alloc_integer();
        ((integer_t *)vc)->value = 3;

        value_t * vd = ast_alloc_integer();
        ((integer_t *)vd)->value = 4;

        init_scopes();

        new_var(a, va);
        new_var(b, vb);

        new_scope();

        new_var(c, vc);
        new_var(d, vd);

        new_function_scope();

        if(get_var(a) != va) {return 1;}
        if(get_var(b) != vb) {return 1;}
        if(get_var(c) != NULL) {return 1;}
        if(get_var(d) != NULL) {return 1;}

        new_var(c, vd);

        if(get_var(c) != vd) {return 1;}

        exit_scope();

        new_function_scope();

        if(get_var(a) != va) {return 1;}
        if(get_var(b) != vb) {return 1;}
        if(get_var(c) != NULL) {return 1;}
        if(get_var(d) != NULL) {return 1;}

        new_var(c, vc);

        if(get_var(c) != vc) {return 1;}

        exit_scope();

        if(get_var(a) != va) {return 1;}
        if(get_var(b) != vb) {return 1;}
        if(get_var(c) != vc) {return 1;}
        if(get_var(d) != vd) {return 1;}

        new_var(a, vb);

        if(get_var(a) != vb) {return 1;}
        if(get_var(b) != vb) {return 1;}
        if(get_var(c) != vc) {return 1;}
        if(get_var(d) != vd) {return 1;}

        new_scope();

        if(get_var(a) != vb) {return 1;}
        if(get_var(b) != vb) {return 1;}
        if(get_var(c) != vc) {return 1;}
        if(get_var(d) != vd) {return 1;}

        new_var(b, va);
        set_var(d, va);

        if(get_var(a) != vb) {return 1;}
        if(get_var(b) != va) {return 1;}
        if(get_var(c) != vc) {return 1;}
        if(get_var(d) != va) {return 1;}

        exit_scope();

        if(get_var(a) != vb) {return 1;}
        if(get_var(b) != vb) {return 1;}
        if(get_var(c) != vc) {return 1;}
        if(get_var(d) != va) {return 1;}

        free_scopes();
        free_strids();
        ast_free_heap();
}