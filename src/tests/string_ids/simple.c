#include <stdio.h>

#include "str.h"
#include "string_id.h"

#define ASSERT_EQ(a,b) if (a == b) {} else {\
printf("failed ASSERT_EQ(%s, %s) aka %ld == %ld\n", #a, #b, a, b); \
return 1;}

#define ASSERT_STRCMP(a,b) if (0 == str_cmp(a, b)) {} else {\
printf("failed ASSERT_STRCMP(%s, %s) aka \"%s\" == %s\n", #a, #b, a.str, b.str); \
return 1;}

#define ASSERT_CSTRCMP(a,b) if (0 == strcmp(a, b)) {} else {\
printf("failed ASSERT_STRCMP(%s, %s) aka \"%s\" == %s\n", #a, #b, a, b); \
return 1;}

int main() {
        init_strids();

        string_id a = get_strid_c("a");
        string_id b = get_strid_c("b");
        string_id c = get_strid_c("c");

        string_id d = get_strid(Str_from_cstr("d"));
        string_id e = get_strid(Str_from_cstr("e"));
        string_id f = get_strid(Str_from_cstr("f"));

        string_id aa = get_strid_c("a");
        string_id bb = get_strid_c("b");
        string_id cc = get_strid_c("c");

        string_id dd = get_strid(Str_from_cstr("d"));
        string_id ee = get_strid(Str_from_cstr("e"));
        string_id ff = get_strid(Str_from_cstr("f"));

        ASSERT_EQ(a, aa);
        ASSERT_EQ(b, bb);
        ASSERT_EQ(c, cc);
        ASSERT_EQ(d, dd);
        ASSERT_EQ(e, ee);
        ASSERT_EQ(f, ff);

        ASSERT_STRCMP(Str_from_cstr("a"), get_str(a));
        ASSERT_STRCMP(Str_from_cstr("a"), get_str(aa));
        ASSERT_STRCMP(Str_from_cstr("b"), get_str(b));
        ASSERT_STRCMP(Str_from_cstr("b"), get_str(bb));
        ASSERT_STRCMP(Str_from_cstr("c"), get_str(c));
        ASSERT_STRCMP(Str_from_cstr("c"), get_str(cc));
        ASSERT_STRCMP(Str_from_cstr("d"), get_str(d));
        ASSERT_STRCMP(Str_from_cstr("d"), get_str(dd));
        ASSERT_STRCMP(Str_from_cstr("e"), get_str(e));
        ASSERT_STRCMP(Str_from_cstr("e"), get_str(ee));
        ASSERT_STRCMP(Str_from_cstr("f"), get_str(f));
        ASSERT_STRCMP(Str_from_cstr("f"), get_str(ff));

        ASSERT_CSTRCMP("a", get_cstr(a));
        ASSERT_CSTRCMP("a", get_cstr(aa));
        ASSERT_CSTRCMP("b", get_cstr(b));
        ASSERT_CSTRCMP("b", get_cstr(bb));
        ASSERT_CSTRCMP("c", get_cstr(c));
        ASSERT_CSTRCMP("c", get_cstr(cc));
        ASSERT_CSTRCMP("d", get_cstr(d));
        ASSERT_CSTRCMP("d", get_cstr(dd));
        ASSERT_CSTRCMP("e", get_cstr(e));
        ASSERT_CSTRCMP("e", get_cstr(ee));
        ASSERT_CSTRCMP("f", get_cstr(f));
        ASSERT_CSTRCMP("f", get_cstr(ff));

        free_strids();
}