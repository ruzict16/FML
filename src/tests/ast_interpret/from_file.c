#include "ast_interpret/ast_interpret.h"

#include <stdio.h>
#include <string.h>
#define MAX 1000

#include "debuglog.h"

int compare_files(char * f_expected, char * f_actual) {
        char c1, c2;
        char s1[MAX], s2[MAX];
        char *p1;
        char *p2;
        FILE *fp1;
        FILE *fp2;
        p1 = s1;
        p2 = s2;
        fp1 = fopen(f_expected, "r");
        fp2 = fopen(f_actual, "r");
        if (fp1 == NULL || fp2 == NULL) {
                printf("One or both of the files can't be used \n ");
                return -1;
        }
        c1 = getc(fp1);
        c2 = getc(fp2);
        while ((c1 != EOF) && (c2 != EOF)) {
                for (; c1 != '\n'; p1++) {
                        *p1 = c1;
                        c1 = getc(fp1);
                }
                *p1 = '\0';

                for (; c2 != '\n'; p2++) {
                        *p2 = c2;
                        c2 = getc(fp2);
                }
                *p2 = '\0';
                if ((strcmp(s1, s2)) != 0) {
                        printf("%s\n", s1);
                        printf("%s\n", s2);
                        return 1;
                }
                c1 = getc(fp1);
                c2 = getc(fp2);
                p1 = s1;
                p2 = s2;
        }
        if (c1 != EOF || c2 != EOF) {
                printf("One of the files ended prematurely\n");
                return 1;
        }
        return 0;
}

int main(int argc, char **argv) {
        (void)argc;
        char prg[201];
        prg[200] = '\0';
        char exp[201];
        exp[200] = '\0';
        char out[201];
        out[200] = '\0';
        snprintf(prg, 200, "%s%s.fml", argv[1], argv[2]);
        snprintf(exp, 200, "%s%s.txt", argv[1], argv[2]);
        snprintf(out, 200, "%s%s.out", argv[1], argv[2]);
        FILE* fp = fopen(out, "w");
        if (fp == NULL) {
                eprintf("Failed to open file\n");
                return 1;
        }
        int x = ast_interpret(prg, fp);
        if (x != 0) {
                return 1;
        }
        fclose(fp);
        return compare_files(exp, out);
}
