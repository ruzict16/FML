#include <stdio.h>

#include "ast_interpret/ast_interpret.h"
#include "parser.h"
#include "ast_interpret/runtime_types.h"
#include "str.h"
#include "ast_interpret/scopes.h"

int main() {
        Str aname = Str_from_cstr("a");
        Str aaname = Str_from_cstr("a");
        Str aaaname = Str_from_cstr("a");
        Str bname = Str_from_cstr("b");
        Str bbname = Str_from_cstr("b");
        Str bbbname = Str_from_cstr("b");

        AstNull nval = (AstNull) {
                .base = (Ast) { .kind = AST_NULL }
        };
        AstInteger aval = (AstInteger) {
                .base = (Ast) { .kind = AST_INTEGER },
                .value = 5
        };
        AstInteger bval = (AstInteger) {
                .base = (Ast) { .kind = AST_INTEGER },
                .value = 10
        };

        AstDefinition def_a = (AstDefinition) {
                .base = (Ast) { .kind = AST_DEFINITION },
                .name = aname,
                .value = (Ast *)&aval
        };
        AstDefinition def_b = (AstDefinition) {
                .base = (Ast) { .kind = AST_DEFINITION },
                .name = bname,
                .value = (Ast *)&nval
        };

        AstDefinition def_nest_a = (AstDefinition) {
                .base = (Ast) { .kind = AST_DEFINITION },
                .name = aaname,
                .value = (Ast *)&bval
        };

        AstVariableAccess acc_a = (AstVariableAccess) {
                .base = (Ast) { .kind = AST_VARIABLE_ACCESS },
                .name = aaaname
        };

        AstVariableAssignment assign_nest_b = (AstVariableAssignment) {
                .base = (Ast) { .kind = AST_VARIABLE_ASSIGNMENT },
                .name = bbname,
                .value = (Ast *)&acc_a
        };

        AstBlock block = (AstBlock) {
                .base = (Ast) { .kind = AST_BLOCK },
                .expression_cnt = 2,
                .expressions = (Ast * []) {
                        (Ast *)&def_nest_a,
                        (Ast *)&assign_nest_b
                }
        };
        AstVariableAccess acc_b = (AstVariableAccess) {
                .base = (Ast) { .kind = AST_VARIABLE_ACCESS },
                .name = bbbname
        };

        /*
                let a = 5;
                let b = NULL;
                begin
                        let a = 10;
                        b <- a;
                end;
                b
        */
        AstTop prg_a = (AstTop) {
                .base = (Ast) { .kind = AST_TOP },
                .expression_cnt = 4,
                .expressions = (Ast * []) {
                        (Ast *)&def_a,
                        (Ast *)&def_b,
                        (Ast *)&block,
                        (Ast *)&acc_b
                }
        };
        init_strids();
        init_scopes();
        value_t * result_a = do_interpret((Ast *)&prg_a);
        if (result_a->kind != integer_kind) {printf("1: Not integer kind"); return 1;}
        if (((integer_t *)result_a)->value != 10) {printf("1: Not right integer: %ld", ((integer_t *)result_a)->value); return 1;}
        free_scopes();
        free_strids();
        /*
                let a = 5;
                let b = NULL;
                begin
                        let a = 10;
                        b <- a;
                end;
                a
        */
        AstTop prg_b = (AstTop) {
                .base = (Ast) { .kind = AST_TOP },
                .expression_cnt = 4,
                .expressions = (Ast * []) {
                        (Ast *)&def_a,
                        (Ast *)&def_b,
                        (Ast *)&block,
                        (Ast *)&acc_a
                }
        };
        init_strids();
        init_scopes();
        value_t * result_b = do_interpret((Ast *)&prg_b);
        if (result_b->kind != integer_kind) {printf("2: Not integer kind"); return 1;}
        if (((integer_t *)result_b)->value != 5) {printf("2: Not right integer: %ld", ((integer_t *)result_b)->value); return 1;}
        free_scopes();
        free_strids();
}