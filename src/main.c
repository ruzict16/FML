#include <stdio.h>
#include <string.h>

#include "debuglog.h"

#include "ast_interpret/ast_interpret.h"
#include "bc_interpret/bc_interpret.h"
#include "bc_compile/bc_compile.h"

void usage(FILE * stream) {
	fprintf(stream, "usage: fml {ast_interpret|bc_interpret|bc_compile} <source_file>");
}

char* replace_char(char* str, char find, char replace){
    char *current_pos = strchr(str,find);
    while (current_pos) {
        *current_pos = replace;
        current_pos = strchr(current_pos,find);
    }
    return str;
}

int main(int argc, char **argv) {
	if (argc < 3) {
		eprintf("Error: expected at least two arguments\n");
		usage(stderr);
		return 1;
	}

	char * cmd = argv[1];
	char * src = argv[2];

	// uint64_t heap_size = 2147483648;
	uint64_t heap_size = 1024*8;
	char * gclogs_default_dir = "./artifacts/gclog/";
	int gclogs_default_dir_len = strlen(gclogs_default_dir);
	int src_len = strlen(src);

	char * gclogs = alloca(gclogs_default_dir_len + src_len + 1);
	gclogs[0] = 0; // LOL, thanks valgrind, shame you don't help with my GC propblems just as much :-D
	strcat(gclogs, gclogs_default_dir);
	strcat(gclogs, src);
	replace_char(gclogs + gclogs_default_dir_len, '/', '_');

	if (argc > 3) {
		int i;
		for (i = 3; i < argc - 1; i += 2) {
			if (0 == strcmp(argv[i], "--heap-size")) {
				heap_size = atoi(argv[i+1]) * 1024 * 1024;
			}
			else if (0 == strcmp(argv[i], "--heap-log")) {
				gclogs = argv[i+1];
			}
		}
	}

	if (0 == strcmp(cmd, "ast_interpret")) {
		return ast_interpret(src, stdout);
	}
	else if (0 == strcmp(cmd, "bc_interpret")) {
		return bc_interpret(src, heap_size, gclogs);
	}
	else if (0 == strcmp(cmd, "bc_compile")) {
		return bc_compile(src);
	}
	else if (0 == strcmp(cmd, "help") || 0 == strcmp(cmd, "--help") || 0 == strcmp(cmd, "-h")) {
		usage(stdout);
		return 0;
	}
	else {
		eprintf("Error: unknown command %s\n", cmd);
		usage(stderr);
		return 1;
	}
}
