#pragma once

#include "string_id.h"
#include "runtime_types.h"

void init_scopes();
void free_scopes();

void new_scope();
void new_function_scope();
void exit_scope();

void set_var(string_id id, value_t * val);
void new_var(string_id id, value_t * val);
value_t * get_var(string_id id);

