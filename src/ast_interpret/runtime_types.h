#pragma once

#include <stdint.h>
#include <stdlib.h>

#include "../lib/parser.h"

#include "string_id.h"

struct value_s;

enum value_kind {
        null_kind,
        integer_kind,
        boolean_kind,
        array_kind,
        object_kind,
        function_kind,
};

typedef struct {
        enum value_kind kind;
} value_t;
typedef struct {
        value_t base;
} null_t;
typedef struct {
        value_t base;
        int64_t value;
} integer_t;
typedef struct {
        value_t base;
        bool value;
} boolean_t;
typedef struct {
        value_t base;
        AstFunction * ast;
} function_t;
typedef struct field_t {
        string_id name;
        value_t * value;
} field_t;
typedef struct {
        value_t base;
        value_t * parent;
        uint64_t fields_cnt;
        field_t fields[];
} object_t;
typedef struct {
        value_t base;
        uint64_t size;
        value_t * data[];
} array_t;

value_t ** lookup_field(value_t ** object, string_id field);

void print_value(value_t * v);
void print_kind(enum value_kind kind);