#pragma once

#include <stdio.h>

#include "ast_interpret/runtime_types.h"

value_t * do_interpret(Ast * ast);
int ast_interpret(const char * src, FILE * printstream);