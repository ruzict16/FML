#include "builtin_functions.h"

#include "heap.h"

value_t *do_builtin_call(value_t *self, string_id name, size_t arg_cnt, value_t **args)
{
        (void)arg_cnt;
        debug("builtin call: %s\n", get_cstr(name));

        const string_id eq = get_strid_c("==");
        const string_id neq = get_strid_c("!=");
        const string_id bool_and = get_strid_c("&");
        const string_id bool_or = get_strid_c("|");
        const string_id plus = get_strid_c("+");
        const string_id minus = get_strid_c("-");
        const string_id times = get_strid_c("*");
        const string_id div = get_strid_c("/");
        const string_id mod = get_strid_c("%");
        const string_id lt = get_strid_c("<");
        const string_id leq = get_strid_c("<=");
        const string_id gt = get_strid_c(">");
        const string_id geq = get_strid_c(">=");
        const string_id get = get_strid_c("get");
        const string_id set = get_strid_c("set");

        bool is_comparable = true;
        bool is_int = self->kind == integer_kind;
        bool is_bool = self->kind == boolean_kind;
        bool is_array = self->kind == array_kind;

        if (is_comparable && name == eq) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_eq(self, (args[0]));
        }
        if (is_comparable && name == neq) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_neq(self, (args[0]));
        }
        if (is_bool && name == bool_and) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_bool_and((boolean_t *)self, (boolean_t *)(args[0]));
        }
        if (is_bool && name == bool_or) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_bool_or((boolean_t *)self, (boolean_t *)(args[0]));
        }
        if (is_int && name == plus) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_plus((integer_t *)self, (integer_t *)(args[0]));
        }
        if (is_int && name == minus) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_minus((integer_t *)self, (integer_t *)(args[0]));
        }
        if (is_int && name == times) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_times((integer_t *)self, (integer_t *)(args[0]));
        }
        if (is_int && name == div) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_div((integer_t *)self, (integer_t *)(args[0]));
        }
        if (is_int && name == mod) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_mod((integer_t *)self, (integer_t *)(args[0]));
        }
        if (is_int && name == lt) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_lt((integer_t *)self, (integer_t *)(args[0]));
        }
        if (is_int && name == leq) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_leq((integer_t *)self, (integer_t *)(args[0]));
        }
        if (is_int && name == gt) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_gt((integer_t *)self, (integer_t *)(args[0]));
        }
        if (is_int && name == geq) {
                // TODO: check arg cnt and arg type
                return (value_t *) fn_geq((integer_t *)self, (integer_t *)(args[0]));
        }

        if (is_array && name == get) {
                // TODO: check arg cnt and arg type
                if (arg_cnt < 1) {
                        eprintf("arg_cnt < 1\n");
                        exit(3);
                }
                return fn_get((array_t *)self, (integer_t *)(args[0]));
        }

        if (is_array && name == set) {
                // TODO: check arg cnt and arg type
                return fn_set((array_t *)self, (integer_t *)(args[0]), (args[1]));
        }

        debug("builtin call failed: %s\n", get_cstr(name));
        return NULL;
}

boolean_t * fn_eq(value_t * a, value_t * b) {
        // debug("fn_eq begin:\n");
        boolean_t * res = (boolean_t *)ast_alloc_boolean();

        // debug(" - fn_eq kinds:\n");
        // debug(" -> ");
        // print_kind(a->kind);
        // debug(" -> ");
        // print_kind(b->kind);

        if (a == b)               { goto res_true;  }
        if (a->kind != b->kind)   { goto res_false; }
        if (a->kind == null_kind) { goto res_true;  }
        if (a->kind == boolean_kind) {
                debug(" - fn_eq bool: %d == %d\n", ((boolean_t *)a)->value, ((boolean_t *)b)->value);
                if (((boolean_t *)a)->value == ((boolean_t *)b)->value) {
                        goto res_true;
                }
                goto res_false;
        }
        if (a->kind == integer_kind) {
                debug(" - fn_eq int: %ld == %ld\n", ((integer_t *)a)->value, ((integer_t *)b)->value);
                if (((integer_t *)a)->value == ((integer_t *)b)->value) {
                        goto res_true;
                }
                goto res_false;
        }

        res_false:
        // debug("fn_eq false\n");
        res->value = false;
        goto ret;

        res_true:
        // debug("fn_eq true\n");
        res->value = true;

        ret:
        // debug("fn_eq done\n");
        return res;
}

boolean_t * fn_neq(value_t * a, value_t * b) {
        debug("fn_neq\n");
        boolean_t * res = (boolean_t *)fn_eq(a, b);
        res->value = !res->value;
        debug("fn_neq done\n");
        return res;
}



boolean_t * fn_bool_and(boolean_t * a, boolean_t * b) {
        debug("fn_bool_and\n");
        boolean_t * res = (boolean_t *)ast_alloc_boolean();
        res->value = a->value && b->value;
        debug("fn_bool_and done\n");
        return res;
}
boolean_t * fn_bool_or(boolean_t * a, boolean_t * b) {
        debug("fn_bool_or\n");
        boolean_t * res = (boolean_t *)ast_alloc_boolean();
        res->value = a->value || b->value;
        debug("fn_bool_or done\n");
        return res;
}



integer_t * fn_plus(integer_t * a, integer_t * b) {
        debug("fn_plus\n");
        integer_t * res = (integer_t *)ast_alloc_integer();
        res->value = a->value + b->value;
        debug("fn_plus done\n");
        return res;
}
integer_t * fn_minus(integer_t * a, integer_t * b) {
        debug("fn_minus\n");
        integer_t * res = (integer_t *)ast_alloc_integer();
        res->value = a->value - b->value;
        debug("fn_minus done\n");
        return res;
}
integer_t * fn_times(integer_t * a, integer_t * b) {
        debug("fn_times\n");
        integer_t * res = (integer_t *)ast_alloc_integer();
        res->value = a->value * b->value;
        debug("fn_times done\n");
        return res;
}
integer_t * fn_div(integer_t * a, integer_t * b) {
        debug("fn_div\n");
        integer_t * res = (integer_t *)ast_alloc_integer();
        res->value = a->value / b->value;
        debug("fn_div done\n");
        return res;
}
integer_t * fn_mod(integer_t * a, integer_t * b) {
        debug("fn_mod\n");
        integer_t * res = (integer_t *)ast_alloc_integer();
        res->value = a->value % b->value;
        debug("fn_mod done\n");
        return res;
}
boolean_t * fn_lt(integer_t * a, integer_t * b) {
        debug("fn_lt\n");
        boolean_t * res = (boolean_t *)ast_alloc_boolean();
        res->value = a->value < b->value;
        debug("fn_lt done\n");
        return res;
}
boolean_t * fn_leq(integer_t * a, integer_t * b) {
        debug("fn_leq\n");
        boolean_t * res = (boolean_t *)ast_alloc_boolean();
        res->value = a->value <= b->value;
        debug("fn_leq done\n");
        return res;
}
boolean_t * fn_gt(integer_t * a, integer_t * b) {
        debug("fn_gt\n");
        boolean_t * res = (boolean_t *)ast_alloc_boolean();
        res->value = a->value > b->value;
        debug("fn_gt done\n");
        return res;
}
boolean_t * fn_geq(integer_t * a, integer_t * b) {
        debug("fn_geq\n");
        boolean_t * res = (boolean_t *)ast_alloc_boolean();
        res->value = a->value >= b->value;
        debug("fn_geq done\n");
        return res;
}

value_t * fn_get(array_t * a, integer_t * i) {
        debug("fn_get\n");
        // TODO: check bounds
        if (a == NULL) {
                debug("fn_get: a NULL\n");
                exit(32);
        }
        if (i == NULL) {
                debug("fn_get: i NULL\n");
                exit(32);
        }
        if ((int64_t)a->size <= i->value) {
                debug("fn_get: bounds: %ld\n", i->value);
                exit(32);
        }
        value_t * res = a->data[i->value];
        return res;
}
value_t * fn_set(array_t * a, integer_t * i, value_t * v) {
        debug("fn_set\n");
        // TODO: check bounds
        if (a == NULL) {
                debug("fn_set: a NULL\n");
                exit(32);
        }
        if (i == NULL) {
                debug("fn_set: i NULL\n");
                exit(32);
        }
        if ((int64_t)a->size <= i->value) {
                debug("fn_set: bounds: %ld\n", i->value);
                exit(32);
        }
        a->data[i->value] = v;
        value_t * res = a->data[i->value];
        return res;
}