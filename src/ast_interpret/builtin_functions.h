#pragma once

#include "runtime_types.h"

#include "debuglog.h"

boolean_t * fn_eq(value_t * a, value_t * b);
boolean_t * fn_neq(value_t * a, value_t * b);
boolean_t * fn_bool_and(boolean_t * a, boolean_t * b);
boolean_t * fn_bool_or(boolean_t * a, boolean_t * b);
integer_t * fn_plus(integer_t * a, integer_t * b);
integer_t * fn_minus(integer_t * a, integer_t * b);
integer_t * fn_times(integer_t * a, integer_t * b);
integer_t * fn_div(integer_t * a, integer_t * b);
integer_t * fn_mod(integer_t * a, integer_t * b);
boolean_t * fn_lt(integer_t * a, integer_t * b);
boolean_t * fn_leq(integer_t * a, integer_t * b);
boolean_t * fn_gt(integer_t * a, integer_t * b);
boolean_t * fn_geq(integer_t * a, integer_t * b);

value_t * fn_get(array_t * a, integer_t * i);
value_t * fn_set(array_t * a, integer_t * i, value_t * v);

value_t * do_builtin_call(value_t * self, string_id name, size_t arg_cnt, value_t ** args);