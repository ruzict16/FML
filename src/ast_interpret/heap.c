#include "heap.h"

#include <stdalign.h>

#include "debuglog.h"

unsigned char * ast_heap = NULL;
size_t ast_heap_top = 0;


void ast_init_null() {
        ((value_t *)(ast_heap))[0].kind = null_kind;
        ast_heap_top += sizeof(null_t);
}

bool ast_ptr_is_aligned(void * ptr, size_t alignment) {
        // debug("alloc attempt: %ld\n", (size_t)(ptr) % alignment);
        return (((size_t)(ptr) % alignment) == 0);
}

value_t * ast_halloc(size_t bytes, size_t alignment) {
        debug("alloc\n");
        size_t i = 0;
        while(!ast_ptr_is_aligned(ast_heap + ast_heap_top, alignment)) {
                ast_heap_top += 1;
                i++;
                if (i > 300) {
                        eprintf("fucked up alignment, you did\n");
                        exit(66);
                }
        }
        value_t * p = (value_t *)(ast_heap + ast_heap_top);
        ast_heap_top += bytes;
        debug("alloc done: %ld\n", ((size_t)p) - ((size_t)ast_heap));
        if (ast_heap_top >= HEAPSIZE) {
                eprintf("HEAP OVERFLOW\n");
                exit(42);
        }
        return p;
}

void ast_init_heap() {
        debug("init_heap\n");
        ast_heap = malloc(HEAPSIZE);
        ast_init_null();
}

void ast_free_heap() {
        debug("free_heap\n");
        free(ast_heap);
}

void ast_dealloc(value_t * p) {
        debug("dealloc\n");
        (void)p;
}

value_t * ast_alloc_null() {
        debug("alloc_null\n");
        debug("alloc_null done\n");
        return (value_t *)&ast_heap[0];
}
value_t * ast_alloc_integer() {
        debug("alloc_integer\n");
        value_t * p = ast_halloc(sizeof(integer_t), alignof(integer_t));
        p->kind = integer_kind;
        debug("alloc_integer done\n");
        return p;
}
value_t * ast_alloc_boolean() {
        debug("alloc_boolean\n");
        value_t * p = ast_halloc(sizeof(boolean_t), alignof(boolean_t));
        p->kind = boolean_kind;
        debug("alloc_boolean done\n");
        return p;
}
value_t * ast_alloc_array(size_t n) {
        debug("alloc_array\n");
        value_t * p = ast_halloc(( n * sizeof(value_t *))+sizeof(array_t), alignof(array_t));
        p->kind = array_kind;
        ((array_t *)p)->size = n;
        debug("alloc_array done\n");
        return p;
}
value_t * ast_alloc_object(size_t member_cnt) {
        debug("alloc_object\n");
        value_t * p = ast_halloc((member_cnt * sizeof(field_t))+sizeof(object_t), alignof(object_t));
        p->kind = object_kind;
        ((object_t *)p)->fields_cnt = member_cnt;
        debug("alloc_object done\n");
        return p;
}
// field_t * ast_alloc_field() {
//         value_t * p = ast_halloc(sizeof(field_t), alignof(field_t));
//         p->kind = field_kind;
//         return (field_t *)p;
// }
value_t * ast_alloc_function() {
        debug("alloc_function\n");
        value_t * p = ast_halloc(sizeof(function_t), alignof(function_t));
        p->kind = function_kind;
        debug("alloc_function done\n");
        return p;
}
