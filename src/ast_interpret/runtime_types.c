#include "runtime_types.h"

#include "debuglog.h"

void print_kind(enum value_kind kind) {
        switch (kind)
        {
        case object_kind:
                debug("object_kind\n");
                break;

        case null_kind:
                debug("null_kind\n");
                break;

        case array_kind:
                debug("array_kind\n");
                break;

        case integer_kind:
                debug("integer_kind\n");
                break;

        case boolean_kind:
                debug("boolean_kind\n");
                break;

        case function_kind:
                debug("function_kind\n");
                break;

        }
}

field_t * get_objects_field(object_t * object, string_id field) {
        size_t i;
        for (i = 0; i < object->fields_cnt; i++) {
                if (object->fields[i].name == field) {
                        return &(object->fields[i]);
                }
        }
        return NULL;
}

value_t ** lookup_field(value_t ** object, string_id field)
{
        if (object == NULL) {
                debug("lookup_field failed object == NULL\n");
                return NULL;
        }
        if ((*object)->kind != object_kind) {
                print_kind((*object)->kind);
                debug("lookup_field failed !object_kind\n");
                return NULL;
        }
        object_t * typed_object = (object_t *)(*object);

        field_t * fld = get_objects_field(typed_object, field);

        if (fld) {
                return &(fld->value);
        }

        (*object) = typed_object->parent;
        return lookup_field(object, field);
}
void print_value(value_t * v);
void print_null() {
        printf("null");
}
void print_integer(integer_t * v) {
        printf("%ld", v->value);
}
void print_boolean(boolean_t * v) {
        if (v->value) {
                printf("true");
        }
        else {
                printf("false");
        }
}
void print_array(array_t * v) {
        if (v->size == 0) {
                printf("[]");
                return;
        }
        size_t i;
        printf("[");
        for (i = 0; i < v->size - 1; i++) {
                print_value(v->data[i]);
                printf(", ");
        }
        print_value(v->data[v->size - 1]);
        printf("]");
}
void print_object(object_t * v) {
        // (void)v; printf("NOT_IMPLEMENTED: print_object");
        printf("object(");
        size_t i;
        if (v->parent != NULL && v->parent->kind != null_kind) {
                printf("..=");
                print_value(v->parent);
                if (v->fields_cnt > 0) {
                        printf(", ");
                }
        }
        for (i = 0; i < v->fields_cnt; i++) {
                printf("%s=", get_cstr(v->fields[i].name));
                print_value(v->fields[i].value);
                if (i != v->fields_cnt - 1) {
                        printf(", ");
                }
        }
        printf(")");
}
void print_function(function_t * v) {
        (void)v; printf("function");
}

void print_value(value_t * v) {
        if (v == NULL) {printf("SEGFAULT_ERROR\n");exit(3);}
        switch(v->kind) {
                break; case null_kind: {
                        print_null();
                }
                break; case integer_kind: {
                        print_integer((integer_t *)v);
                }
                break; case boolean_kind: {
                        print_boolean((boolean_t *)v);
                }
                break; case array_kind: {
                        print_array((array_t *)v);
                }
                break; case object_kind: {
                        print_object((object_t *)v);
                }
                break; case function_kind: {
                        print_function((function_t *)v);
                }
        }
}
