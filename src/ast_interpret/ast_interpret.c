#include "ast_interpret.h"

#include <stdio.h>


#include "debuglog.h"

#include "arena.h"
#include "parser.h"
#include "load_file.h"
#include "with.h"

#include "runtime_types.h"
#include "builtin_functions.h"
#include "heap.h"
#include "scopes.h"

#include "str.h"

value_t * do_null(AstNull * node);
value_t * do_boolean(AstBoolean * node);
value_t * do_integer(AstInteger * node);
value_t * do_array(AstArray * node);
value_t * do_object(AstObject * node);
value_t * do_function(AstFunction * node);
value_t * do_definition(AstDefinition * node);
value_t * do_variable_access(AstVariableAccess * node);
value_t * do_variable_assignment(AstVariableAssignment * node);
value_t * do_index_access(AstIndexAccess * node);
value_t * do_index_assignment(AstIndexAssignment * node);
value_t * do_field_access(AstFieldAccess * node);
value_t * do_field_assignment(AstFieldAssignment * node);
value_t * do_function_call(AstFunctionCall * node);
value_t * do_method_call(AstMethodCall * node);
value_t * do_conditional(AstConditional * node);
value_t * do_loop(AstLoop * node);
value_t * do_print(AstPrint * node);
value_t * do_block(AstBlock * node);
value_t * do_top(AstTop * node);

value_t * do_interpret(Ast * ast) {
        if (ast == NULL) return NULL;
        switch (ast->kind) {
                break; case AST_VARIABLE_ACCESS:     return do_variable_access((AstVariableAccess *) ast);
                break; case AST_FIELD_ACCESS:        return do_field_access((AstFieldAccess *) ast);
                break; case AST_INDEX_ACCESS:        return do_index_access((AstIndexAccess *) ast);
                break; case AST_FUNCTION_CALL:       return do_function_call((AstFunctionCall *) ast);
                break; case AST_METHOD_CALL:         return do_method_call((AstMethodCall *) ast);
                break; case AST_VARIABLE_ASSIGNMENT: return do_variable_assignment((AstVariableAssignment *) ast);
                break; case AST_FIELD_ASSIGNMENT:    return do_field_assignment((AstFieldAssignment *) ast);
                break; case AST_INDEX_ASSIGNMENT:    return do_index_assignment((AstIndexAssignment *) ast);
                break; case AST_NULL:                return do_null((AstNull *) ast);
                break; case AST_BOOLEAN:             return do_boolean((AstBoolean *) ast);
                break; case AST_INTEGER:             return do_integer((AstInteger *) ast);
                break; case AST_CONDITIONAL:         return do_conditional((AstConditional *) ast);
                break; case AST_ARRAY:               return do_array((AstArray *) ast);
                break; case AST_LOOP:                return do_loop((AstLoop *) ast);
                break; case AST_FUNCTION:            return do_function((AstFunction *) ast);
                break; case AST_BLOCK:               return do_block((AstBlock *) ast);
                break; case AST_OBJECT:              return do_object((AstObject *) ast);
                break; case AST_DEFINITION:          return do_definition((AstDefinition *) ast);
                break; case AST_PRINT:               return do_print((AstPrint *) ast);
                break; case AST_TOP:                 return do_top((AstTop *) ast);
        }
        eprintf("unknown AST type\n");
        return NULL;
}



int ast_interpret(const char *src, FILE * printstream)
{
        (void) printstream;
        ast_init_heap();
        init_strids();
        init_scopes();
        int exit_code = 0;

        char * file = NULL;
        Arena arena;

        with(file = load_file(src), free(file)) {
        with(arena_init(&arena), arena_destroy(&arena)) {

                Ast * ast = parse_src(&arena, Str_from_cstr(file));

                if (ast == NULL) {
                        eprintf("Failed to parse source: %s\n", src);
                        arena_destroy(&arena);
                        exit_with(exit_code = 1);
                }

                do_interpret(ast);
        }}
        free_scopes();
        free_strids();
        ast_free_heap();

        return exit_code;
}


bool is_truthy(const value_t * value) {
        // debug("truthy");
        /*
        # Truthiness

        Whether a value is deemed true for the purpose of conditions
        in conditional (if) and loop (while) expressions
        is called their truthiness.

        If the condition evaluates to a boolean,
                the condition is true if the runtime object is true and false otherwise.
        If the condition evaluates to null,
                the condition is false.
        If the condition evaluates to any other value,
                the condition is true.

        So only null and false are falsy, all other values are truthy.
        */

        switch(value->kind) {
                break; case null_kind: {
                        // debug("truthy: null_kind");
                        return false;
                }
                break; case boolean_kind: {
                        // debug("truthy: boolean_kind");
                        return (((boolean_t *)value)->value);
                }
                default: {
                        // debug("truthy: default");
                        return true;
                }
        }
}


value_t * do_null(AstNull * node) {
        // debug("do_null\n");
        (void)node;
        // Return (a reference to) a Null runtime object.
        return ast_alloc_null();
}
value_t * do_boolean(AstBoolean * node) {
        // debug("do_boolean\n");
        // Return (a reference to) a Boolean runtime object.
        value_t * p = ast_alloc_boolean();
        ((boolean_t *)p)->value = node->value;
        return p;
}
value_t * do_integer(AstInteger * node) {
        // debug("do_integer\n");
        // Return (a reference to) an Integer runtime object.
        value_t * p = ast_alloc_integer();
        ((integer_t *)p)->value = node->value;
        return p;
}


value_t * do_array(AstArray * node) {
        // debug("do_array\n");
        // Evaluate the AST specified by size in the current scope, expecting a positive integer value.
        value_t * size = do_interpret(node->size);
        if (size->kind != integer_kind) {
                // TODO: handle error: array has to have an integer size
        }

        // Create an Array runtime object using the result of evaluation of size as length.
        array_t * p = (array_t *)ast_alloc_array(((integer_t *)size)->value);
        p->size = ((integer_t *)size)->value;
        // For each element in the array,
        int64_t i;
        for (i = 0; i < ((integer_t *)size)->value; i++) {
                // evaluate initializer
                // in a new temporary scope
                // TODO: new scope for every element? or for the loop?
                new_scope();
                value_t * item = do_interpret(node->initializer);
                exit_scope();
                // and set the value of the element to the result.
                ((array_t *)p)->data[i] = item;
        }
        return (value_t *)p;
}

int comp_fields(const void * field_a, const void * field_b) {
    field_t * fa = ((field_t *)field_a);
    field_t * fb = ((field_t *)field_b);

    return str_cmp(get_str(fa->name), get_str(fb->name));
}

value_t * do_object(AstObject * node) {
        debug("do_object\n");
        (void)node;
        // Create a runtime object representing the Object instance.
        object_t * obj = (object_t *)ast_alloc_object(node->member_cnt);

        // Evaluate the extends AST
        //      in the current scope
        //
        //          ??? but there is a object scope test
        //
        //      into (a reference to) an object
        value_t * ext = do_interpret(node->extends);
        //      and register it with the Object instance
        //      as the parent object.
        obj->parent = ext;

        // Then walk the list of members:
        size_t i;
        debug("Register members: %ld == %ld\n", obj->fields_cnt, node->member_cnt);
        for (i = 0; i < obj->fields_cnt; i++) {
                new_scope();
                // If the member is a variable definition,
                if (node->members[i]->kind == AST_DEFINITION) {
                        AstDefinition * def = (AstDefinition *)node->members[i];

                        // evaluate the AST representing the initial value
                        // in the current scope,
                        value_t * value = do_interpret(def->value);
                        string_id name = get_strid(def->name);
                        debug(" - Register member: %s\n", get_cstr(name));

                        // and register (a reference to) the resulting value
                        // within the object as a field
                        // with the name provided by name.
                        obj->fields[i].name = name;
                        obj->fields[i].value = value;
                }
                // If any other member type occurs,
                else {
                        // the AST is not well formed—​error out.
                        eprintf("AST bad\n");
                        // TODO: error out
                }
                exit_scope();
        }
        // sort fields
        qsort (obj->fields, obj->fields_cnt, sizeof(field_t), comp_fields);


        // Return (a reference to) the Object instance.
        debug("do_object done\n");
        return (value_t *)obj;
}
value_t * do_function(AstFunction * node) {
        debug("do_function\n");
        (void)node;
        // Create a Function runtime object representing this AstFunction.
        // Return (a reference to) the created Function runtime object.
        // debug("NOT IMPLEMENTED: do_function\n");
        function_t * foo = (function_t *)ast_alloc_function();
        foo->ast = node;
        return (value_t *)foo;
}
value_t * do_definition(AstDefinition * node) {
        debug("do_definition\n");
        // Covering only variable definition here,
        // member field definition will be covered under AstObject.

        // Evaluate AST specified by value in the current scope,
        string_id name = get_strid(node->name);
        value_t * val = do_interpret(node->value);
        // then create a new variable in the current scope,
        // using the result of the evaluation as the variable’s value.
        new_var(name, val);
        // Return (a reference to) the value of the variable.
        return val;
}
value_t * do_variable_access(AstVariableAccess * node) {
        debug("do_variable_access %.*s\n", (int)node->name.len, (const char *)node->name.str);
        // Look up a variable with this name
        string_id name = get_strid(node->name);
        // debug("do_variable_access 2 %.*s\n", (int)node->name.len, (const char *)node->name.str);
        value_t * var = get_var(name);
        // debug("do_variable_access done %.*s\n", (int)node->name.len, (const char *)node->name.str);
        // and return (a reference to) this runtime object.
        return var;

        // NOTE:
        // The lookup should first try the current scope
        // and continue with subsequent parent scopes until it succeeds.
}
value_t * do_variable_assignment(AstVariableAssignment * node) {
        debug("do_variable_assignment\n");
        (void)node;
        // Evaluate value in the current scope.
        value_t * val = do_interpret(node->value);
        // Then, look up a variable specified by name
        // and set its value to (a reference to) the runtime object
        // returned from the evaluation of value.
        string_id name = get_strid(node->name);
        set_var(name, val);
        // Return (a reference to) the assigned runtime object.
        return val;
}
value_t * do_actual_method_call(value_t * self, function_t * method, size_t arg_cnt, value_t ** args);
value_t * do_unwrapped_method_call(
        Ast* ast_object,
        Str method_name,
        Ast ** ast_args,
        size_t args_cnt
) {
        debug("do_method_call\n");

        // Evaluate the object AST in the current scope to retrieve (a reference to) an apparent receiver object.
        value_t * object = do_interpret(ast_object);

        // Evaluate each of the arguments in the argument sequence in order using the current scope.
        value_t ** args = malloc(args_cnt * sizeof(value_t *));
        size_t i;
        for(i=0; i < args_cnt; i++) {
                args[i] = do_interpret(ast_args[i]);
        }

        string_id name = get_strid(method_name);

        // Lookup name (the name of the method) like a field normally
        // (recursively on parents if not found on current object),
        // but remember the object whose field name is—​this is the actual receiver.
        value_t * current_self = object;
        value_t ** field = lookup_field(&current_self, name);

        value_t * ret = NULL;
        if (field == NULL) {
                ret = do_builtin_call(current_self, name, args_cnt, args);
                if (ret == NULL) {
                        eprintf("No such method\n");
                        exit(8);
                }
                // TODO: still can fail when name is not a builtin of the top most parent
        }
        else if ((*field)->kind == function_kind) {
                ret = do_actual_method_call(current_self, (function_t *)(*field), args_cnt, args);
        }
        else {
                // Assert that the value of the field is a function.
                // TODO: error out
                eprintf("Method call does not refer to a member function");
        }


        free(args);
        debug("do_method_call done\n");
        // Return (a reference to) the value returned from the method’s body.
        return ret;

}
value_t * do_index_access(AstIndexAccess * node) {
        debug("do_index_access\n");
        (void)node;
        // Evaluate object and then index in the current scope.
        // value_t * obj = do_interpret(node->object);
        // value_t * i = do_interpret(node->index);

        // debug("object: %d\n",node->object->kind);
        // debug("index: %d\n",node->index->kind);

        // if (obj->kind != array_kind)  {
        //         eprintf("indexer can be used only with arrays\n");
        //         print_kind(obj->kind);
        //         exit(49);
        // }

        // if (i->kind != integer_kind)  {
        //         eprintf("index has to be an integer value\n");
        //         print_kind(i->kind);
        //         exit(49);
        // }


        // Call object's get method with index as an argument.
        return do_unwrapped_method_call(node->object, Str_from_cstr("get"), &(node->index), 1);
}
value_t * do_index_assignment(AstIndexAssignment * node) {
        debug("do_index_assignment\n");
        // Evaluate object, then index and then value in the current scope.
        // value_t * obj = do_interpret(node->object);
        // value_t * i = do_interpret(node->index);
        // value_t * val = do_interpret(node->value);

        // TODO: check kinds
        // Call object's set method with index and value as arguments.
        Ast * args[2] = {node->index, node->value};
        return do_unwrapped_method_call(node->object, Str_from_cstr("set"), args, 2);
        // return fn_set((array_t *)obj, (integer_t *)i, val);
}
value_t * do_field_access(AstFieldAccess * node) {
        debug("do_field_access\n");
        (void)node;
        // Evaluate the object AST in the current scope,
        // expecting (a reference to) an Object runtime object.
        value_t * object = do_interpret(node->object);

        // Look up the value of field with name field within the object
        // Field lookup starts with the current Object
        // and follows the parent objects until a fields is found.
        value_t ** field = lookup_field(&object, get_strid(node->field));

        // and return (a reference to) the result.
        if (field) {
                debug("do_field_access done\n");
                return *field;
        }
        // An error should be raised if field is not found.
        else {
                eprintf("Field could not been found\n");
                exit (6);
        }
}
value_t * do_field_assignment(AstFieldAssignment * node) {
        debug("do_field_assignment\n");
        (void)node;
        // Evaluate object and then value in the current scope.
        value_t * object = do_interpret(node->object);
        value_t * value = do_interpret(node->value);
        // Then, find the field with name field in the (reference to the) object
        // returned from the evaluation of object
        value_t ** field = lookup_field(&object, get_strid(node->field));
        // and set its value to (the reference to) the object
        // returned from the evaluation of value.
        (*field) = value;
        debug("do_field_assignment done\n");
        return (*field);
}
void zip_args_as_scope_vars(size_t cnt, Str * params, value_t ** args) {
        size_t i;
        for(i=0; i < cnt; i++) {
                string_id name = get_strid(params[i]);
                new_var(name, args[i]);
        }
}
value_t * do_function_call(AstFunctionCall * node) {
        debug("do_function_call\n");
        // Evaluate function in the current scope.
        // Expect it to be a Function runtime object
        function_t * foo = (function_t *)do_interpret(node->function);
        // which has the same arity as is the number of arguments in the call
        // (both don’t include the receiver this).
        if (foo->ast->parameter_cnt != node->argument_cnt) {
                // TODO: error out
        }

        // Evaluate each argument starting from the first in the sequence,
        // yielding a sequence of (references to) runtime objects.
        value_t ** args = malloc(node->argument_cnt * sizeof(value_t *));
        size_t i;
        for(i=0; i < node->argument_cnt; i++) {
                args[i] = do_interpret(node->arguments[i]);
        }

        // Create a new scope, whose parent is only the global scope.
        new_function_scope();

        // Zip the sequence of parameters from the function definition
        // with the sequence of (references to) runtime objects
        // representing values of the arguments,
        // so that each parameter is matched with one argument value.
        // In the new scope register each pair as well as this to be null.
        zip_args_as_scope_vars(node->argument_cnt, foo->ast->parameters, args);
        new_var(get_strid_c("this"), ast_alloc_null());

        // Then, execute the AST of the functions body in the scope.
        value_t * ret = do_interpret(foo->ast->body);

        // Discard the new scope and return to the previous scope.
        exit_scope();

        debug("do_function_call end; return: ");
        print_kind(ret->kind);
        free(args);

        // Return (a reference to) the result of function’s body evaluation.
        return ret;
}

value_t * do_actual_method_call(value_t * self, function_t * method, size_t arg_cnt, value_t ** args) {
        debug("do_actual_function_call\n");
        // Create a new child scope.
        new_function_scope();

        // Zip the sequence of parameters from the method definition with the sequence
        // of (references to) objects representing values of the arguments,
        // so that each parameter is matched with one argument value.
        // In the child scope register each pair as well as the actual receiver as this.
        zip_args_as_scope_vars(arg_cnt, method->ast->parameters, args);
        new_var(get_strid_c("this"), self);

        // Then, execute the AST of the method’s body in the scope.
        value_t * ret = do_interpret(method->ast->body);

        // Discard the new scope and return to the previous scope. // I implied that
        exit_scope();
        debug("do_actual_function_call done\n");
        return ret;
}

value_t * do_method_call(AstMethodCall * node) {
        debug("do_method_call\n");
        (void)node;
        // Evaluate the object AST in the current scope to retrieve (a reference to) an apparent receiver object.
        value_t * object = do_interpret(node->object);

        // Evaluate each of the arguments in the argument sequence in order using the current scope.
        value_t ** args = malloc(node->argument_cnt * sizeof(value_t *));
        size_t i;
        for(i=0; i < node->argument_cnt; i++) {
                args[i] = do_interpret(node->arguments[i]);
        }

        string_id name = get_strid(node->name);

        // Lookup name (the name of the method) like a field normally
        // (recursively on parents if not found on current object),
        // but remember the object whose field name is—​this is the actual receiver.
        value_t * current_self = object;
        value_t ** field = lookup_field(&current_self, name);

        value_t * ret = NULL;
        if (field == NULL) {
                ret = do_builtin_call(current_self, name, node->argument_cnt, args);
                if (ret == NULL) {
                        eprintf("No such method\n");
                        exit(8);
                }
                // TODO: still can fail when name is not a builtin of the top most parent
        }
        else if ((*field)->kind == function_kind) {
                ret = do_actual_method_call(current_self, (function_t *)(*field), node->argument_cnt, args);
        }
        else {
                // Assert that the value of the field is a function.
                // TODO: error out
                eprintf("Method call does not refer to a member function");
        }


        free(args);
        debug("do_method_call done\n");
        // Return (a reference to) the value returned from the method’s body.
        return ret;

}
value_t * do_conditional(AstConditional * node) {
        debug("do_conditional\n");
        (void)node;
        // Evaluate the condition in the current scope. Decide the truthiness of the condition and:
        if (is_truthy(do_interpret(node->condition))) {
                new_scope();
                // If the condition is true, evaluate consequent in a temporary scope and return its result.
                value_t * v = do_interpret(node->consequent);
                exit_scope();
                return v;
        }
        else {
                new_scope();
                // If the condition is false, evaluate alternative in a temporary scope and return its result.
                value_t * v = do_interpret(node->alternative);
                exit_scope();
                return v;
        }

        // NOTE:
        // While the condition is part of the parent scope
        // (definitions inside the conditions are available further, notably in whichever branch is evaluated),
        // the evaluated branch has it’s own scope and its definitions don’t propage further
        // (regardless of the branch being a block).
}
value_t * do_loop(AstLoop * node) {
        debug("do_loop\n");
        (void)node;
        // Evaluate the condition using the current scope. Decide the truthiness of the condition and:
        while(is_truthy(do_interpret(node->condition))) {
                // If the condition is true, evaluate the body of the loop in a new temporary scope and repeat the entire interpretation.
                new_scope();
                do_interpret(node->body);
                exit_scope();
        }
        // If the condition is false, return (a reference to) the Null runtime object.
        return ast_alloc_null();

        // NOTE:
        // Each iteration has its separate (temporary) scope, whether the body is a block or not.
}

void print_previous(const char * begin, const char * cur) {
        if (cur - begin <= 0) {
                debug(" - actually do_print: NADA\n");
                return;
        }
        debug(" - actually do_print: %.*s\n", (int)(cur - begin), begin);
        printf("%.*s", (int)(cur - begin), begin);
}

value_t * do_print(AstPrint * node) {
        debug("do_print: %.*s\n", (int)node->format.len, (const char *)node->format.str);
        // debug("%.*s\n", node->format.len, node->format.str);
        (void)node;
        // Evaluate each argument starting from the first in the sequence,
        // yielding a sequence of (references to) runtime objects.
        value_t ** args = malloc(node->argument_cnt * sizeof(value_t *));
        size_t i;
        for (i = 0; i < node->argument_cnt; i++) {
                args[i] = do_interpret(node->arguments[i]);
        }
        size_t next_arg = 0;
        const char * begin = (const char *)node->format.str;
        const char * cur = (const char *)node->format.str;
        const char * end = (const char *)node->format.str + node->format.len;
        for (;cur <= end;cur++) {
                if (*cur == '~') {
                        print_previous(begin, cur);
                        print_value(args[next_arg++]);
                        begin = cur + 1;
                        continue;
                }
                if (*cur == '\\' && cur + 1 != end) {
                        print_previous(begin, cur);
                        switch(*(cur + 1)) {
                                break; case 'n': {
                                        printf("%c", 0x0A);
                                }
                                break; case 'r': {
                                        printf("%c", 0x0D);
                                }
                                break; case 't': {
                                        printf("%c", 0x09);
                                }
                                break; case '\\': {
                                        printf("%c", 0x5C);
                                }
                                break; case '~': {
                                        printf("%c", 0x7E);
                                }
                                break; case '"': {
                                        printf("%c", 0x22);
                                }
                                break; default:
                                        eprintf("Unknown escape sequence/n");
                                        exit(77);
                        }
                        cur++;
                        begin = cur + 1;
                        continue;
                }
        }

        print_previous(begin, end);
        debug("do_print done\n");
        free(args);
        return ast_alloc_null();
}
value_t * do_block(AstBlock * node) {
        debug("do_block\n");
        // printf("do_block\n");
        (void)node;
        // Create a new scope.
        new_scope();
        // Evaluate each expression subtree in this scope,
        value_t * result = NULL;
        size_t i;
        for (i = 0; i < node->expression_cnt; i++) {
                result = do_interpret((node->expressions)[i]);
        }
        // discard the new scope,
        exit_scope();
        // and return the result of the last expr.
        // printf("do_block end\n");
        return result;
}
value_t * do_top(AstTop * node) {
        debug("do_top\n");
        // printf("do_top\n");
        (void)node;
        // Evaluate each expression subtree in this scope using the top-most (global) scope.

        // Evaluate each expression subtree in this scope,
        value_t * result = NULL;
        size_t i;
        for (i = 0; i < node->expression_cnt; i++) {
                result = do_interpret((node->expressions)[i]);
        }
        // and return the result of the last expr.
        // printf("do_top end\n");
        return result;
}
