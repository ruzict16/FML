#pragma once
#define HEAPSIZE 2147483648

#include "runtime_types.h"

void ast_init_heap();
void ast_free_heap();

value_t * ast_alloc_null();
value_t * ast_alloc_integer();
value_t * ast_alloc_boolean();
value_t * ast_alloc_array(size_t n);
value_t * ast_alloc_object(size_t member_cnt);
value_t * ast_alloc_function();
// field_t * ast_alloc_field();
void ast_dealloc(value_t * p);