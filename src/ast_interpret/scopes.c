#include "scopes.h"

#include "string_id.h"
#include "runtime_types.h"
#include "heap.h"

#include "debuglog.h"

typedef struct var_node_t {
        string_id id;
        value_t * value;
        struct var_node_t * lookup_var;
} var_node_t;

typedef struct scope_node_t {
        var_node_t * vars;
        struct scope_node_t * parent_scope;
        struct scope_node_t * lookup_scope;
} scope_node_t;

scope_node_t * global_scope = NULL;
scope_node_t * current_scope = NULL;

void init_scopes() {
        scope_node_t * scope = malloc(sizeof(scope_node_t));
        scope->vars = NULL;
        scope->parent_scope = NULL;
        scope->lookup_scope = NULL;
        current_scope = scope;
        global_scope = scope;
}

void new_scope() {
        debug("new_scope\n");
        scope_node_t * scope = malloc(sizeof(scope_node_t));
        scope->vars = NULL;
        scope->parent_scope = current_scope;
        scope->lookup_scope = current_scope;
        current_scope = scope;
}

void new_function_scope() {
        debug("new_function_scope\n");
        scope_node_t * scope = malloc(sizeof(scope_node_t));
        scope->vars = NULL;
        scope->parent_scope = current_scope;
        scope->lookup_scope = global_scope;
        current_scope = scope;
}

void exit_scope() {
        debug("exit_scope\n");
        // TODO: cleanup
        current_scope = current_scope->parent_scope;
}

var_node_t * find_scope_var(string_id id, scope_node_t * scope) {
        var_node_t * var_node = scope->vars;
        while(var_node) {
                if (var_node->id == id) {
                        return var_node;
                }
                var_node = var_node->lookup_var;
        }
        debug("did not find scope var\n");
        return NULL;
}

var_node_t * find_var(string_id id) {
        scope_node_t * scope = current_scope;
        while (scope) {
                var_node_t * val = find_scope_var(id, scope);
                if (val) {
                        return val;
                }
                scope = scope->lookup_scope;
        }
        debug("did not find var\n");
        return NULL;
}

void new_var(string_id id, value_t * val) {
        debug("new_var: %ld aka %s aka ", id, get_cstr(id));
        print_kind(val->kind);
        var_node_t * node = malloc(sizeof(var_node_t));
        node->id = id;
        node->value = val;
        node->lookup_var = current_scope->vars;
        current_scope->vars = node;
}

void set_var(string_id id, value_t * val) {
        debug("set_var: %ld aka %s aka ", id, get_cstr(id));
        fflush(stdout);
        print_kind(val->kind);
        var_node_t * var = find_var(id);
        if (var) {
                var->value = val;
        }
        else {
                // TODO: maybe error?
                new_var(id, val);
        }
}

value_t * get_var(string_id id) {
        debug("get_var: %ld aka %s\n", id, get_cstr(id));
        var_node_t * var = find_var(id);
        if (var) {
                debug(" - found var: ");
                print_kind(var->value->kind);
                return var->value;
        }
        else {
                debug(" - did not found var, creating new one as null\n");
                new_var(id, ast_alloc_null());
                return get_var(id);
        }
}


void free_scopes() {
        debug("free_scopes\n");
        // TODO: cleanup
}
