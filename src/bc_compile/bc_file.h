#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#pragma region instruction_types

typedef enum {
        BCINS_DROP = 0x00,
        BCINS_CONST = 0x01,
        BCINS_PRINT = 0x02,
        BCINS_ARRAY = 0x03,
        BCINS_OBJECT = 0x04,
        BCINS_GETFIELD = 0x05,
        BCINS_SETFIELD = 0x06,
        BCINS_CALLMET = 0x07,
        BCINS_CALLFUN = 0x08,
        BCINS_SETLOCAL = 0x09,
        BCINS_GETLOCAL = 0x0A,
        BCINS_SETGLOBAL = 0x0B,
        BCINS_GETGLOBAL = 0x0C,
        BCINS_BRANCH = 0x0D,
        BCINS_JUMP = 0x0E,
        BCINS_RETURN = 0x0F,
        BCINS_DUMMY = 0xFF,
} instruction_kind_t;

typedef struct {
        instruction_kind_t kind;
        uint8_t size;
} instruction_t;

typedef struct {
        instruction_t base;
} dummy_t;

typedef uint16_t const_pool_id_t;

typedef struct { instruction_t base; } instruction_drop_t;
typedef struct { instruction_t base; } instruction_return_t;
typedef struct { instruction_t base; } instruction_array_t;
typedef struct { instruction_t base; } instruction_dummy_t;
typedef struct { instruction_t base; uint8_t argc; } instruction_call_function_t;
typedef struct { instruction_t base; uint16_t local; } instruction_set_local_t;
typedef struct { instruction_t base; uint16_t local; } instruction_get_local_t;
typedef struct { instruction_t base; uint16_t global; } instruction_set_global_t;
typedef struct { instruction_t base; uint16_t global; } instruction_get_global_t;
typedef struct { instruction_t base; int16_t off; } instruction_branch_t;
typedef struct { instruction_t base; int16_t off; } instruction_jump_t;
typedef struct { instruction_t base; const_pool_id_t cls; } instruction_object_t;
typedef struct { instruction_t base; const_pool_id_t id; } instruction_get_constant_t;
typedef struct { instruction_t base; const_pool_id_t id; } instruction_get_field_t;
typedef struct { instruction_t base; const_pool_id_t id; } instruction_set_field_t;
typedef struct { instruction_t base; const_pool_id_t name; uint8_t argc; } instruction_call_method_t;
typedef struct { instruction_t base; const_pool_id_t fmt; uint8_t argc; } instruction_print_t;

#pragma endregion instruction_types

#pragma region const_pool_items


typedef enum  {
        BCCONST_INT = 0x00,
        BCCONST_NUL = 0x01,
        BCCONST_STR = 0x02,
        BCCONST_FUN = 0x03,
        BCCONST_BUL = 0x04,
        BCCONST_CLS = 0x05,
} compiler_const_pool_item_type_t;

typedef struct {
        compiler_const_pool_item_type_t kind;
} compiler_const_pool_item_t;

typedef struct {
        compiler_const_pool_item_t base;
        uint64_t size;
        uint8_t * value;
} compiler_const_pool_string_t;

typedef struct {
        compiler_const_pool_item_t base;
        int32_t value;
} compiler_const_pool_number_t;

typedef struct {
        compiler_const_pool_item_t base;
} compiler_const_pool_null_t;

typedef struct {
        compiler_const_pool_item_t base;
        bool value;
} compiler_const_pool_bool_t;

typedef struct {
        compiler_const_pool_item_t base;
        uint64_t size;
        const_pool_id_t * value;
} compiler_const_pool_class_t;

struct function_body_t;
typedef struct {
        compiler_const_pool_item_t base;
        struct function_body_t * value;
} compiler_const_pool_function_t;

#pragma endregion const_pool_items

#pragma region core

typedef struct {
        int params;
        int locals;
        uint64_t instructions_cnt;
        instruction_t * instructions[50000];
        bool is_entry;
} function_body_t;

typedef struct {
        function_body_t * fn;
        uint64_t i;
} instruction_position_t;

// typedef struct {
//         int * identifier_stack;
//         int * scope_pointers;
//         // get(identifier) -> stack_i
//         // push_identifier(identifier) -> stack_i
//         // begin_scope()
//         // end_scope()
// } stack_index_t;


typedef struct {
        const_pool_id_t size;
        compiler_const_pool_item_t * arr;
} compiler_const_pool_t;

#include <lib/str.h>

typedef struct bc_environment_t {
        struct bc_environment_t * parent;
        uint64_t names_size;
        Str names[5000];
        uint64_t ids[5000];
        bool scoped;
} bc_environment_t;

typedef struct {
        uint64_t const_pool_size;
        compiler_const_pool_item_t * const_pool [5000]; // includes strings and functions (only indexes into functions vector right now)

        function_body_t * current_function;

        uint64_t globals_count;
        const_pool_id_t globals[5000]; // const_pool indexes (to strings)

        bc_environment_t * envs; // linked list

        // Print vars
        uint8_t status;
        uint16_t constant_index;
        uint16_t instruction_index;
        uint16_t stage;
        const_pool_id_t entry_fn;
} bcfile_t;

void bc_init(bcfile_t * f);

#pragma endregion core

#pragma region serialization_functions

const char * error_string(bcfile_t * f);
uint8_t get_status(bcfile_t * f);
void print_init(bcfile_t * f);

#define INPROGRESS 42
#define FAILURE 66
#define DONE 20

/**
 * @brief Serializes at most `buffer_size` bytes into `buffer`
 * It is not guaranteed that the serialization will fill the whole `buffer_size`
 * in each step even if there are still bytes left to serialize.
 * That is due to simplicity of implementing multiple stages of serialization.
 * Caller is responsible for setting correct offset in it's inner buffer as the `buffer` pointer
 * and is responsible for calling this function over and over
 * as long as the `get_status` returns `INPROGRESS`
 * @param f compiler's representation of the file to serialize
 * @param buffer pointer to the buffer where the next serialized byte will be put
 * @param buffer_size maximum number of bytes to serialize (counted from the `buffer`)
 * @return number of serialized bytes (will be less than or equal `buffer_size`)
 */
uint64_t print_batch(bcfile_t * f, uint8_t * buffer, uint64_t buffer_size);

void bc_print(bcfile_t * f, FILE *);

#pragma endregion serialization_functions

#pragma region functions

// Dedupe, Premake
const_pool_id_t new_constant_bool(bcfile_t * f, bool b);
// Dedupe
const_pool_id_t new_constant_integer(bcfile_t * f, int32_t i);
// Dedupe, Premake
const_pool_id_t new_constant_null(bcfile_t * f);
// Dedupe
const_pool_id_t new_constant_class(bcfile_t * f, const_pool_id_t * members, uint32_t members_cnt);
// Dedupe
const_pool_id_t new_constant_string(bcfile_t * f, const uint8_t * data, unsigned long size);
const_pool_id_t new_constant_function(bcfile_t * f, function_body_t * fn);

// instruction_t * new_instruction_constant(const_pool_id_t i);
// instruction_t * new_instruction_drop();
// instruction_t * new_instruction_call_fn(uint8_t argc);
// instruction_t * new_instruction_call_met(const_pool_id_t name, uint8_t argc);
// instruction_t * new_instruction_setlocal(uint16_t i);
// instruction_t * new_instruction_setglobal(uint16_t i);
// instruction_t * new_instruction_getlocal(uint16_t i);
// instruction_t * new_instruction_getglobal(uint16_t i);
// instruction_t * new_instruction_print(const_pool_id_t format, uint8_t argc);
// instruction_t * new_instruction_object(const_pool_id_t class);

instruction_t * new_instruction_drop( );
instruction_t * new_instruction_return( );
instruction_t * new_instruction_array( );
instruction_t * new_instruction_dummy( );
instruction_t * new_instruction_call_function( uint8_t argc );
instruction_t * new_instruction_set_local( uint16_t local );
instruction_t * new_instruction_get_local( uint16_t local );
instruction_t * new_instruction_set_global( uint16_t global );
instruction_t * new_instruction_get_global( uint16_t global );
instruction_t * new_instruction_branch( int64_t off );
instruction_t * new_instruction_jump( int64_t off );
instruction_t * new_instruction_object( const_pool_id_t cls );
instruction_t * new_instruction_get_constant( const_pool_id_t id );
instruction_t * new_instruction_get_field( const_pool_id_t id );
instruction_t * new_instruction_set_field( const_pool_id_t id );
instruction_t * new_instruction_call_method( const_pool_id_t name, uint8_t argc );
instruction_t * new_instruction_print( const_pool_id_t fmt, uint8_t argc );


void env_begin(bcfile_t * f, bc_environment_t * parent);
void env_end(bcfile_t * f);
bool env_is_global(bcfile_t * f);

bc_environment_t * get_current_env(bcfile_t * f);
void set_current_env(bcfile_t * f, bc_environment_t * env);


uint16_t declare_local(bcfile_t * f, const uint8_t * name, uint64_t name_len);
uint16_t declare_opaque_local(bcfile_t * f);
inline uint16_t declare_this(bcfile_t * f) {return declare_local(f, (const uint8_t *)"this", 4);}
uint16_t find_local(bcfile_t * f, const uint8_t * name, uint64_t name_len, bool * success);

uint16_t access_global(bcfile_t * f, const uint8_t * name, uint64_t name_len);

instruction_position_t add_instruction(bcfile_t * f, instruction_t * ins);
void patch_instruction(instruction_position_t ref, instruction_t * ins);
uint64_t next_instruction_index(bcfile_t * f);

#pragma endregion functions
