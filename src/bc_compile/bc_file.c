#include "bc_file.h"

#include <stdlib.h>
#include <string.h>

void bc_init(bcfile_t * bc) {
        // fprintf(stderr, "const true\n");
        compiler_const_pool_bool_t * t = malloc(sizeof(compiler_const_pool_bool_t));
        (*t).base.kind = BCCONST_BUL;
        (*t).value = true;
        // // fprintf(stderr, "const false\n");
        compiler_const_pool_bool_t * f = malloc(sizeof(compiler_const_pool_bool_t));
        (*f).base.kind = BCCONST_BUL;
        (*f).value = false;
        // fprintf(stderr, "const null\n");
        compiler_const_pool_null_t * n = malloc(sizeof(compiler_const_pool_null_t));
        (*n).base.kind = BCCONST_NUL;

        bc->const_pool_size = 0;
        bc->const_pool[bc->const_pool_size++] = (compiler_const_pool_item_t *) n;
        bc->const_pool[bc->const_pool_size++] = (compiler_const_pool_item_t *) f;
        bc->const_pool[bc->const_pool_size++] = (compiler_const_pool_item_t *) t;

        bc->current_function = malloc(sizeof(function_body_t));

        *(bc->current_function) = (function_body_t) {
                .params = 1,
                .locals = 0,
                .instructions_cnt = 0,
                .is_entry = true
        };

        // fprintf(stderr, "const main\n");
        new_constant_function(bc, bc->current_function);

        bc->globals_count = 0;
        bc->envs = NULL;
}

#pragma region new_constants

const_pool_id_t new_constant_null(bcfile_t * bc) {
        (void)bc;
        return 0;
}

const_pool_id_t new_constant_bool(bcfile_t * bc, bool b) {
        (void)bc;
        return b ? 2 : 1;
}

const_pool_id_t new_constant_integer(bcfile_t * bc, int32_t n) {
        size_t i;
        for (i = 0; i < bc->const_pool_size; i++) {
                if (
                        bc->const_pool[i]->kind == BCCONST_INT &&
                        ((compiler_const_pool_number_t *) (bc->const_pool[i]))->value == n
                ) {
                        return i;
                }
        }
        compiler_const_pool_number_t * nn = malloc(sizeof(compiler_const_pool_number_t));
        nn->base.kind = BCCONST_INT;
        nn->value = n;
        // fprintf(stderr, "const int %d\n", nn->value);
        bc->const_pool[bc->const_pool_size] = (compiler_const_pool_item_t *) nn;
        return bc->const_pool_size++;
}

const_pool_id_t new_constant_function(bcfile_t * bc, function_body_t * fn) {
        compiler_const_pool_function_t * fun = malloc(sizeof(compiler_const_pool_function_t));
        fun->base.kind = BCCONST_FUN;
        fun->value = (struct function_body_t *) fn;
        // fprintf(stderr, "const fn\n");
        bc->const_pool[bc->const_pool_size] = (compiler_const_pool_item_t *) fun;
        return bc->const_pool_size++;
}

const_pool_id_t new_constant_string(bcfile_t * bc, const uint8_t * data, unsigned long size) {
        size_t i;
        for (i = 0; i < bc->const_pool_size; i++) {
                if (
                        bc->const_pool[i]->kind == BCCONST_STR &&
                        ((compiler_const_pool_string_t *) (bc->const_pool[i]))->size == size &&
                        strncmp((const char *)((compiler_const_pool_string_t *)(bc->const_pool[i]))->value, (const char *)data, size) == 0
                ) {
                        return i;
                }
        }
        compiler_const_pool_string_t * str = malloc(sizeof(compiler_const_pool_string_t));
        str->base.kind = BCCONST_STR;
        str->value = (uint8_t *) data;
        str->size = size;
        // fprintf(stderr, "const string\n");
        bc->const_pool[bc->const_pool_size] = (compiler_const_pool_item_t *) str;
        return bc->const_pool_size++;
}

const_pool_id_t new_constant_class(bcfile_t * bc, const_pool_id_t * members, uint32_t members_cnt) {
        compiler_const_pool_class_t * cls = malloc(sizeof(compiler_const_pool_class_t));
        cls->base.kind = BCCONST_CLS;
        cls->value = malloc(sizeof(const_pool_id_t) * members_cnt);
        memcpy(cls->value, members, members_cnt);
        cls->size = members_cnt;
        // fprintf(stderr, "const class\n");
        bc->const_pool[bc->const_pool_size] = (compiler_const_pool_item_t *) cls;
        return bc->const_pool_size++;
}


#pragma endregion new_constants

#pragma region new_instructions

instruction_t * new_instruction_drop() {
        instruction_drop_t * ins = malloc(sizeof(instruction_drop_t));
        ins->base.size = 1;
        ins->base.kind = BCINS_DROP;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_return() {
        instruction_return_t * ins = malloc(sizeof(instruction_return_t));
        ins->base.size = 1;
        ins->base.kind = BCINS_RETURN;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_array() {
        instruction_array_t * ins = malloc(sizeof(instruction_array_t));
        ins->base.size = 1;
        ins->base.kind = BCINS_ARRAY;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_dummy() {
        instruction_dummy_t * ins = malloc(sizeof(instruction_dummy_t));
        ins->base.size = 0;
        ins->base.kind = BCINS_DUMMY;
        return (instruction_t *) ins;
}

instruction_t * new_instruction_get_constant(const_pool_id_t i) {
        instruction_get_constant_t * ins = malloc(sizeof(instruction_get_constant_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_CONST;
        ins->id = i;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_call_function(uint8_t argc) {
        instruction_call_function_t * ins = malloc(sizeof(instruction_call_function_t));
        ins->base.size = 2;
        ins->base.kind = BCINS_CALLFUN;
        ins->argc = argc;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_call_method(const_pool_id_t name, uint8_t argc) {
        instruction_call_method_t * ins = malloc(sizeof(instruction_call_method_t));
        ins->base.size = 4;
        ins->base.kind = BCINS_CALLMET;
        ins->name = name;
        ins->argc = argc;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_set_local(uint16_t i) {
        instruction_set_local_t * ins = malloc(sizeof(instruction_set_local_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_SETLOCAL;
        ins->local = i;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_set_global(uint16_t i) {
        instruction_set_global_t * ins = malloc(sizeof(instruction_set_global_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_SETGLOBAL;
        ins->global = i;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_get_local(uint16_t i) {
        instruction_get_local_t * ins = malloc(sizeof(instruction_get_local_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_GETLOCAL;
        ins->local = i;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_get_global(uint16_t i) {
        instruction_get_global_t * ins = malloc(sizeof(instruction_get_global_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_GETGLOBAL;
        ins->global = i;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_print(const_pool_id_t format, uint8_t argc) {
        instruction_print_t * ins = malloc(sizeof(instruction_print_t));
        ins->base.size = 4;
        ins->base.kind = BCINS_PRINT;
        ins->fmt = format;
        ins->argc = argc;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_object(const_pool_id_t class) {
        instruction_object_t * ins = malloc(sizeof(instruction_object_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_OBJECT;
        ins->cls = class;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_branch( int64_t off ) {
        instruction_branch_t * ins = malloc(sizeof(instruction_branch_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_BRANCH;
        ins->off = off;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_jump( int64_t off ) {
        instruction_jump_t * ins = malloc(sizeof(instruction_jump_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_JUMP;
        ins->off = off;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_get_field( const_pool_id_t id ) {
        instruction_get_field_t * ins = malloc(sizeof(instruction_get_field_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_GETFIELD;
        ins->id = id;
        return (instruction_t *) ins;
}
instruction_t * new_instruction_set_field( const_pool_id_t id ) {
        instruction_set_field_t * ins = malloc(sizeof(instruction_set_field_t));
        ins->base.size = 3;
        ins->base.kind = BCINS_SETFIELD;
        ins->id = id;
        return (instruction_t *) ins;
}

#pragma endregion new_instructions

#pragma region insert_instructions

instruction_position_t add_instruction(bcfile_t * bc, instruction_t * ins) {
        bc->current_function->instructions[bc->current_function->instructions_cnt] = ins;
        return (instruction_position_t) {
                .fn=bc->current_function,
                .i = bc->current_function->instructions_cnt++
        };
}

void patch_instruction(instruction_position_t ref, instruction_t * ins) {
        ref.fn->instructions[ref.i] = ins;
}

uint64_t next_instruction_index(bcfile_t * bc) {
        return bc->current_function->instructions_cnt;
}

#pragma endregion insert_instructions

#pragma region envs

bc_environment_t * get_current_env(bcfile_t * bc) {
        return bc->envs;
}

void env_begin(bcfile_t * bc, bc_environment_t * parent) {
        bc_environment_t * e = malloc(sizeof(bc_environment_t));
        if (e == NULL) {
                fprintf(stderr, "hjk\n");
                exit(11);
        }
        *e = (bc_environment_t) {
                .parent = parent,
                .names_size = 0,
                .scoped = true
        };
        bc->envs = e;
}
void env_end(bcfile_t * bc) {
        bc_environment_t * e = bc->envs;
        bc->envs = e->parent;
        free(e);
}

bool env_is_global(bcfile_t * bc) {
        return bc->envs->scoped == false;
}

void set_current_env(bcfile_t * bc, bc_environment_t * env) {
        bc->envs = env;
}

#include <lib/str.h>
uint16_t declare_local(bcfile_t * bc, const uint8_t * name, uint64_t name_len) {
        bc->envs->names[bc->envs->names_size] = (Str) {
                .str = name,
                .len = name_len
        };
        // fprintf(stderr, "New local: %ld\n", bc->envs->local_cnt);
        uint64_t local = bc->current_function->locals++;
        bc->envs->ids[bc->envs->names_size++] = local;
        return local;
}

uint16_t declare_opaque_local(bcfile_t * bc) {
        return bc->current_function->locals++;
}

uint16_t find_local_in_env(bc_environment_t * env, const uint8_t * name, uint64_t name_len, bool * success) {
        size_t i;
        Str name_str = (Str) {
                .str = name,
                .len = name_len
        };
        if (success == NULL) {
                fprintf(stderr, "hjk\n");
                exit(11);
        }
        for (i = 0; i < env->names_size; i++) {
                if (str_eq(env->names[i], name_str)) {
                        *success = true;
                        return env->ids[i];
                }
        }
        *success = false;
        return -1;
}

uint16_t find_local(bcfile_t * bc, const uint8_t * name, uint64_t name_len, bool * success) {
        bool succ;
        bc_environment_t * env = bc->envs;
        if (success == NULL) {
                fprintf(stderr, "hjk\n");
                exit(11);
        }
        while (env) {
                uint16_t local = find_local_in_env(env, name, name_len, &succ);
                if (succ) {
                        // fprintf(stderr, "Found local: %d\n", local);
                        *success = true;
                        return local;
                }
                env = env->parent;
        }
        *success = false;
        return -1;
}

const_pool_id_t access_global(bcfile_t * bc, const uint8_t * name, uint64_t name_len) {
        size_t i;
        const_pool_id_t name_str = new_constant_string(bc, name, name_len);
        for (i = 0; i < bc->globals_count; i++) {
                if (bc->globals[i] == name_str) {
                        return name_str;
                }
        }
        bc->globals[bc->globals_count++] = name_str;
        return name_str;
}

#pragma endregion envs

#pragma region serialization_functions

const char * error_string(bcfile_t * bc) {
        (void)bc;
        return "ERROR";
}

uint8_t get_status(bcfile_t * bc) {
        return bc->status;
}

void print_init(bcfile_t * bc) {
        bc->status = INPROGRESS;
        bc->constant_index = 0;
        bc->instruction_index = 0;
        bc->stage = 0;
}

void bc_print(bcfile_t * bc, FILE * outstream) {
        uint8_t out_buffer[0xFFFF];
        print_init(bc);
        uint64_t compiled = 0;
        while(get_status(bc) == INPROGRESS) {
                compiled = print_batch(bc, out_buffer, 0xFFFF);
                // fprintf(stderr, "batch: %ld\n", compiled);
                if (compiled > 0xFFFF) {
                        exit(66);
                }
                if (get_status(bc) == FAILURE) {
                        exit(99);
                }
                uint64_t i;
                for (i = 0; i < compiled; i++) {
                        fputc(out_buffer[i], outstream);
                }
        }
}

#define PUTB(x) buffer[written++] = (x & 0xFF);
#define PUTBB(x) buffer[written++] = (x & 0xFF); buffer[written++] = ((x >> 8) & 0xFF);
#define PUTBBBB(x) buffer[written++] = (x & 0xFF); buffer[written++] = ((x >> 8) & 0xFF); buffer[written++] = ((x >> 16) & 0xFF); buffer[written++] = ((x >> 24) & 0xFF);

int64_t inslen(function_body_t * fn, uint16_t begin, uint16_t end) {
        uint64_t size = 0;
        uint64_t i;
        for (i = begin; i < end; i++) {
                size += fn->instructions[i]->size;
        }
        // fprintf(stderr, "inslen: %d <-> %d\n", begin, end);
        // fprintf(stderr, "inslen: %ld\n", size);
        return size;
}
int64_t insoffset(function_body_t * fn, uint16_t ins, int16_t offset) {
        uint16_t begin = (ins + 1) + (offset < 0 ? offset : 0);
        uint16_t end = (ins + 1) + (offset > 0 ? offset : 0);
        // fprintf(stderr, "insoffset: %d + %d\n", ins, offset);
        // fprintf(stderr, "inslen: %d <-> %d\n", begin, end);
        return inslen(fn, begin, end) * ((-2 * (offset < 0))+1);
}

uint64_t print_instruction(bcfile_t * bc, uint8_t * buffer, uint64_t buffer_size) {
        (void)buffer_size;
        uint64_t written = 0;
        function_body_t * fn = (function_body_t *)((compiler_const_pool_function_t *) bc->const_pool[bc->constant_index])->value;
        uint16_t insid = bc->instruction_index++;
        if (insid >= fn->instructions_cnt) {
                bc->constant_index++;
                bc->instruction_index = 0;
                return written;
        }
        if (insid == 0) {
                fprintf(stderr, " - FUN\n");
                if (fn->is_entry) {
                        bc->entry_fn = bc->constant_index;
                }
                PUTB(0x03);
                PUTB(fn->params);
                PUTBB(fn->locals);
                PUTBBBB(inslen(fn, 0, fn->instructions_cnt));
        }
        instruction_t * rins = fn->instructions[insid];
        switch (rins->kind) {
                break; case BCINS_DROP: {
                        fprintf(stderr, " -  - DROP\n");
                        PUTB(0x00);
                }
                break; case BCINS_CONST: {
                        fprintf(stderr, " -  - CONST\n");
                        PUTB(0x01);
                        instruction_get_constant_t * ins = (instruction_get_constant_t *)rins;
                        PUTBB(ins->id);
                }
                break; case BCINS_PRINT: {
                        fprintf(stderr, " -  - PRINT\n");
                        PUTB(0x02);
                        instruction_print_t * ins = (instruction_print_t *)rins;
                        PUTBB(ins->fmt);
                        PUTB(ins->argc);
                }
                break; case BCINS_ARRAY: {
                        fprintf(stderr, " -  - ARRAY\n");
                        PUTB(0x03);
                }
                break; case BCINS_OBJECT: {
                        fprintf(stderr, " -  - OBJECT\n");
                        PUTB(0x04);
                        instruction_object_t * ins = (instruction_object_t *)rins;
                        PUTBB(ins->cls);
                }
                break; case BCINS_GETFIELD: {
                        fprintf(stderr, " -  - GETFIELD\n");
                        PUTB(0x05);
                        instruction_get_field_t * ins = (instruction_get_field_t *)rins;
                        PUTBB(ins->id);
                }
                break; case BCINS_SETFIELD: {
                        fprintf(stderr, " -  - SETFIELD\n");
                        PUTB(0x06);
                        instruction_set_field_t * ins = (instruction_set_field_t *)rins;
                        PUTBB(ins->id);
                }
                break; case BCINS_CALLMET: {
                        fprintf(stderr, " -  - CALLMET\n");
                        PUTB(0x07);
                        instruction_call_method_t * ins = (instruction_call_method_t *)rins;
                        PUTBB(ins->name);
                        PUTB(ins->argc);
                }
                break; case BCINS_CALLFUN: {
                        fprintf(stderr, " -  - CALLFUN\n");
                        PUTB(0x08);
                        instruction_call_function_t * ins = (instruction_call_function_t *)rins;
                        PUTB(ins->argc);
                }
                break; case BCINS_SETLOCAL: {
                        fprintf(stderr, " -  - SETLOCAL\n");
                        PUTB(0x09);
                        instruction_set_local_t * ins = (instruction_set_local_t *)rins;
                        PUTBB(ins->local);
                }
                break; case BCINS_GETLOCAL: {
                        fprintf(stderr, " -  - GETLOCAL\n");
                        PUTB(0x0A);
                        instruction_get_local_t * ins = (instruction_get_local_t *)rins;
                        PUTBB(ins->local);
                }
                break; case BCINS_SETGLOBAL: {
                        fprintf(stderr, " -  - SETGLOBAL\n");
                        PUTB(0x0B);
                        instruction_set_global_t * ins = (instruction_set_global_t *)rins;
                        PUTBB(ins->global);
                }
                break; case BCINS_GETGLOBAL: {
                        fprintf(stderr, " -  - GETGLOBAL\n");
                        PUTB(0x0C);
                        instruction_get_global_t * ins = (instruction_get_global_t *)rins;
                        PUTBB(ins->global);
                }
                break; case BCINS_BRANCH: {
                        PUTB(0x0D);
                        instruction_branch_t * ins = (instruction_branch_t *)rins;
                        int64_t off = insoffset(fn,insid,ins->off);
                        fprintf(stderr, " -  - BRANCH: %ld\n", off);

                        PUTBB(off);
                }
                break; case BCINS_JUMP: {
                        PUTB(0x0E);
                        instruction_jump_t * ins = (instruction_jump_t *)rins;
                        int64_t off = insoffset(fn,insid,ins->off);
                        fprintf(stderr, " -  - JUMP: %ld\n", off);
                        PUTBB(off);
                }
                break; case BCINS_RETURN: {
                        fprintf(stderr, " -  - RETURN\n");
                        PUTB(0x0F);
                }
                break; case BCINS_DUMMY: {
                        fprintf(stderr, " -  - DUMMY\n");
                        exit (77);
                }
        }
        return written;
}

uint64_t print_constant(bcfile_t * bc, uint8_t * buffer, uint64_t buffer_size) {
        (void)buffer_size;
        uint64_t written = 0;
        if (bc->constant_index >= bc->const_pool_size) {
                bc->stage++;
                return written;
        }
        if (bc->constant_index == 0 && bc->instruction_index == 0) {
                PUTBB(bc->const_pool_size);
        }
        compiler_const_pool_item_t * rcnst = bc->const_pool[bc->constant_index];
        switch (rcnst->kind) {
                break; case BCCONST_NUL: {
                        fprintf(stderr, " - NULL\n");
                        PUTB(0x01);
                        bc->constant_index++;
                }
                break; case BCCONST_INT: {
                        fprintf(stderr, " - INT\n");
                        PUTB(0x00);
                        compiler_const_pool_number_t * cnst = (compiler_const_pool_number_t *)rcnst;
                        PUTBBBB(cnst->value);
                        bc->constant_index++;
                }
                break; case BCCONST_BUL: {
                        fprintf(stderr, " - BOOL\n");
                        PUTB(0x04);
                        compiler_const_pool_bool_t * cnst = (compiler_const_pool_bool_t *)rcnst;
                        PUTB(cnst->value ? 0x01 : 0x00);
                        bc->constant_index++;
                }
                break; case BCCONST_STR: {
                        fprintf(stderr, " - STR\n");
                        PUTB(0x02);
                        compiler_const_pool_string_t * cnst = (compiler_const_pool_string_t *)rcnst;
                        PUTBBBB(cnst->size);
                        uint64_t i;
                        for (i = 0; i < cnst->size; i++) {
                                PUTB(cnst->value[i]);
                        }
                        bc->constant_index++;
                }
                break; case BCCONST_CLS: {
                        fprintf(stderr, " - CLASS\n");
                        PUTB(0x05);
                        compiler_const_pool_class_t * cnst = (compiler_const_pool_class_t *)rcnst;
                        PUTBB(cnst->size);
                        uint64_t i;
                        for (i = 0; i < cnst->size; i++) {
                                PUTBB(cnst->value[i]);
                        }
                        bc->constant_index++;
                }
                break; case BCCONST_FUN: {
                        written += print_instruction(bc, buffer + written, buffer_size - written);
                }
        }
        return written;
}

uint64_t print_batch(bcfile_t * bc, uint8_t * buffer, uint64_t buffer_size) {
        uint64_t written = 0;
        switch (bc->stage) {
                // Header
                break; case 0: {
                        fprintf(stderr, "Header\n");
                        PUTB(0x46);
                        PUTB(0x4D);
                        PUTB(0x4C);
                        PUTB(0x0A);
                        bc->stage++;
                }
                // Constants
                break; case 1: {
                        fprintf(stderr, "Constants\n");
                        written += print_constant(bc, buffer + written, buffer_size - written);
                }
                // Globals
                break; case 2: {
                        fprintf(stderr, "Globals\n");
                        uint64_t i;
                        PUTBB(bc->globals_count);
                        for (i = 0; i < bc->globals_count; i++) {
                                PUTBB(bc->globals[i]);
                        }
                        bc->stage++;
                }
                // Entry
                break; case 3: {
                        fprintf(stderr, "Entry\n");
                        PUTBB(bc->entry_fn);
                        bc->status = DONE;
                }
                break; default: {
                        bc->status = FAILURE;
                }
        }
        return written;
}

#pragma endregion serialization_functions
