#include "bc_compile.h"

#include <stdio.h>

#include "bc_interpret/types.h"
#include "debuglog.h"

#include "arena.h"
#include "parser.h"
#include "load_file.h"
#include "with.h"

#include "bc_file.h"

#include "str.h"

#pragma region comile_fn_main

// Stage 1
void compile_top(AstTop * node, bcfile_t * bc);
void compile_block(AstBlock * node, bcfile_t * bc);
void compile_print(AstPrint * node, bcfile_t * bc);
void compile_null(AstNull * node, bcfile_t * bc);
void compile_integer(AstInteger * node, bcfile_t * bc);
void compile_boolean(AstBoolean * node, bcfile_t * bc);
void compile_function(AstFunction * node, bcfile_t * bc);
void compile_function_call(AstFunctionCall * node, bcfile_t * bc);
void compile_definition(AstDefinition * node, bcfile_t * bc);
void compile_variable_access(AstVariableAccess * node, bcfile_t * bc);
void compile_variable_assignment(AstVariableAssignment * node, bcfile_t * bc);
// Stage 2
void compile_object(AstObject * node, bcfile_t * bc);
void compile_field_access(AstFieldAccess * node, bcfile_t * bc);
void compile_field_assignment(AstFieldAssignment * node, bcfile_t * bc);
void compile_method_call(AstMethodCall * node, bcfile_t * bc);
void compile_conditional(AstConditional * node, bcfile_t * bc);
void compile_loop(AstLoop * node, bcfile_t * bc);
// Stage 3
void compile_array(AstArray * node, bcfile_t * bc);
void compile_index_assignment(AstIndexAssignment * node, bcfile_t * bc);
void compile_index_access(AstIndexAccess * node, bcfile_t * bc);

void compile(bcfile_t * bc, Ast * ast) {
        if (ast == NULL) return;
        switch (ast->kind) {
                break; case AST_VARIABLE_ACCESS:     compile_variable_access((AstVariableAccess *) ast, bc); return;
                break; case AST_FIELD_ACCESS:        compile_field_access((AstFieldAccess *) ast, bc); return;
                break; case AST_INDEX_ACCESS:        compile_index_access((AstIndexAccess *) ast, bc); return;
                break; case AST_FUNCTION_CALL:       compile_function_call((AstFunctionCall *) ast, bc); return;
                break; case AST_METHOD_CALL:         compile_method_call((AstMethodCall *) ast, bc); return;
                break; case AST_VARIABLE_ASSIGNMENT: compile_variable_assignment((AstVariableAssignment *) ast, bc); return;
                break; case AST_FIELD_ASSIGNMENT:    compile_field_assignment((AstFieldAssignment *) ast, bc); return;
                break; case AST_INDEX_ASSIGNMENT:    compile_index_assignment((AstIndexAssignment *) ast, bc); return;
                break; case AST_NULL:                compile_null((AstNull *) ast, bc); return;
                break; case AST_BOOLEAN:             compile_boolean((AstBoolean *) ast, bc); return;
                break; case AST_INTEGER:             compile_integer((AstInteger *) ast, bc); return;
                break; case AST_CONDITIONAL:         compile_conditional((AstConditional *) ast, bc); return;
                break; case AST_ARRAY:               compile_array((AstArray *) ast, bc); return;
                break; case AST_LOOP:                compile_loop((AstLoop *) ast, bc); return;
                break; case AST_FUNCTION:            compile_function((AstFunction *) ast, bc); return;
                break; case AST_BLOCK:               compile_block((AstBlock *) ast, bc); return;
                break; case AST_OBJECT:              compile_object((AstObject *) ast, bc); return;
                break; case AST_DEFINITION:          compile_definition((AstDefinition *) ast, bc); return;
                break; case AST_PRINT:               compile_print((AstPrint *) ast, bc); return;
                break; case AST_TOP:                 compile_top((AstTop *) ast, bc); return;
        }
        eprintf("unknown AST type\n");
}

#pragma endregion comile_fn_main

int bc_compile(const char *src)
{
        // fprintf(stderr, "Source: %s\n", src);
        Arena arena;
        char * file = NULL;
        int exit_code = 0;
        with(file = load_file(src), free(file)) {
        with(arena_init(&arena), arena_destroy(&arena)) {

                bcfile_t bc;
                bc_init(&bc);
                env_begin(&bc, NULL);
                get_current_env(&bc)->scoped = false;



                // fprintf(stderr, "Source: %s\n", src);
                Ast * ast = parse_src(&arena, Str_from_cstr(file));

                if (ast == NULL) {
                        eprintf("Failed to parse source: %s\n", src);
                        arena_destroy(&arena);
                        exit_with(exit_code = 1);
                }

                compile(&bc, ast);
                add_instruction(&bc, new_instruction_return());
                env_end(&bc);
                fprintf(stderr, "--------\n");
                bc_print(&bc, stdout);
        }}

        return exit_code;
}

#pragma region stage1_impl

#pragma region literals

void compile_null(AstNull * node, bcfile_t * bc) {
        fprintf(stderr, "Compile null\n");
        (void)node;
        add_instruction(bc,
                new_instruction_get_constant(
                        new_constant_null(bc)
                )
        );
}
void compile_integer(AstInteger * node, bcfile_t * bc) {
        fprintf(stderr, "Compile integer\n");
        add_instruction(bc,
                new_instruction_get_constant(
                        new_constant_integer(bc, node->value)
                )
        );
}
void compile_boolean(AstBoolean * node, bcfile_t * bc) {
        fprintf(stderr, "Compile bool\n");
        add_instruction(bc,
                new_instruction_get_constant(
                        new_constant_bool(bc, node->value)
                )
        );
}

#pragma endregion literals

void compile_stmt_array(bcfile_t * bc, Ast ** stmt_arr, size_t stmt_cnt, bool do_drop) {
        fprintf(stderr, "Compile stmtarray\n");
        size_t i;
        for (i = 0; i < stmt_cnt; i++) {
                if (i != 0 && do_drop) {
                        add_instruction(bc, new_instruction_drop());
                }
                compile(bc, stmt_arr[i]);
        }
}

void compile_top(AstTop * node, bcfile_t * bc) {
        fprintf(stderr, "Compile top\n");
        compile_stmt_array(bc, node->expressions, node->expression_cnt, true);
}

void compile_block(AstBlock * node, bcfile_t * bc) {
        fprintf(stderr, "Compile block\n");
        env_begin(bc, get_current_env(bc));
        compile_stmt_array(bc, node->expressions, node->expression_cnt, true);
        env_end(bc);
}

void compile_definition(AstDefinition * node, bcfile_t * bc) {
        fprintf(stderr, "Compile definition\n");
        compile(bc, node->value);
        if (env_is_global(bc)) {
                add_instruction(bc, new_instruction_set_global(access_global(bc, node->name.str, node->name.len)));
        }
        else {
                add_instruction(bc, new_instruction_set_local(declare_local(bc, node->name.str, node->name.len)));
        }
}

void compile_print(AstPrint * node, bcfile_t * bc) {
        fprintf(stderr, "Compile print\n");
        compile_stmt_array(bc, node->arguments, node->argument_cnt, false);
        add_instruction(bc, new_instruction_print(new_constant_string(bc, node->format.str, node->format.len), node->argument_cnt));
}

void compile_variable_access(AstVariableAccess * node, bcfile_t * bc) {
        fprintf(stderr, "Compile var access\n");
        bool success = false;
        uint16_t local = find_local(bc, node->name.str, node->name.len, &success);
        if (success) {
                add_instruction(bc, new_instruction_get_local(local));
        }
        else {
                add_instruction(bc, new_instruction_get_global(access_global(bc, node->name.str, node->name.len)));
        }
}

void compile_variable_assignment(AstVariableAssignment * node, bcfile_t * bc) {
        fprintf(stderr, "Compile var asignment\n");
        bool success = false;
        uint16_t local = find_local(bc, node->name.str, node->name.len, &success);
        compile(bc, node->value);
        if (success) {
                add_instruction(bc, new_instruction_set_local(local));
        }
        else {
                add_instruction(bc, new_instruction_set_global(access_global(bc, node->name.str, node->name.len)));
        }
}

void compile_function_call(AstFunctionCall * node, bcfile_t * bc) {
        fprintf(stderr, "Compile function call\n");
        compile(bc, node->function);
        compile_stmt_array(bc, node->arguments, node->argument_cnt, false);
        add_instruction(bc, new_instruction_call_function(node->argument_cnt));
}

void compile_function(AstFunction * node, bcfile_t * bc) {
        fprintf(stderr, "Compile function\n");
        function_body_t * fn = malloc(sizeof(function_body_t));
        if (fn == NULL) {
                fprintf(stderr, "hjk\n");
                exit(11);
        }
        *fn = (function_body_t) {
                .params = node->parameter_cnt + 1,
                .locals = 0,
                .instructions_cnt = 0,
                .is_entry = false
        };
        function_body_t * parent_function = bc->current_function;
        bc->current_function = fn;
        bc_environment_t * env = get_current_env(bc);
        // reset localcnt
        env_begin(bc, NULL);
        declare_this(bc);
        size_t i;
        for(i = 0; i < node->parameter_cnt; i++) {
                declare_local(bc, node->parameters[i].str, node->parameters[i].len);
        }
        compile(bc, node->body);
        env_end(bc);
        set_current_env(bc, env);
        add_instruction(bc, new_instruction_return());
        bc->current_function = parent_function;

        add_instruction(bc,
                new_instruction_get_constant(
                        new_constant_function(bc, fn)
                )
        );
}

#pragma endregion stage1_impl

#pragma region stage2_impl

void compile_object(AstObject * node, bcfile_t * bc) {
        fprintf(stderr, "Compile object\n");
        compile(bc, node->extends);

        uint32_t members_cnt = 0;
        const_pool_id_t members[5000];

        size_t i;
        for (i = 0; i < node->member_cnt; i++) {
                AstDefinition * field = (AstDefinition *) node->members[i]; // TODO: check if true
                members[members_cnt++] = new_constant_string(bc, field->name.str, field->name.len);
                env_begin(bc, bc->envs);
                compile(bc, field->value);
                env_end(bc);
        }

        add_instruction(bc, new_instruction_object(new_constant_class(bc, members, members_cnt)));
}

void compile_field_access(AstFieldAccess * node, bcfile_t * bc) {
        fprintf(stderr, "Compile field access\n");
        compile(bc, node->object);
        add_instruction(bc, new_instruction_get_field(new_constant_string(bc, node->field.str, node->field.len)));
}

void compile_field_assignment(AstFieldAssignment * node, bcfile_t * bc) {
        fprintf(stderr, "Compile field assignment\n");
        compile(bc, node->object);
        compile(bc, node->value);
        add_instruction(bc, new_instruction_set_field(new_constant_string(bc, node->field.str, node->field.len)));
}

void compile_method_call(AstMethodCall * node, bcfile_t * bc) {
        fprintf(stderr, "Compile method call\n");
        compile(bc, node->object);
        size_t i;
        for (i = 0; i < node->argument_cnt; i++) {
                compile(bc, node->arguments[i]);
        }
        add_instruction(bc, new_instruction_call_method(new_constant_string(bc, node->name.str, node->name.len), node->argument_cnt + 1));
}

void compile_conditional(AstConditional * node, bcfile_t * bc) {
        fprintf(stderr, "Compile conditional\n");
        compile(bc, node->condition);
        instruction_position_t branch_to_if_branch_dumy = add_instruction(bc, new_instruction_dummy());
        env_begin(bc, bc->envs);
        compile(bc, node->alternative);
        env_end(bc);
        instruction_position_t jump_over_else_branch_dumy = add_instruction(bc, new_instruction_dummy());
        patch_instruction(branch_to_if_branch_dumy, new_instruction_branch((int64_t)next_instruction_index(bc) - (int64_t)branch_to_if_branch_dumy.i - (int64_t)1));
        env_begin(bc, bc->envs);
        compile(bc, node->consequent);
        env_end(bc);
        patch_instruction(jump_over_else_branch_dumy, new_instruction_jump((int64_t)next_instruction_index(bc) - (int64_t)jump_over_else_branch_dumy.i - (int64_t)1));
}
void compile_loop(AstLoop * node, bcfile_t * bc) {
        fprintf(stderr, "Compile loop\n");
        add_instruction(bc,
                new_instruction_get_constant(
                        new_constant_null(bc)
                )
        );
        instruction_position_t jump_to_test = add_instruction(bc, new_instruction_dummy());
        uint64_t do_ip = next_instruction_index(bc);
        add_instruction(bc, new_instruction_drop());
        env_begin(bc, bc->envs);
        compile(bc, node->body);
        env_end(bc);
        int64_t off = (int64_t)next_instruction_index(bc) - (int64_t)jump_to_test.i - (int64_t)1;
        patch_instruction(jump_to_test, new_instruction_jump(off));
        fprintf(stderr, "JMP: %ld\n", off);
        compile(bc, node->condition);
        instruction_branch_t * ins = (instruction_branch_t *) new_instruction_branch((int64_t)do_ip - (int64_t)next_instruction_index(bc) - (int64_t)1);
        add_instruction(bc, (instruction_t *)ins);
        fprintf(stderr, "BRANCH: %d\n", ins->off);
}

#pragma endregion stage2_impl

#pragma region stage3_impl

void compile_array(AstArray * node, bcfile_t * bc) {
        fprintf(stderr, "Compile array\n");
        uint16_t size = declare_opaque_local(bc);
        uint16_t i = declare_opaque_local(bc);
        uint16_t arr = declare_opaque_local(bc);

        // Save size
        compile(bc, node->size);
        add_instruction(bc, new_instruction_set_local(size));
        add_instruction(bc, new_instruction_drop());

        // Create ARR
        add_instruction(bc, new_instruction_get_local(size));
        add_instruction(bc, new_instruction_get_constant(new_constant_null(bc)));
        add_instruction(bc, new_instruction_array());
        add_instruction(bc, new_instruction_set_local(arr));
        add_instruction(bc, new_instruction_drop());

        // int i = 0
        add_instruction(bc, new_instruction_get_constant(new_constant_integer(bc, 0)));
        add_instruction(bc, new_instruction_set_local(i));
        add_instruction(bc, new_instruction_drop());

        // i < size
        instruction_position_t cond = add_instruction(bc, new_instruction_get_local(i));
        add_instruction(bc, new_instruction_get_local(size));
        add_instruction(bc, new_instruction_call_method(new_constant_string(bc, (const uint8_t *)"<", 1), 2));
        instruction_position_t branch_to_body = add_instruction(bc, new_instruction_dummy());
        instruction_position_t jump_to_end = add_instruction(bc, new_instruction_dummy());

        // i++
        instruction_position_t inc = add_instruction(bc, new_instruction_get_local(i));
        add_instruction(bc, new_instruction_get_constant(new_constant_integer(bc, 1)));
        add_instruction(bc, new_instruction_call_method(new_constant_string(bc, (const uint8_t *)"+", 1), 2));
        add_instruction(bc, new_instruction_set_local(i));
        add_instruction(bc, new_instruction_drop());
        instruction_position_t jump_to_cond = add_instruction(bc, new_instruction_dummy());

        // body
        instruction_position_t body = add_instruction(bc, new_instruction_get_local(arr));
        add_instruction(bc, new_instruction_get_local(i));
        env_begin(bc, bc->envs);
        compile(bc, node->initializer);
        env_end(bc);
        add_instruction(bc, new_instruction_call_method(new_constant_string(bc, (const uint8_t *)"set", 3), 3));
        add_instruction(bc, new_instruction_drop());
        instruction_position_t jump_to_inc = add_instruction(bc, new_instruction_dummy());

        // end
        uint64_t end = next_instruction_index(bc);

        // patch together
        patch_instruction(branch_to_body, new_instruction_branch((int64_t)body.i - (int64_t)branch_to_body.i - (int64_t)1));
        patch_instruction(jump_to_end, new_instruction_branch((int64_t)end - (int64_t)jump_to_end.i - (int64_t)1));
        patch_instruction(jump_to_cond, new_instruction_branch((int64_t)cond.i - (int64_t)jump_to_cond.i - (int64_t)1));
        patch_instruction(jump_to_inc, new_instruction_branch((int64_t)inc.i - (int64_t)jump_to_inc.i - (int64_t)1));
}
void compile_index_assignment(AstIndexAssignment * node, bcfile_t * bc) {
        fprintf(stderr, "Compile index assignment\n");
        compile(bc, node->object);
        compile(bc, node->index);
        compile(bc, node->value);
        add_instruction(bc, new_instruction_call_method(new_constant_string(bc, (const uint8_t *)"set", 3), 3));
}
void compile_index_access(AstIndexAccess * node, bcfile_t * bc) {
        fprintf(stderr, "Compile index access\n");
        compile(bc, node->object);
        compile(bc, node->index);
        add_instruction(bc, new_instruction_call_method(new_constant_string(bc, (const uint8_t *)"get", 3), 2));
}

#pragma endregion stage3_impl
