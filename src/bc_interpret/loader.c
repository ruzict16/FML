#include "loader.h"

#include "common.h"
#include <stdio.h>

byte_t * load_bc(const char *filename, size_t * loaded_bytes) {
        FILE *fileptr;
        byte_t *buffer;
        byte_t *result = NULL;
        long filelen;

        fileptr = fopen(filename, "rb"); // Open the file in binary mode
        fseek(fileptr, 0, SEEK_END);     // Jump to the end of the file
        filelen = ftell(fileptr);        // Get the current byte offset in the file
        rewind(fileptr);                 // Jump back to the beginning of the file

        buffer = (byte_t *)malloc(filelen * sizeof(char)); // Enough memory for the file
        if(fread(buffer, filelen, 1, fileptr)) { // Read in the entire file
                (*loaded_bytes) = filelen;
                result = buffer;
        }
        fclose(fileptr); // Close the file
        return result;
}
