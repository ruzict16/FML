#include "builtin_functions.h"

#include <string.h>

#include "bc_interpret/const_pool.h"
#include "bc_interpret/runtime_types.h"
#include "heap.h"
#include "debuglog.h"
#include "str.h"

object_id bltn_eq(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        // debug("bltn_eq begin:\n");
        object_id res = alloc_boolean();
        bool_t * res_b = (bool_t *)get_object_ptr(res);

        // debug(" - bltn_eq kinds:\n");
        // debug(" -> ");
        // print_kind(get_kind(a));
        // debug(" -> ");
        // print_kind(get_kind(b));

        if (a == b)                       { goto res_true;  }
        if (get_kind(a) != get_kind(b))   { goto res_false; }
        if (get_kind(a) == null_kind)     { goto res_true;  }
        if (get_kind(a) == bool_kind) {
                debug("::DEBUG::  - bltn_eq bool: %d == %d\n", ((bool_t *)get_object_ptr(a))->value, ((bool_t *)get_object_ptr(b))->value);
                if (((bool_t *)get_object_ptr(a))->value == ((bool_t *)get_object_ptr(b))->value) {
                        goto res_true;
                }
                goto res_false;
        }
        if (get_kind(a) == int_kind) {
                debug("::DEBUG::  - bltn_eq int: %d == %d\n", ((int_t *)get_object_ptr(a))->value, ((int_t *)get_object_ptr(b))->value);
                if (((int_t *)get_object_ptr(a))->value == ((int_t *)get_object_ptr(b))->value) {
                        goto res_true;
                }
                goto res_false;
        }

        res_false:
                // debug("bltn_eq false\n");
                res_b->value = false;
                goto ret;

        res_true:
                // debug("bltn_eq true\n");
                res_b->value = true;

        ret:
                // debug("bltn_eq done\n");
                return res;
}

object_id bltn_neq(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_neq\n");
        object_id res = bltn_eq(a, b, unused);
        bool_t * res_b = (bool_t *)get_object_ptr(res);
        res_b->value = !res_b->value;
        debug("::DEBUG:: bltn_neq done\n");
        return res;
}

object_id bltn_bool_and(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_bool_and\n");
        object_id res = alloc_boolean();
        debug("::DEBUG:: bltn_bool_and B: %d\n", res);
        bool_t * res_b = (bool_t *)get_object_ptr(res);
        debug("::DEBUG:: bltn_bool_and C: %p\n", (void *) res_b);
        res_b->value = ((bool_t*)get_object_ptr(a))->value && ((bool_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_bool_and done\n");
        return res;
}

object_id bltn_bool_or(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_bool_or\n");
        object_id res = alloc_boolean();
        bool_t * res_b = (bool_t *)get_object_ptr(res);
        res_b->value = ((bool_t*)get_object_ptr(a))->value || ((bool_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_bool_or done\n");
        return res;
}

object_id bltn_plus(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_plus\n");
        // eprintf("::DEBUG:: bltn_plus (%d + %d) ==> (%d + %d)\n", a, b, ((int_t*)get_object_ptr(a))->value, ((int_t*)get_object_ptr(b))->value);
        object_id res = alloc_integer();
        // eprintf("::DEBUG:: bltn_plus again (%d + %d) ==> (%d + %d)\n", a, b, ((int_t*)get_object_ptr(a))->value, ((int_t*)get_object_ptr(b))->value);
        int_t * res_i = (int_t *)get_object_ptr(res);
        res_i->value = ((int_t*)get_object_ptr(a))->value + ((int_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_plus done\n");
        return res;
}

object_id bltn_minus(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_minus\n");
        // eprintf("::DEBUG:: bltn_minus\n");
        object_id res = alloc_integer();
        int_t * res_i = (int_t *)get_object_ptr(res);
        res_i->value = ((int_t*)get_object_ptr(a))->value - ((int_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_minus done\n");
        return res;
}

object_id bltn_times(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_times\n");
        object_id res = alloc_integer();
        int_t * res_i = (int_t *)get_object_ptr(res);
        res_i->value = ((int_t*)get_object_ptr(a))->value * ((int_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_times done\n");
        return res;
}

object_id bltn_div(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_div\n");
        object_id res = alloc_integer();
        int_t * res_i = (int_t *)get_object_ptr(res);
        res_i->value = ((int_t*)get_object_ptr(a))->value / ((int_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_div done\n");
        return res;
}

object_id bltn_mod(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_mod\n");
        object_id res = alloc_integer();
        int_t * res_i = (int_t *)get_object_ptr(res);
        res_i->value = ((int_t*)get_object_ptr(a))->value % ((int_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_mod done\n");
        return res;
}

object_id bltn_lt(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_lt\n");
        object_id res = alloc_boolean();
        bool_t * res_b = (bool_t *)get_object_ptr(res);
        res_b->value = ((int_t*)get_object_ptr(a))->value < ((int_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_lt done\n");
        return res;
}

object_id bltn_leq(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_leq\n");
        object_id res = alloc_boolean();
        bool_t * res_b = (bool_t *)get_object_ptr(res);
        res_b->value = ((int_t*)get_object_ptr(a))->value <= ((int_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_leq done\n");
        return res;
}

object_id bltn_gt(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_gt\n");
        object_id res = alloc_boolean();
        bool_t * res_b = (bool_t *)get_object_ptr(res);
        res_b->value = ((int_t*)get_object_ptr(a))->value > ((int_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_gt done\n");
        return res;
}

object_id bltn_geq(object_id a, object_id b, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_geq\n");
        object_id res = alloc_boolean();
        bool_t * res_b = (bool_t *)get_object_ptr(res);
        res_b->value = ((int_t*)get_object_ptr(a))->value >= ((int_t*)get_object_ptr(b))->value;
        debug("::DEBUG:: bltn_geq done\n");
        return res;
}

object_id bltn_get(object_id a, object_id i, object_id unused) {
        is_used(unused); // I know

        debug("::DEBUG:: bltn_get\n");
        // TODO: check bounds
        if (a == 0) {
                debug("::DEBUG:: bltn_get: a NULL\n");
                exit(32);
        }
        if (i == 0) {
                debug("::DEBUG:: bltn_get: i NULL\n");
                exit(33);
        }
        if ((int64_t)((arr_t*)get_object_ptr(a))->size <= ((int_t*)get_object_ptr(i))->value) {
                debug("::DEBUG:: bltn_get: bounds: %d\n", ((int_t*)get_object_ptr(i))->value);
                exit(34);
        }
        object_id res = ((arr_t*)get_object_ptr(a))->values[((int_t*)get_object_ptr(i))->value];
        return res;
}

object_id bltn_set(object_id a, object_id v, object_id i) {
        debug("::DEBUG:: bltn_set\n");
        // TODO: check bounds
        if (a == 0) {
                debug("::DEBUG:: bltn_set: a NULL\n");
                exit(35);
        }
        if (i == 0) {
                debug("::DEBUG:: bltn_set: i NULL\n");
                exit(36);
        }
        if ((int64_t)((arr_t*)get_object_ptr(a))->size <= ((int_t*)get_object_ptr(i))->value) {
                debug("::DEBUG:: bltn_set: bounds: %d, size: %ld\n", ((int_t*)get_object_ptr(i))->value, (int64_t)((arr_t*)get_object_ptr(a))->size);
                exit(37);
        }
        ((arr_t*)get_object_ptr(a))->values[((int_t*)get_object_ptr(i))->value] = v;
        object_id res = ((arr_t*)get_object_ptr(a))->values[((int_t*)get_object_ptr(i))->value];
        return res;
}


































typedef struct builtin_t {
        bool exists;
        const char * name;
        constant_pool_index_t name_id;
        uint8_t arg_cnt;
        bltn_fn_ptr bltn;
} builtin_t;

builtin_t builtins[] = {
        {
                // eq
                .exists = false,
                .name = "==",
                .name_id = 0,
                .bltn = bltn_eq
        },
        {
                // neq
                .exists = false,
                .name = "!=",
                .name_id = 0,
                .bltn = bltn_neq
        },
        {
                // bool_and
                .exists = false,
                .name = "&",
                .name_id = 0,
                .bltn = bltn_bool_and
        },
        {
                // bool_or
                .exists = false,
                .name = "|",
                .name_id = 0,
                .bltn = bltn_bool_or
        },
        {
                // plus
                .exists = false,
                .name = "+",
                .name_id = 0,
                .bltn = bltn_plus
        },
        {
                // minus
                .exists = false,
                .name = "-",
                .name_id = 0,
                .bltn = bltn_minus
        },
        {
                // times
                .exists = false,
                .name = "*",
                .name_id = 0,
                .bltn = bltn_times
        },
        {
                // div
                .exists = false,
                .name = "/",
                .name_id = 0,
                .bltn = bltn_div
        },
        {
                // mod
                .exists = false,
                .name = "%",
                .name_id = 0,
                .bltn = bltn_mod
        },
        {
                // lt
                .exists = false,
                .name = "<",
                .name_id = 0,
                .bltn = bltn_lt
        },
        {
                // leq
                .exists = false,
                .name = "<=",
                .name_id = 0,
                .bltn = bltn_leq
        },
        {
                // gt
                .exists = false,
                .name = ">",
                .name_id = 0,
                .bltn = bltn_gt
        },
        {
                // geq
                .exists = false,
                .name = ">=",
                .name_id = 0,
                .bltn = bltn_geq
        },
        {
                // get
                .exists = false,
                .name = "get",
                .name_id = 0,
                .bltn = bltn_get
        },
        {
                // set
                .exists = false,
                .name = "set",
                .name_id = 0,
                .bltn = bltn_set
        },
};

const size_t builtins_cnt = sizeof(builtins) / sizeof(builtin_t);

void check_if_builtin_name(constant_pool_index_t id, uint32_t len, const char * str) {
        size_t i;
        for(i = 0; i < builtins_cnt; i++) {
                if (strlen(builtins[i].name) != len) {
                        continue;
                }
                if (strncmp(str, builtins[i].name, len) != 0) {
                        continue;
                }
                builtins[i].exists = true;
                builtins[i].name_id = id;
                debug("::BUILTIN:: Found builtin: %s == %.*s\n", builtins[i].name, len, str);
        }
}

size_t get_builtin_id(constant_pool_index_t id) {
        size_t i;
        for(i = 0; i < builtins_cnt; i++) {
                if (builtins[i].name_id == id) {
                        return i;
                }
        }
        print_const_pool();
        warn("::WARNING No builtin found (name %d)\n", id);
        return -1;
}

bltn_fn_ptr get_builtin_fn_ptr(size_t i) {
        #ifdef ENABLE_CHECKERS
        if (i >= builtins_cnt) {
                warn("::WARNING:: Accessing nonexistant builtin (fn_ptr %ld)\n", i);
        }
        #endif
        return builtins[i].bltn;
}
















#define my_printf(ofile, format, ...) fprintf(ofile, format __VA_OPT__(,) __VA_ARGS__); debug("::PRINT:: " format "\n" __VA_OPT__(,) __VA_ARGS__);

void bc_print_value(object_id v, FILE * ofile);
void bc_print_null(FILE * ofile) {
        debug("::PRINT:: - print_null\n");
        my_printf(ofile, "null");
}
void bc_print_integer(int_t * v, FILE * ofile) {
        debug("::PRINT:: - print_int\n");
        my_printf(ofile, "%d", v->value);
}
void bc_print_boolean(bool_t * v, FILE * ofile) {
        debug("::PRINT:: - print_bool\n");
        if (v->value) {
                my_printf(ofile, "true");
        }
        else {
                my_printf(ofile, "false");
        }
}
void bc_print_array(arr_t * v, FILE * ofile) {
        debug("::PRINT:: - print_array\n");
        if (v->size == 0) {
                my_printf(ofile, "[]");
                return;
        }
        size_t i;
        my_printf(ofile, "[");
        for (i = 0; i < v->size - 1; i++) {
                bc_print_value(v->values[i], ofile);
                my_printf(ofile, ", ");
        }
        bc_print_value(v->values[v->size - 1], ofile);
        my_printf(ofile, "]");
}

constant_pool_index_t curr_cls;
int compare_fields(const void * field_a, const void * field_b) {
        constant_pool_index_t field_name_a_id = get_class_fieldname ( curr_cls, *(uint16_t*)field_a );
        size_t                field_name_a_len = get_string_length ( field_name_a_id );
        const char *          field_name_a_data = get_string_data ( field_name_a_id  );
        constant_pool_index_t field_name_b_id = get_class_fieldname ( curr_cls, *(uint16_t*)field_b );
        size_t                field_name_b_len = get_string_length ( field_name_b_id );
        const char *          field_name_b_data = get_string_data ( field_name_b_id  );

        Str field_name_a = {
                .len=field_name_a_len,
                .str=(uint8_t*)field_name_a_data
        };
        Str field_name_b = {
                .len=field_name_b_len,
                .str=(uint8_t*)field_name_b_data
        };

        return str_cmp(field_name_a, field_name_b);
}
void bc_print_object(obj_t * v, FILE * ofile) {
        debug("::PRINT:: - print_object\n");
        // (void)v; my_printf("NOT_IMPLEMENTED: print_object");
        my_printf(ofile, "object(");
        size_t i;
        uint16_t fields_cnt = get_class_field_count(v->class);
        uint16_t * fields_ids = alloca(sizeof(uint16_t) * fields_cnt);
        for (i = 0; i < fields_cnt; i++) {
                fields_ids[i] = i;
        }

        // TODO: sort
        curr_cls = v->class;
        qsort (fields_ids, fields_cnt, sizeof(uint16_t), compare_fields);

        if (v->parent != 0 && get_kind(v->parent) != null_kind) {
                my_printf(ofile, "..=");
                bc_print_value(v->parent, ofile);
                if (fields_cnt > 0) {
                        my_printf(ofile, ", ");
                }
        }
        for (i = 0; i < fields_cnt; i++) {
                constant_pool_index_t field_name_id = get_class_fieldname ( v->class, fields_ids[i]  );
                size_t                field_name_len = get_string_length ( field_name_id );
                const char *          field_name_data = get_string_data ( field_name_id  );

                my_printf(ofile, "%.*s=", (int)field_name_len, field_name_data);

                bc_print_value(v->fields[fields_ids[i]], ofile);
                if (i != (uint16_t)(fields_cnt - 1)) {
                        my_printf(ofile, ", ");
                }
        }
        my_printf(ofile, ")");
}
void bc_print_function(fun_t * v, FILE * ofile) {
        debug("::PRINT:: - print_function\n");
        (void)v; my_printf(ofile, "function");
}

void bc_print_value(object_id v, FILE * ofile) {
        debug("::PRINT:: - print_value: %d\n", get_kind(v));
        #ifdef ENABLE_CHECKERS
        if (v == NULL) {warn("::WARNING:: Cannot print uninitialized object\n");exit(3);}
        #endif
        switch(get_kind(v)) {
                break; case null_kind: {
                        bc_print_null(ofile);
                }
                break; case int_kind: {
                        bc_print_integer((int_t *)get_object_ptr(v), ofile);
                }
                break; case bool_kind: {
                        bc_print_boolean((bool_t *)get_object_ptr(v), ofile);
                }
                break; case arr_kind: {
                        bc_print_array((arr_t *)get_object_ptr(v), ofile);
                }
                break; case obj_kind: {
                        bc_print_object((obj_t *)get_object_ptr(v), ofile);
                }
                break; case fun_kind: {
                        bc_print_function((fun_t *)get_object_ptr(v), ofile);
                }
        }
}


void bc_print_previous(const char * begin, const char * cur) {
        if (cur - begin <= 0) {
                // debug("::PRINT:: - actually do_print: NADA\n");
                return;
        }
        // debug("::PRINT:: - actually do_print: %.*s\n", (int)(cur - begin), begin);
        my_printf(stdout, "%.*s", (int)(cur - begin), begin);
}


void bltn_print(size_t format_len, const char * format, size_t argc, object_id* args) {
        debug("::PRINT:: do_print: %.*s\n", (int)format_len, format);

        size_t i = 0;
        size_t next_arg = 0;

        const char * begin = format;
        const char * cur = format;
        const char * end = format + format_len;

        for (;cur <= end;cur++) {
                if (*cur == '~') {
                        debug("::PRINT:: placeholder: %ld\n", next_arg);
                        bc_print_previous(begin, cur);
                        bc_print_value(args[next_arg++], stdout);
                        begin = cur + 1;
                        #ifdef ENABLE_CHECKERS
                        if(i++ >= argc) {
                                eprintf("Format expects more objects than provided\n");
                                exit(67);
                        }
                        #else
                        is_used(i);
                        is_used(argc);
                        #endif
                        continue;
                }
                if (*cur == '\\' && cur + 1 != end) {
                        debug("::PRINT:: escape\n");
                        bc_print_previous(begin, cur);
                        switch(*(cur + 1)) {
                                break; case 'n': {
                                        my_printf(stdout, "%c", 0x0A);
                                }
                                break; case 'r': {
                                        my_printf(stdout, "%c", 0x0D);
                                }
                                break; case 't': {
                                        my_printf(stdout, "%c", 0x09);
                                }
                                break; case '\\': {
                                        my_printf(stdout, "%c", 0x5C);
                                }
                                break; case '~': {
                                        my_printf(stdout, "%c", 0x7E);
                                }
                                break; case '"': {
                                        my_printf(stdout, "%c", 0x22);
                                }
                                break; default:
                                        eprintf("Unknown escape sequence/n");
                                        exit(77);
                        }
                        cur++;
                        begin = cur + 1;
                        continue;
                }
                debug("::PRINT:: pass\n");
        }

        bc_print_previous(begin, end);
        debug("::PRINT:: do_print done\n\n");
}
