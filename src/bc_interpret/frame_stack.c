#include "frame_stack.h"


#include "byte_helpers.h"
#include "const_pool.h"
#include "types.h"
#include "debuglog.h"


frame_t * frame_stack = NULL;

void frame_push(size_t args_n, size_t return_ptr) {
        debug("::FRAME:: push: %ld\n", args_n);
        frame_t * pre = frame_stack;
        // size_t args_n = get_function_local_count(foo);
        // args_n += get_function_param_count(foo);
        frame_stack = malloc(sizeof(frame_t) + (sizeof(base_object_t *) * args_n));
        // #ifdef ENABLE_CHECKERS
        {
                frame_stack->locals_cnt = args_n;
                size_t i;
                for(i = 0; i < args_n; i++) {
                        frame_stack->locals[i] = 0;
                }
        }
        // #endif
        frame_stack->previous_frame = pre;
        frame_stack->return_ptr = return_ptr;
}
void frame_pop() {
        debug("::FRAME:: pop\n");
        frame_t * pre = frame_stack->previous_frame;
        free(frame_stack);
        frame_stack = pre;
}

void frame_local_set(frame_index_t i, object_id val) {
        debug("::FRAME:: set local %d = %d\n", i, val ? get_kind(val) : 666);
        #ifdef ENABLE_CHECKERS
                if (i >= frame_stack->locals_cnt) {
                        warn("::WARNING:: frame_local set buffer overflow\n");
                }
                if (val == NULL) {
                        warn("::WARNING:: frame_local set == NULL\n");
                }
                if (get_kind(val) > 6) {
                        warn("::WARNING:: frame_local set %d\n", get_kind(val));
                }
        #endif
        frame_stack->locals[i] = val;
}

object_id frame_local_get(frame_index_t i) {
        debug("::FRAME:: get local %d\n", i);
        #ifdef ENABLE_CHECKERS
                if (i >= frame_stack->locals_cnt) {
                        warn("::WARNING:: frame_local get buffer overflow\n");
                }
                if (frame_stack->locals[i] == NULL) {
                        warn("::WARNING:: frame_local get == NULL\n");
                }
        #endif
        return frame_stack->locals[i];
}

size_t frame_return_ptr() {
        debug("::FRAME:: get_ret: %ld\n", frame_stack->return_ptr);
        return frame_stack->return_ptr;
}

void init_frame_stack(constant_pool_index_t entry_id) {
        debug("::FRAME:: init\n");
        frame_stack = NULL;
        frame_push(entry_id, 0);
}

void free_frame_stack() {
        debug("::FRAME:: free\n");
        while (frame_stack != NULL) {
                frame_pop();
        }
}

bool is_end(frame_stack_iterator * f) {
        bool res = f->frame == NULL || f->frame->locals_cnt == 0;
        // eprintf(" - is end? [%c]\n", res ? 'y' : 'n');
        return res;
}

// typedef struct {
//         frame_t * frame;
//         size_t i;
// } frame_stack_iterator;

void debug_iterator(frame_stack_iterator * f, const char * prefix) {
        if (f == NULL) {
                eprintf("%s", prefix);
                eprintf("[null]\n");
                return;
        }
        eprintf("%s", prefix);
        eprintf("- frame: %p\n", (void *)f->frame);
        if (f->frame != NULL) {
                eprintf("%s", prefix);
                eprintf("   - pre: %p\n", (void *)f->frame->previous_frame);
                eprintf("%s", prefix);
                eprintf("   - locals_cnt: %ld\n", f->frame->locals_cnt);
        }
        eprintf("%s", prefix);
        eprintf("- i: %ld\n", f->i);
}

frame_stack_iterator next_frame_iterator(frame_stack_iterator * f) {
        // eprintf("Next frame iterator:\n - input:\n");
        // debug_iterator(f, "   ");
        // copy frame stack from input to use as acc
        frame_stack_iterator res = {
                .frame = f->frame,
                .i = f->i
        };
        // by default increment i
        res.i++;
        // if input == NULL start at begin
        if (res.frame == NULL) {
                res.frame = frame_stack;
                res.i = 0;
                // eprintf(" - set (begin case):\n");
                // debug_iterator(&res, "   ");
        }
        // until res.frame == NULL or nonzero sized frame is found
        while (res.frame != NULL && res.i >= res.frame->locals_cnt) {
                res.frame = res.frame->previous_frame;
                res.i = 0;
                // eprintf(" - set:\n");
                // debug_iterator(&res, "   ");
        }
        // if no nonzero sized frame was found return end
        if (res.frame == NULL) {
                res.frame = NULL;
                res.i = 0;
                // eprintf(" - return (end case):\n");
                // debug_iterator(&res, "   ");
                return res;
        }

        // eprintf(" - return:\n");
        // debug_iterator(&res, "   ");
        return res;
}

object_id frame_stack_value(frame_stack_iterator * f) {
        if (f->frame == NULL || f->i >= f->frame->locals_cnt) {
                eprintf("WARN: framestack iterator overran values\n");
                eprintf(" - frame: %p\n", (void *)f->frame);
                eprintf(" - index: %ld\n", f->i);
                eprintf(" - locals_cnt: %ld\n", f->frame->locals_cnt);
                eprintf(" - locals_cnt: %ld\n", f->frame->locals_cnt);
                return 0;
        }
        // eprintf("      Get frame stack value of:\n");
        // debug_iterator(f, "        ");
        return f->frame->locals[f->i];
}
