#include "bc_interpret.h"


#include "ast_interpret/runtime_types.h"
#include "bc_interpret/byte_helpers.h"
#include "bc_interpret/runtime_types.h"
#include "bc_interpret/types.h"
#include "common.h"
#include "debuglog.h"

#include "loader.h"

#include "magic_checker.h"
#include "const_pool.h"
#include "globals.h"
#include "operand_stack.h"
#include "frame_stack.h"
#include "heap.h"
#include "builtin_functions.h"
#include "byte_helpers.h"

byte_t * bytes = NULL;
size_t ins_ptr = 0;

size_t fnargbuffer_size = 0;
object_id * fnargbuffer = NULL;

size_t dummy = 0;

bool object_field_index(obj_t * obj, constant_pool_index_t field_name, size_t * field_index) {
        bool success;
        *field_index = get_class_field_index(obj->class, field_name, &success);
        return success;
}
object_id get_real_reciever(object_id apparent_receiver, constant_pool_index_t field_name, size_t * field_index) {
        base_object_t * real = get_object_ptr(apparent_receiver);
        size_t field_index_internal;
        while (real != NULL) {
                if (get_kindp(real) != obj_kind) {
                        *field_index = 666666666;
                        return get_object_id(real);
                }
                if (object_field_index((obj_t *)real, field_name, &field_index_internal)) {
                        *field_index = field_index_internal;
                        return get_object_id(real);
                }
                real = get_object_ptr(((obj_t *)real)->parent);
        }
        return 0;
}

void bc_do_interpret() {
        // mark(" - fnargbuffer = realloc");
        fnargbuffer = realloc(fnargbuffer, sizeof(object_id) * 10);
        fnargbuffer_size = 10;
        size_t i = 0;
        while(ins_ptr) {
                i = ins_ptr;
                if (i == 1) { exit(77); }
                debug("::DEBUG:: load opcode %ld\n", i);
                opcode_t op = inc_load(uint8_t, i, bytes);
                #ifdef ENABLE_CHECKERS
                warn_about_keep_bits();
                #endif
                switch (op) {
                        break; case const_ins: {
                                // mark(" - const_ins begin");
                                eprintf("CONST\n");
                                constant_pool_index_t constid = inc_load(uint16_t, i, bytes);
                                debug("::INSTRUC:: [*%ld = %d] const #%d\n", i, op, constid);
                                object_id val = alloc_runtime_object_from_constant(constid);
                                #ifdef ENABLE_CHECKERS
                                if (val == NULL) {
                                        warn("::WARNING:: const_ins Pushing NULL\n");
                                }
                                if (get_kind(val) > 6) {
                                        warn("::WARNING:: const_ins Push %d\n", get_kind(val));
                                }
                                #endif
                                operand_push(val);
                                unset_bit(val, keep_bit);
                                ins_ptr = i;
                                // mark(" - const_ins end");
                        }
                        break; case arr_ins: {
                                debug("::INSTRUC:: [*%ld = %d] arr_ins\n", i, op);
                                eprintf("ARRAY\n");

                                // Pop a pointer to a runtime object from the operand stack
                                // This object is the initial value
                                set_bit(operand_peek(), keep_bit);
                                object_id init_val = operand_pop();


                                // Pop another pointer from the operand stack
                                // This object represents the size of the array.
                                set_bit(operand_peek(), keep_bit); // not needed here, but would be needed if GC was nonblocking
                                object_id arr_size_obj = operand_pop();

                                // The size must be a runtime object that represents an Integer
                                // whose value must be greater or equal to zero
                                int_t * arr_size_int = (int_t *)get_object_ptr(arr_size_obj);
                                int_val_t arr_size = arr_size_int->value;
                                unset_bit(arr_size_obj, keep_bit); // not needed here, but would be needed if GC was nonblocking
                                // TODO: check

                                // Allocate a new runtime object of type Array with the size defined by the operand
                                object_id arr = alloc_array(arr_size);
                                #ifdef ENABLE_CHECKERS
                                if (arr == NULL) {
                                        warn("::WARNING:: pre arr_ins Pushing NULL\n");
                                }
                                if (get_kind(arr) > 6) {
                                        warn("::WARNING:: pre arr_ins Push %d\n", get_kind(arr));
                                }
                                #endif

                                // Set each element within the array to a pointer to the initial value as obtained from the other operand
                                size_t arr_i;
                                for(arr_i = 0; arr_i < (size_t)arr_size; arr_i++) {
                                        ((arr_t *)get_object_ptr(arr))->values[arr_i] = init_val;
                                }

                                unset_bit(init_val, keep_bit);

                                // Push the pointer to the new Array runtime object onto the operand stack.
                                #ifdef ENABLE_CHECKERS
                                if (arr == NULL) {
                                        warn("::WARNING:: arr_ins Pushing NULL\n");
                                }
                                if (get_kind(arr) > 6) {
                                        warn("::WARNING:: arr_ins Push %d\n", get_kind(arr));
                                }
                                #endif
                                operand_push(arr);

                                // Bump the instruction pointer.
                                ins_ptr = i;
                        }
                        break; case obj_ins: {
                                eprintf("OBJECT\n");
                                debug("::INSTRUC:: [*%ld = %d] obj_ins\n", i, op);

                                // Load operand
                                constant_pool_index_t cls = inc_load(uint16_t, i, bytes);

                                // Retrieve the program object at the given index in the constant pool.
                                uint16_t fld_cnt = get_class_field_count(cls);

                                // The program object represents the class of the object being created,
                                // so it must be a Class which contains a list of constant pool indices.
                                // Each of these indices must point to a String program object representing a name of a field.

                                // TODO: check

                                // Allocate a new runtime object representing the new object instance.
                                object_id obj = instantiate_class(cls);

                                // Iterate over the Class constant pool indices in reverse order.
                                // For each constant pool index in the class retrieve the String program object it points to
                                // and pop a pointer to a runtime object from the operand stack.

                                // Register a field with the given name in the runtime object
                                // and set the pointer as its value.
                                size_t fld_i;
                                for (fld_i = 0; fld_i < fld_cnt; fld_i++) {
                                        set_bit(operand_peek(), keep_bit); // not needed here, but would be needed if GC was nonblocking
                                        ((obj_t *)get_object_ptr(obj))->fields[(fld_cnt-1) - fld_i] = operand_pop();
                                        unset_bit(((obj_t *)get_object_ptr(obj))->fields[(fld_cnt-1) - fld_i], keep_bit); // not needed here, but would be needed if GC was nonblocking
                                }

                                // Then, pop one more pointer from the operand stack.
                                // Set the pointer as the parent of the Object runtime object being created.
                                set_bit(operand_peek(), keep_bit); // not needed here, but would be needed if GC was nonblocking
                                ((obj_t *)get_object_ptr(obj))->parent = operand_pop();
                                unset_bit(((obj_t *)get_object_ptr(obj))->parent, keep_bit); // not needed here, but would be needed if GC was nonblocking

                                // Finally, push a pointer to the runtime object onto the operand stack
                                #ifdef ENABLE_CHECKERS
                                if (obj == NULL) {
                                        warn("::WARNING:: obj_ins Pushing NULL\n");
                                }
                                if (get_kind(obj) > 6) {
                                        warn("::WARNING:: obj_ins Push %d\n", get_kind(obj));
                                }
                                #endif
                                operand_push(obj);
                                unset_bit(obj, keep_bit);  // not needed here, but would be needed if GC was nonblocking

                                // and bump the instruction pointer.
                                ins_ptr = i;
                        }
                        break; case getloc_ins: {
                                debug("::INSTRUC:: [*%ld = %d] getloc_ins\n", i, op);
                                // mark(" - getloc_ins begin");
                                frame_index_t fi = inc_load(uint16_t, i, bytes);
                                object_id val = frame_local_get(fi);
                                eprintf("GET_LOCAL %d\n", val);
                                #ifdef ENABLE_CHECKERS
                                if (val == NULL) {
                                        warn("::WARNING:: getloc_ins Pushing NULL\n");
                                }
                                if (get_kind(val) > 6) {
                                        warn("::WARNING:: getloc_ins Push %d\n", get_kind(val));
                                }
                                #endif
                                operand_push(val);
                                ins_ptr = i;
                                // mark(" - getloc_ins end");
                        }
                        break; case setloc_ins: {
                                debug("::INSTRUC:: [*%ld = %d] setloc", i, op);
                                // mark(" - setloc_ins begin");
                                frame_index_t fi = inc_load(uint16_t, i, bytes);
                                // mark(" - setloc_ins peek");
                                eprintf("SET_LOCAL %d\n", operand_peek());
                                frame_local_set(fi, operand_peek());
                                // mark(" - setloc_ins bump");
                                ins_ptr = i;
                                // mark(" - setloc_ins end");
                        }
                        break; case getglob_ins: {
                                debug("::INSTRUC:: [*%ld = %d] getglob_ins\n", i, op);
                                // mark(" - getglob_ins begin");
                                constant_pool_index_t ci = inc_load(uint16_t, i, bytes);
                                object_id val = get_global(ci);
                                eprintf("GET_GLOBAL %d\n", val);
                                #ifdef ENABLE_CHECKERS
                                if (val == NULL) {
                                        warn("::WARNING:: getglob_ins Pushing NULL\n");
                                }
                                if (get_kind(val) > 6) {
                                        warn("::WARNING:: getglob_ins Push %d\n", get_kind(val));
                                }
                                #endif
                                operand_push(val);
                                ins_ptr = i;
                                // mark(" - getglob_ins end");
                        }
                        break; case setglob_ins: {
                                debug("::INSTRUC:: [*%ld = %d] setglob_ins\n", i, op);
                                // mark(" - setglob_ins begin");
                                constant_pool_index_t ci = inc_load(uint16_t, i, bytes);
                                eprintf("SET_GLOBAL %d\n", operand_peek());
                                set_global(ci, operand_peek());
                                ins_ptr = i;
                                // mark(" - setglob_ins end");
                        }
                        break; case getfld_ins: {
                                eprintf("GET_FIELD\n");
                                debug("::INSTRUC:: [*%ld = %d] getfld_ins\n", i, op);
                                constant_pool_index_t field_name = inc_load(uint16_t, i, bytes);
                                // TODO: check field_name is string

                                set_bit(operand_peek(), keep_bit); // not needed here, but would be needed if GC was nonblocking
                                object_id apparent_receiver = operand_pop();

                                size_t field_index = 0; // if real_receiver is not of type object, this can be uninitialized !!!
                                object_id real_receiver = get_real_reciever(apparent_receiver, field_name, &field_index);
                                set_bit(real_receiver, keep_bit); // not needed here, but would be needed if GC was nonblocking

                                #ifdef ENABLE_CHECKERS
                                if (real_receiver == NULL || get_kind(real_receiver) != obj_kind) {
                                        eprintf("Field does not exist on the specified object of type %d\n", (get_kind(apparent_receiver)));
                                        exit(2);
                                }
                                #endif
                                if (real_receiver != apparent_receiver) { // Altho this `if` is very VERY much needed :-D, god DAMMMIT!!!!!
                                        unset_bit(apparent_receiver, keep_bit); // not needed here, but would be needed if GC was nonblocking
                                }
                                object_id val = ((obj_t *) get_object_ptr(real_receiver))->fields[field_index];
                                set_bit(val, keep_bit); // not needed here, but would be needed if GC was nonblocking
                                unset_bit(real_receiver, keep_bit); // not needed here, but would be needed if GC was nonblocking
                                #ifdef ENABLE_CHECKERS
                                if (val == NULL) {
                                        warn("::WARNING:: getfld_ins Pushing NULL\n");
                                }
                                if (get_kind(val) > 6) {
                                        warn("::WARNING:: getfld_ins Push %d\n", get_kind(val));
                                }
                                #endif
                                operand_push(val);
                                unset_bit(val, keep_bit); // not needed here, but would be needed if GC was nonblocking
                                ins_ptr = i;
                        }
                        break; case setfld_ins: {
                                eprintf("SET_FIELD\n");
                                debug("::INSTRUC:: [*%ld = %d] setfld_ins\n", i, op);
                                constant_pool_index_t field_name = inc_load(uint16_t, i, bytes);
                                // TODO: check field_name is string

                                set_bit(operand_peek(), keep_bit); // not needed here, but would be needed if GC was nonblocking
                                object_id val = operand_pop();
                                set_bit(operand_peek(), keep_bit); // not needed here, but would be needed if GC was nonblocking
                                object_id apparent_receiver = operand_pop();

                                size_t field_index = 0; // if real_receiver is not of type object, this can be uninitialized !!!
                                object_id real_receiver = get_real_reciever(apparent_receiver, field_name, &field_index);
                                set_bit(real_receiver, keep_bit); // not needed here, but would be needed if GC was nonblocking
                                if (real_receiver != apparent_receiver) { // Altho this `if` is very VERY much needed :-D, god DAMMMIT!!!!!
                                        unset_bit(apparent_receiver, keep_bit); // not needed here, but would be needed if GC was nonblocking
                                }
                                #ifdef ENABLE_CHECKERS
                                if (real_receiver == NULL || get_kind(real_receiver) != obj_kind) {
                                        eprintf("Field does not exist on the specified object of type %d\n", get_kind(apparent_receiver));
                                        exit(2);
                                }
                                #endif

                                (((obj_t *) get_object_ptr(real_receiver))->fields[field_index]) = val;
                                unset_bit(real_receiver, keep_bit); // not needed here, but would be needed if GC was nonblocking
                                #ifdef ENABLE_CHECKERS
                                if (val == NULL) {
                                        warn("::WARNING:: setfld_ins Pushing NULL\n");
                                }
                                if (get_kind(val) > 6) {
                                        warn("::WARNING:: setfld_ins Push %d\n", get_kind(val));
                                }
                                #endif
                                operand_push(val);
                                unset_bit(val, keep_bit); // not needed here, but would be needed if GC was nonblocking
                                ins_ptr = i;
                        }
                        break; case call_ins: {
                                eprintf("CALL\n");
                                debug("::INSTRUC:: [*%ld = %d] call -", ins_ptr, op);
                                // mark(" - call_ins begin");
                                // Load instruction arg (arg count)
                                uint8_t args_cnt = inc_load(uint8_t, i, bytes);
                                debug("::DEBUG::  args_cnt: %d\n", args_cnt);

                                // Maybe reallocate space for the argument pointers on the top of the stack
                                if (args_cnt > fnargbuffer_size) {
                                        fnargbuffer = realloc(fnargbuffer, sizeof(object_id) * args_cnt * 2);
                                        fnargbuffer_size = args_cnt * 2;
                                }

                                // Pop arguments into buffer
                                size_t arg_i;
                                for (arg_i = 0; arg_i < args_cnt; arg_i++) {
                                        debug("::DEBUG::  - call: pop arg\n");
                                        set_bit(operand_peek(), keep_bit);
                                        fnargbuffer[arg_i] = operand_pop();
                                }

                                // Pop function
                                debug("::DEBUG::  - call: pop fun\n");
                                set_bit(operand_peek(), keep_bit);
                                object_id obj = operand_pop();
                                // TODO: check function
                                fun_t * fn = (fun_t *) get_object_ptr(obj);

                                // Push frame

                                size_t real_arg_cnt = fn->local_cnt + fn->param_cnt + 1;
                                // TODO: check argcnt
                                frame_push(real_arg_cnt, i);

                                // Set locals

                                frame_local_set(0, alloc_null());
                                for (i = 0; i < args_cnt; i++) {
                                        frame_local_set(i+1, fnargbuffer[(args_cnt - 1) - i]);
                                        unset_bit(fnargbuffer[(args_cnt - 1) - i], keep_bit);
                                }
                                for (i = 0; i < fn->local_cnt; i++) {
                                        frame_local_set(args_cnt + i+1, alloc_null());
                                }

                                ins_ptr = fn->start;
                                unset_bit(obj, keep_bit);
                                debug("::DEBUG::  - call: set ins_ptr: %ld\n", ins_ptr);
                                // mark(" - call_ins end");
                        }
                        break; case callfld_ins: {
                                eprintf("CALL_FIELD\n");
                                constant_pool_index_t method_name = inc_load(uint16_t, i, bytes);
                                uint8_t args_cnt = inc_load(uint8_t, i, bytes);
                                debug("::INSTRUC:: [*%ld = %d] callfld_ins: _%d_(%d)\n", i, op, method_name, args_cnt);

                                // TODO: check it's a string

                                // Maybe reallocate space for the argument pointers on the top of the stack
                                if (args_cnt > fnargbuffer_size) {
                                        fnargbuffer = realloc(fnargbuffer, sizeof(object_id) * args_cnt * 2);
                                        fnargbuffer_size = args_cnt * 2;
                                }

                                size_t arg_i;
                                #ifdef ENABLE_CHECKERS
                                if (args_cnt < 1) {
                                        warn("::WARNING:: args_cnt would underflow\n");
                                }
                                #endif
                                fnargbuffer[0] = 0;
                                fnargbuffer[1] = 0;
                                for (arg_i = 0; arg_i < (size_t)(args_cnt-1); arg_i++) {
                                        set_bit(operand_peek(), keep_bit);
                                        fnargbuffer[arg_i] = operand_pop();
                                        debug("::DEBUG:: fnargbuffer[%ld] = %d\n", arg_i, get_kind(fnargbuffer[arg_i]));
                                }
                                // for (; arg_i < fnargbuffer_size; arg_i++) {
                                //         // set_bit(operand_peek(), keep_bit);
                                //         fnargbuffer[arg_i] = 0;
                                //         // debug("::DEBUG:: fnargbuffer[%ld] = %d\n", arg_i, get_kind(fnargbuffer[arg_i]));
                                // }

                                debug("\n::DEBUG:: A\n");
                                set_bit(operand_peek(), keep_bit);
                                debug("\n::DEBUG:: B\n");
                                object_id apparent_receiver = operand_pop();

                                debug("\n::DEBUG:: C\n");
                                size_t method_index = 666666666; // if real_receiver is not of type object, this can be uninitialized !!!

                                debug("\n::DEBUG:: D\n");
                                object_id real_receiver = get_real_reciever(apparent_receiver, method_name, &method_index);

                                debug("\n::DEBUG:: E\n");
                                set_bit(real_receiver, keep_bit);

                                debug("\n::DEBUG:: F\n");
                                if (real_receiver != apparent_receiver) {

                                        debug("\n::DEBUG:: A\n");
                                        unset_bit(apparent_receiver, keep_bit);
                                }

                                #ifdef ENABLE_CHECKERS
                                if (real_receiver == NULL) {
                                        eprintf("Method does not exist on the specified object of type %d\n", get_kind(apparent_receiver));
                                        exit(2);
                                }
                                #endif

                                debug("\n::DEBUG:: G\n");
                                // eprintf(" - CALL_FIELD\n");
                                switch(get_kind(real_receiver)) {
                                        break; case obj_kind: {
                                                debug("\n::DEBUG:: Obj\n");
                                                // TODO: check field is a function
                                                // TODO: get the field (function)
                                                if (method_index == 666666666) {
                                                        eprintf("WARN: method index was uninitialized\n");
                                                }
                                                debug("\n::DEBUG:: H: %ld\n", method_index);
                                                obj_t * recv_obj = (obj_t *)get_object_ptr(real_receiver);
                                                object_id met_id = recv_obj->fields[method_index];
                                                debug("\n::DEBUG:: HH: %d\n", met_id);


                                                fun_t * fn = (fun_t *)get_object_ptr(met_id);


                                                // Push frame

                                                debug("\n::DEBUG:: I: %p\n", (void *) fn);
                                                size_t real_arg_cnt = fn->local_cnt + fn->param_cnt + 1;
                                                debug("\n::DEBUG:: J\n");
                                                // TODO: check argcnt
                                                frame_push(real_arg_cnt, i);
                                                debug("\n::DEBUG:: K\n");
                                                // Set locals

                                                frame_local_set(0, real_receiver);
                                                debug("\n::DEBUG:: L\n");
                                                unset_bit(real_receiver, keep_bit);
                                                debug("\n::DEBUG:: M\n");
                                                if (args_cnt) {
                                                        for (arg_i = 0; arg_i < (size_t)(args_cnt - 1); arg_i++) {
                                                                debug("\n::DEBUG:: N\n");
                                                                debug("::DEBUG:: fnargbuffer[(args_cnt - 1) - arg_i] = fnargbuffer[%ld]\n", (args_cnt - 2) - arg_i);
                                                                debug("\n::DEBUG:: O\n");
                                                                frame_local_set(arg_i+1, fnargbuffer[(args_cnt - 2) - arg_i]);
                                                                debug("\n::DEBUG:: P\n");
                                                                unset_bit(fnargbuffer[(args_cnt - 2) - arg_i], keep_bit);
                                                                debug("\n::DEBUG:: Q\n");
                                                        }
                                                        for (arg_i = 0; arg_i < fn->local_cnt; arg_i++) {
                                                                debug("\n::DEBUG:: R\n");
                                                                debug("::DEBUG:: alloc null()\n");
                                                                debug("\n::DEBUG:: S\n");
                                                                frame_local_set(args_cnt + arg_i+1, alloc_null());
                                                                debug("\n::DEBUG:: T\n");
                                                        }
                                                }

                                                debug("\n::DEBUG:: U\n");
                                                ins_ptr = fn->start;
                                                debug("::DEBUG::  - call: set ins_ptr: %ld\n", ins_ptr);
                                                debug("\n::DEBUG:: V\n");
                                                // mark(" - call_ins end");
                                        }
                                        break; case fun_kind: {
                                                debug("\n::DEBUG:: Fun\n");
                                                eprintf("Cannot call method on a function\n");
                                                exit(2);
                                        }
                                        break; default: {
                                                debug("\n::DEBUG:: Default\n");
                                                size_t bltn_id = get_builtin_id(method_name);
                                                bltn_fn_ptr bltn = get_builtin_fn_ptr(bltn_id);

                                                // TODO: check argcnt == 2

                                                if (fnargbuffer[1]) {
                                                        debug("::BUILTIN:: bltn(%d, %d, %d)\n", get_kind(real_receiver), get_kind(fnargbuffer[0]), get_kind(fnargbuffer[1]));
                                                        // eprintf("::BUILTIN:: bltn(%d, %d, %d)\n", get_kind(real_receiver), get_kind(fnargbuffer[0]), get_kind(fnargbuffer[1]));
                                                }
                                                else {
                                                        debug("::BUILTIN:: bltn(%d, %d, (nil) )\n", get_kind(real_receiver), get_kind(fnargbuffer[0]));
                                                        // eprintf("::BUILTIN:: bltn(%d, %d, (nil) )\n", get_kind(real_receiver), get_kind(fnargbuffer[0]));
                                                }

                                                object_id val = bltn(real_receiver, fnargbuffer[0], fnargbuffer[1]);

                                                set_bit(val, keep_bit); // either set by alloc already OR more importantly comes from the arguments
                                                // eprintf(" --- default %d\n", real_receiver);
                                                unset_bit(real_receiver, keep_bit);
                                                // eprintf(" --- default %d\n", fnargbuffer[0]);
                                                unset_bit(fnargbuffer[0], keep_bit);
                                                // eprintf(" --- default %d\n", fnargbuffer[1]);
                                                unset_bit(fnargbuffer[1], keep_bit);
                                                // eprintf(" --- default\n");

                                                #ifdef ENABLE_CHECKERS
                                                if (val == NULL) {
                                                        warn("::WARNING:: callfld_ins Pushing NULL\n");
                                                }
                                                if (get_kind(val) > 6) {
                                                        warn("::WARNING:: callfld_ins Push %d\n", get_kind(val));
                                                }
                                                #endif
                                                operand_push(val);
                                                unset_bit(val, keep_bit);
                                                ins_ptr = i;
                                        }
                                }

                                // for (arg_i = 0; arg_i < (size_t)(args_cnt - 1); arg_i++) {
                                //         // Just to be sure that there aro no memory leaks
                                //         unset_bit(fnargbuffer[(args_cnt - 2) - arg_i], keep_bit);
                                // }
                        }
                        break; case retn_ins: {
                                eprintf("RETURN\n");
                                debug("::INSTRUC:: [*%ld = %d] retn_ins\n", i, op);
                                // mark(" - retn_ins begin");
                                ins_ptr = frame_return_ptr();
                                // TODO: check ptr range?
                                frame_pop();
                                // mark(" - retn_ins end");
                        }
                        break; case jmp_ins: {
                                eprintf("JUMP\n");
                                int16_t rel = (int16_t) inc_load(uint16_t, i, bytes);
                                debug("::INSTRUC:: [*%ld = %d] jmp_ins + (%d)\n", i, op, rel);
                                // mark(" - jmp_ins begin");
                                ins_ptr = i + rel;
                                // mark(" - jmp_ins end");
                        }
                        break; case jmpif_ins: {
                                eprintf("JUMP_IF\n");
                                debug("::INSTRUC:: [*%ld = %d] jmpif_ins\n", i, op);
                                // mark(" - jmpif_ins begin");
                                set_bit(operand_peek(), keep_bit);
                                object_id cond = operand_pop();
                                int16_t rel = (int16_t) inc_load(uint16_t, i, bytes);
                                if (is_truthy(cond)) {
                                        ins_ptr = i + rel;
                                }
                                else {
                                        ins_ptr = i;
                                }
                                unset_bit(cond, keep_bit);
                                // mark(" - jmpif_ins end");
                        }
                        break; case print_ins: {
                                eprintf("PRINT\n");
                                debug("::INSTRUC:: [*%ld = %d] print\n", i, op);
                                constant_pool_index_t format = inc_load(uint16_t, i, bytes);
                                uint8_t args_cnt = inc_load(uint8_t, i, bytes);
                                // exit(44);

                                if (args_cnt > fnargbuffer_size) {
                                        fnargbuffer = realloc(fnargbuffer, sizeof(object_id) * args_cnt * 2);
                                        fnargbuffer_size = args_cnt * 2;
                                }

                                size_t arg_i;
                                for (arg_i = 0; arg_i < (size_t)(args_cnt); arg_i++) {
                                        set_bit(operand_peek(), keep_bit);
                                        fnargbuffer[(args_cnt - 1) - arg_i] = operand_pop();
                                        debug("::PRINT:: prepare buffer[%ld] = %d\n", (args_cnt - 1) - arg_i, get_kind(fnargbuffer[(args_cnt - 1) - arg_i]));
                                }

                                bltn_print(get_string_length(format), get_string_data(format), (size_t)args_cnt, fnargbuffer);

                                for (arg_i = 0; arg_i < (size_t)(args_cnt); arg_i++) {
                                        unset_bit(fnargbuffer[(args_cnt - 1) - arg_i], keep_bit);
                                }
                                object_id val = alloc_null();
                                #ifdef ENABLE_CHECKERS
                                if (val == NULL) {
                                        warn("::WARNING:: print_ins Pushing NULL\n");
                                }
                                if (get_kind(val) > 6) {
                                        warn("::WARNING:: print_ins Push %d\n", get_kind(val));
                                }
                                #endif
                                operand_push(val);

                                // printf("%s", format);
                                ins_ptr = i;
                        }
                        break; case drop_ins: {
                                eprintf("DROP\n");
                                debug("::INSTRUC:: [*%ld = %d] drop_ins\n", i, op);
                                // mark(" - drop_ins begin");
                                operand_pop();
                                ins_ptr = i;
                                // mark(" - drop_ins end");
                        }
                }
        }
}

int bc_interpret(const char *src, uint64_t heap_bytes, char * gclogs)
{
        mark ( "::MARK:: Init heap" );

        init_heap(heap_bytes, gclogs);

        debug("::DEBUG:: Source: %s\n", src);
        mark ( "::MARK:: Get bytecode's bytes and size" );

        size_t len = 0;
        bytes = load_bc(src, &len);

        mark ( "::MARK:: Init cursor" );
        size_t cursor = 0;

        mark ( "::MARK:: Check magic bytes" );

        cursor = check_magic(bytes, cursor, len);
        debug("::DEBUG::  - cursor: %ld\n", cursor);
        if (cursor == 0) {
                eprintf("Magic check failed\n");
                return 2;
        }

        mark ( "::MARK:: Init constant pool" );

        cursor = init_const_pool(bytes, cursor, len);

        mark ( "::MARK:: Init globals" );

        cursor = init_globals(bytes, cursor, len);

        mark ( "::MARK:: Init operand stack" );

        init_operand_stack();

        mark ( "::MARK:: Init entry" );

        debug("::DEBUG::  - Get entry index: %ld\n", cursor);
        constant_pool_index_t entry_id = inc_load(uint16_t, cursor, bytes);
        debug("::DEBUG::  - Result: %d\n", entry_id);
        ins_ptr = get_function_start(entry_id);
        debug("::DEBUG::  - Set ins_ptr: %ld\n", ins_ptr);

        mark ( "::MARK:: Init frame stack" );

        init_frame_stack(entry_id);

        mark ( "::MARK:: Interpret" );
        {
                debug("::DEBUG::  - ins_ptr: %ld\n", ins_ptr);
                bc_do_interpret();
        }

        free_frame_stack();
        free_operand_stack();
        free_globals();
        free_const_pool();
        free_heap();


        return 0;
}
