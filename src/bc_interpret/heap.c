#include "heap.h"

#include <stdalign.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "bc_interpret/runtime_types.h"
#include "debuglog.h"

#include "common.h"
#include "types.h"
#include "const_pool.h"
#include "operand_stack.h"
#include "frame_stack.h"
#include "globals.h"

#include "parser.h"

unsigned char * heap = NULL;
uint64_t HEAPSIZE;
uint64_t OBJECT_TABLE_SIZE;
size_t heap_top = 0;
FILE * gclogfile;

base_object_t ** object_table;
void print_heap(FILE * f);

void init_null() {
        // eprintf("heap ptr: %p\n", (void *)heap);
        object_table[0] = NULL;
        object_table[1] = ((base_object_t *)(heap));
        set_object_id(object_table[1], 1);
        get_object_ptr(1)->kind = null_kind;
        heap_top += sizeof(nul_t);
}

bool ptr_is_aligned(void * ptr, size_t alignment) {
        // debug("alloc attempt: %ld\n", (size_t)(ptr) % alignment);
        return (((size_t)(ptr) % alignment) == 0);
}

size_t bytes_to_align(size_t old_heap_top, size_t alignment) {
        size_t i = 0;
        while(!ptr_is_aligned(heap + old_heap_top + i, alignment)) {
                i++;
                if (i > 300) {
                        eprintf("fucked up alignment, you did\n");
                        exit(66);
                }
        }
        return i;
}

size_t align_with_bytes(size_t old_heap_top, size_t bytes) {
        size_t i;
        for (i = 0; i < bytes; i++) {
                heap[old_heap_top+i] = pad_bit;
        }
        return bytes;
}

// size_t align_heap(size_t old_heap_top, size_t alignment) {
//         // eprintf("Before align_heap:\n");
//         // print_heap(stderr);
//         size_t x = align_with_bytes(old_heap_top, bytes_to_align(old_heap_top, alignment));
//         // eprintf("After align_heap:\n");
//         // print_heap(stderr);
//         // eprintf("Plus pad of: %ld\n", x);
//         return x;
// }

object_id halloc(size_t bytes, size_t alignment) {
        debug("::HEAP:: alloc\n");
        // eprintf("HEAP ALLOC\n");
        size_t btoal = bytes_to_align(heap_top, alignment);
        #ifdef GARBAGE_COLLECT
        if (bytes + btoal + heap_top >= /*32*/ HEAPSIZE) {
                garbage_collect();
                btoal = bytes_to_align(heap_top, alignment);
        }
        #endif
        heap_top += align_with_bytes(heap_top, btoal);
        base_object_t * p = (base_object_t *)(heap + heap_top);
        p->kind = 0;
        p->object_table_id_a = 0;
        p->object_table_id_b = 0;
        p->object_table_id_c = 0;
        heap_top += bytes;
        debug("::HEAP:: alloc done: %ld\n", ((size_t)p) - ((size_t)heap));
        if (heap_top >= HEAPSIZE) {
                eprintf("HEAP OVERFLOW\n");
                exit(42);
        }
        debug("::ALLOC:: (%ld, %ld)->%ld\n", bytes, alignment, heap_top);

        object_id i;
        debug("::ALLOC:: (%ld, %ld)->%ld\n", bytes, alignment, heap_top);
        for (i = 1; i < OBJECT_TABLE_SIZE && object_table[i] != NULL; i++) {}
        if (i >= OBJECT_TABLE_SIZE) {
                eprintf("No more room for another object\n");
                exit(23);
        }
        debug("    do alloc: %d[%ld] = %p %% %ld\n", i, bytes, (void *)p, alignment);
        // eprintf("::ALLOC:: (%ld, %ld)-> %d[%p] | %ld\n", bytes, alignment, i, (void *)p, heap_top);

        object_table[i] = p;
        debug("::ALLOC:: (%ld, %ld)->%ld\n", bytes, alignment, heap_top);
        set_object_id(p, i);
        debug("::ALLOC:: (%ld, %ld)->%ld\n", bytes, alignment, heap_top);
        return i;
}


size_t obj_size(base_object_t * current) {
        if (current == NULL) {
                eprintf("WARN: returning 0 length for NULLptr object\n");
                return 0;
        }

        switch (get_kindp(current)) {
                break; case null_kind: {
                        // eprintf("   null size\n");
                        return sizeof(nul_t);
                }
                break; case bool_kind: {
                        // eprintf("   bool size\n");
                        return sizeof(bool_t);
                }
                break; case int_kind: {
                        // eprintf("   int size\n");
                        return sizeof(int_t);
                }
                break; case fun_kind: {
                        // eprintf("   fun size\n");
                        return sizeof(fun_t);
                }
                break; case obj_kind: {
                        // eprintf("   obj size\n");
                        return (get_class_field_count(((obj_t*)current)->class) * sizeof(base_object_t *))+sizeof(obj_t);
                }
                break; case arr_kind: {
                        // eprintf("   arr size\n");
                        return (((arr_t *)current)->size * sizeof(base_object_t *)) + sizeof(arr_t);
                }
        }

        eprintf("WARN: returning 0 length for UNKNOWN object\n");
        exit(90);
        return 0;
}

base_object_t * next_heap_object(base_object_t * current) {
        // base_object_t * in = current;
        if (current == NULL) {
                // eprintf("    -- nullcase\n");
                return (base_object_t *)&heap[0];
        }

        unsigned char * ptr = (unsigned char *) current;
        ptr += obj_size(current);

        while(ptr < (heap + heap_top) && (*ptr) == pad_bit) {
                // eprintf("    -- padbit\n");
                ptr++;
        }

        if (ptr >= (heap + heap_top)) {
                // eprintf("    -- endcase\n");
                return NULL;
        }

        // eprintf("next_heap_object of %p (aka %d) is %ld bytes after aka -- %p\n", (void*)in, get_kindp(in), (((unsigned char *)ptr) - ((unsigned char *)current)), (void *)ptr);
        return (base_object_t *)ptr;
}

// FIXME: print heap
void print_heap(FILE * f) {
        base_object_t * obj = next_heap_object(NULL);
        unsigned char * begin_ptr = heap;
        unsigned char * pre_obj_end = heap;
        unsigned char * cur_obj_begin = heap;
        unsigned char * cur_obj_end = heap;
        fprintf(f, "HEAP:\n{\n");
        fprintf(f, "    %ld - %p: -----\n", 0L, (void *)begin_ptr);
        cur_obj_begin = (unsigned char *) obj;
        cur_obj_end = cur_obj_begin + obj_size(obj);
        fprintf(f, "        [%ld]-{ object: %d aka %d }\n", cur_obj_end - cur_obj_begin, _get_object_id(obj), get_kindp(obj));
        fprintf(f, "    %ld - %p: -----\n", cur_obj_end - begin_ptr, (void *)cur_obj_end);
        pre_obj_end = cur_obj_end;
        while((obj = next_heap_object(obj)) != NULL) {
                cur_obj_begin = (unsigned char *) obj;
                cur_obj_end = cur_obj_begin + obj_size(obj);
                if (pre_obj_end < cur_obj_begin) {
                        fprintf(f, "        [%ld]-{ ///////// }\n", cur_obj_begin - pre_obj_end);
                        fprintf(f, "    %ld - %p: -----\n", cur_obj_begin - begin_ptr, (void *)cur_obj_begin);
                }
                fprintf(f, "        [%ld]-{ object: %d aka %d }\n", cur_obj_end - cur_obj_begin, _get_object_id(obj), get_kindp(obj));
                fprintf(f, "    %ld - %p: -----\n", cur_obj_end - begin_ptr, (void *)cur_obj_end);
                pre_obj_end = cur_obj_end;
        }
        fprintf(f, "    TOP: %ld\n", heap_top);
        fprintf(f, "}\n");
}

void print_object_table(FILE * f) {
        size_t i;
        fprintf(f, "OBJECT TABLE:\n{\n");
        for (i = 1; i < OBJECT_TABLE_SIZE; i++) {
                if (object_table[i] != NULL) {
                        fprintf(f, "    Object [%ld] = %p\n", i, (void*)object_table[i]);
                }
        }
        fprintf(f, "}\n");
}

bool time_get(struct timespec *ts)
{
        #if defined(_WIN32) && defined(__MINGW32__)
                return clock_gettime(CLOCK_REALTIME, ts) == 0;
        #else
                return timespec_get(ts, TIME_UTC) == TIME_UTC;
        #endif
}

void gclog(char event) {
        eprintf("GCLOG A\n");
        long ns;
        eprintf("GCLOG B\n");
        {
        eprintf("GCLOG C\n");
                struct timespec time;
        eprintf("GCLOG D\n");
                time_get(&time);
        eprintf("GCLOG E\n");
                ns = time.tv_nsec;
        eprintf("GCLOG F\n");
        }
        eprintf("GCLOG G\n");
        if (event == 'S') {
                eprintf("GCLOG H\n");
                fprintf( gclogfile, "timestamp,event,heap\n");
                eprintf("GCLOG I\n");
        }
        eprintf("GCLOG J\n");
        fprintf(gclogfile, "%ld,%c,%ld\n", ns,event,heap_top);
        eprintf("GCLOG K\n");
        fflush(gclogfile);
}

void init_heap(uint64_t heap_bytes, char * gclogs) {
        debug("::HEAP:: init_heap\n");
        #ifndef GARBAGE_COLLECT
        heap_bytes = 1024*1024*1024;
        #endif
        eprintf("open: %s\n", gclogs);
        gclogfile = fopen(gclogs, "w+");
        if (gclogfile == NULL) {
                eprintf("gclogfile open error\n");
                exit(67);
        }
        eprintf("open: %s\n", gclogs);
        heap = malloc(heap_bytes);
        HEAPSIZE = heap_bytes;
        OBJECT_TABLE_SIZE = (heap_bytes / sizeof(bool_t));
        object_table = malloc(sizeof(base_object_t *) * OBJECT_TABLE_SIZE);
        memset(object_table, 0, sizeof(base_object_t *) * OBJECT_TABLE_SIZE);
        gclog('S');
        init_null();
}

void free_heap() {
        debug("::HEAP:: free_heap\n");
        gclog('E');
        fclose(gclogfile);
        free(heap);
        free(object_table);
}

void dealloc(base_object_t * p) {
        debug("::HEAP:: dealloc\n");
        (void)p;
}

size_t alignment_of_kind(object_kind_t k) {
        switch (k) {
                break; case null_kind: {
                        return alignof(nul_t);
                }
                break; case bool_kind: {
                        return alignof(bool_t);
                }
                break; case int_kind: {
                        return alignof(int_t);
                }
                break; case arr_kind: {
                        return alignof(arr_t);
                }
                break; case str_kind: {
                        return alignof(str_t);
                }
                break; case obj_kind: {
                        return alignof(obj_t);
                }
                break; case fun_kind: {
                        return alignof(fun_t);
                }
        }
        return alignof(max_align_t);
}

object_id alloc_null() {
        // eprintf("  ALLOC null\n");
        debug("::HEAP:: alloc_null\n");
        debug("::HEAP:: alloc_null done\n");
        return 1;
}
object_id alloc_integer() {
        // // eprintf("  ALLOC int\n");
        debug("::HEAP:: alloc_integer\n");
        object_id p = halloc(sizeof(int_t), alignof(int_t));
        set_bit(p, keep_bit);
        set_kind(p, int_kind);
        // eprintf("After alloc:\n");
        // print_heap(stderr);
        debug("::HEAP:: alloc_integer done\n");
        return p;
}
object_id alloc_boolean() {
        // eprintf("  ALLOC bool\n");
        debug("::HEAP:: alloc_boolean\n");
        object_id p = halloc(sizeof(bool_t), alignof(bool_t));
        set_bit(p, keep_bit);
        set_kind(p, bool_kind);
        // eprintf("After alloc:\n");
        // print_heap(stderr);
        debug("::HEAP:: alloc_boolean done\n");
        return p;
}
object_id alloc_array(size_t n) {
        // eprintf("  ALLOC arr\n");
        debug("::HEAP:: alloc_array\n");
        object_id p = halloc(( n * sizeof(base_object_t *))+sizeof(arr_t), alignof(arr_t));
        set_bit(p, keep_bit);
        set_kind(p, arr_kind);
        ((arr_t *)get_object_ptr(p))->size = n;
        size_t x;
        for (x = 0; x < n; x++) {
                ((arr_t *)get_object_ptr(p))->values[x] = 0;
        }
        // eprintf("After alloc:\n");
        // print_heap(stderr);
        debug("::HEAP:: alloc_array done\n");
        return p;
}
object_id alloc_object(obj_size_t field_cnt) {
        // eprintf("  ALLOC obj\n");
        debug("::HEAP:: alloc_object\n");
        object_id p = halloc((field_cnt * sizeof(base_object_t *))+sizeof(obj_t), alignof(obj_t));
        set_bit(p, keep_bit);
        size_t x;
        for (x = 0; x < field_cnt; x++) {
                ((obj_t *)get_object_ptr(p))->fields[x] = 0;
        }
        ((obj_t *)get_object_ptr(p))->parent = 0;
        set_kind(p, obj_kind);
        // eprintf("After alloc:\n");
        // print_heap(stderr);
        debug("::HEAP:: alloc_object done\n");
        return p;
}

object_id alloc_function() {
        // eprintf("  ALLOC fun\n");
        debug("::HEAP:: alloc_function\n");
        object_id p = halloc(sizeof(fun_t), alignof(fun_t));
        set_bit(p, keep_bit);
        set_kind(p, fun_kind);
        // eprintf("After alloc:\n");
        // print_heap(stderr);
        debug("::HEAP:: alloc_function done\n");
        return p;
}

base_object_t * get_object_ptr(object_id id) {
        // eprintf("    get object ptr: %d\n", id);
        return object_table[id];
}

void gc_mark_static() {
        // Mark non-deallocating objects as roots
        // eprintf("      gc_mark_static\n");
        ((base_object_t *)heap)->kind |= root_bit;
}
void gc_mark_operands() {
        // Mark operands as roots
        // eprintf("      gc_mark_operands\n");
        operand_t * op = NULL;
        size_t bound_check = 0;
        while ((op = next_operand(op)) != NULL) {
                get_object_ptr(operand_value(op))->kind |= root_bit;
                // eprintf("mark operand: %d\n", operand_value(op));
                if(bound_check++ > 5000) {
                        eprintf("ERROR: operands go brrrr\n");
                        exit(89);
                }
        }
}
void gc_mark_locals() {
        // Mark local objects as roots
        // eprintf("      gc_mark_locals\n");
        frame_stack_iterator it = {
                .frame = NULL,
                .i = 0
        };
        it = next_frame_iterator(&it);
        size_t bound_check = 0;
        while (!is_end(&it)) {
                if (frame_stack_value(&it) != 0) {
                        // eprintf("        gc: mark local: %d\n", frame_stack_value(&it));
                        // eprintf("mark local: %d\n", frame_stack_value(&it));
                        get_object_ptr(frame_stack_value(&it))->kind |= root_bit;
                }
                // else {
                //         eprintf("        gc: mark skip NULL\n");
                // }
                it = next_frame_iterator(&it);
                if(bound_check++ > 5000) {
                        eprintf("ERROR: frame_stack go brrrr\n");
                        exit(89);
                }
        }
}
void gc_mark_globals() {
        // Mark global objects as roots
        // eprintf("      gc_mark_globals\n");
        size_t i = 0;
        size_t bound_check = 0;
        for (i = 0; i < globals_count(); i++) {
                object_id id = get_global_by_id(i);
                if (id != 0) {
                        // eprintf("        gc: mark global: %d\n", id);
                        // eprintf("mark global: %d\n", id);
                        get_object_ptr(id)->kind |= root_bit;
                }
                // else {
                //         eprintf("        gc: mark skip\n");
                // }
                if(bound_check++ > 5000) {
                        eprintf("ERROR: globals go brrrr\n");
                        exit(89);
                }
        }
}
void gc_mark_keepers() {
        // Mark temporarily kept objects as roots
        // eprintf("      gc_mark_keepers\n");
        base_object_t * obj = NULL;
        size_t bound_check = 0;
        while((obj = next_heap_object(obj)) != NULL) {
                if (obj->kind & keep_bit) {
                        obj->kind |= root_bit;
                        // eprintf("mark keeper: %d = %p\n", _get_object_id(obj), (void *)obj);
                }
                if(bound_check++ > 5000) {
                        eprintf("ERROR: keepers go brrrr\n");
                        exit(89);
                }
        }
}

void gc_mark_obj(base_object_t * obj) {
        // recursively traverse structures
        // eprintf("      gc_mark_obj\n");
        // TODO: bounds check???
        object_id * children;
        size_t children_cnt;
        switch (get_kindp(obj)) {
                break; case obj_kind: {
                        children_cnt = get_class_field_count(((obj_t*)obj)->class);
                        children = ((obj_t*)obj)->fields;
                        gc_mark_obj(get_object_ptr(((obj_t*)obj)->parent));
                }
                break; case arr_kind: {
                        children_cnt = ((arr_t *)obj)->size;
                        children = ((arr_t *)obj)->values;
                }
                break; default: {
                        children_cnt = 0;
                        children = NULL;
                }
        }
        size_t i;
        for (i = 0; i < children_cnt; i++) {
                if (children[i] == 0) { continue; }
                gc_mark_obj(get_object_ptr(children[i]));
        }

        obj->kind |= alive_bit;
}

void gc_mark_alive() {
        // Mark all alive objects
        size_t bound_check = 0;
        // eprintf("      gc_mark_alive\n");
        base_object_t * obj = NULL;
        while((obj = next_heap_object(obj)) != NULL) {
                if (obj->kind & root_bit) {
                        // eprintf("      gc: mark alive: %d = %p\n", _get_object_id(obj), (void *)obj);
                        // eprintf("mark alive: %d = %p\n", _get_object_id(obj), (void *)obj);
                        gc_mark_obj(obj);
                }
                if(bound_check++ > 5000) {
                        eprintf("ERROR: alive go brrrr\n");
                        exit(89);
                }
        }
}

bool is_pad(void * p) {
        return (*((unsigned char *)p)) == pad_bit;
}
bool is_alive(void * p) {
        return (!is_pad(p)) && (((base_object_t *)p)->kind & alive_bit);
}
base_object_t * bobj(void * p) {
        return (base_object_t *)p;
}

unsigned char * gc_keep_object(unsigned char * space, unsigned char * alive) {
        long pad = bytes_to_align(space - heap, alignment_of_kind(get_kindp((base_object_t *)alive)));

        size_t objsize = obj_size((base_object_t *)alive);
        unsigned char * new_alive_object_ptr = space + pad;
        // eprintf("get obj id to keep\n");
        object_table[get_object_id((base_object_t *)alive)] = (base_object_t *)new_alive_object_ptr;

        object_kind_t prek = get_kindp((base_object_t *)alive);

        if (pad > 0) {
                memset(space, pad_bit, pad);
        }

        if (alive - space > pad) {
                memmove(space + pad, alive, objsize);
                memset(new_alive_object_ptr + objsize, pad_bit, ((unsigned char *)alive + objsize) - (new_alive_object_ptr + objsize));
        }

        object_kind_t postk = get_kindp((base_object_t *)(space + pad));

        if (prek != postk) {
                eprintf("Kinds do not match after sweep\n");
                exit(14);
        }


        return new_alive_object_ptr + objsize;
}
unsigned char * gc_kill_object(unsigned char * space, unsigned char * dead) {
        // eprintf("get obj id to kill: %p\n", (void *)dead);
        size_t objsize = obj_size((base_object_t *)dead);
        object_table[get_object_id((base_object_t *)dead)] = 0;
        memset(dead, pad_bit, objsize);
        return space;
}

void gc_sweep() {
        // eprintf("    gc_sweep\n");
        // Pokojskaaaa!!!!
        base_object_t * cur = NULL;
        unsigned char * fre = heap;

        size_t bound_check = 0;

        base_object_t * nxt = next_heap_object(cur);
        while(nxt != NULL) {
                cur = nxt;
                if (cur->kind & alive_bit) {
                        // preconditions:
                        //  - fre is at the minimum/rightmost/mostbehind/... place where I can put stuff
                        //  - cur is at alive object
                        //  - fre <= cur
                        nxt = next_heap_object(cur);
                        fre = gc_keep_object(fre, (unsigned char *)cur);
                        // eprintf("after keep:\n");
                        // print_heap(stderr);
                }
                else {
                        nxt = next_heap_object(cur);
                        fre = gc_kill_object(fre, (unsigned char *)cur);
                        // eprintf("after kill:\n");
                        // print_heap(stderr);
                }
                if(bound_check++ > 5000) {
                        eprintf("ERROR: unmark go brrrr\n");
                        exit(89);
                }
        }

        heap_top = fre - heap;
}
void gc_unmark() {
        // unset all alive_bit and root_bit bits (NOT keep_bit bits!)
        // eprintf("gc_unmark\n");
        base_object_t * obj = NULL;
        size_t bound_check = 0;
        while((obj = next_heap_object(obj)) != NULL) {
                obj->kind &= (~(root_bit | alive_bit));
                if(bound_check++ > 5000) {
                        eprintf("ERROR: unmark go brrrr\n");
                        exit(89);
                }
        }
}

void garbage_collect() {
        gclog('B');
        // eprintf("    garbage_collect\n");
        // :-D too many passes maybe? :-D
        gc_mark_static();
        gc_mark_operands();
        gc_mark_locals();
        gc_mark_globals();
        gc_mark_keepers();
        // eprintf("-------------------------------------------------\n");
        // print_heap(stderr);
        gc_mark_alive();
        gc_sweep();
        // eprintf("\nSWEEEEEEEEEEEEP\n\n");
        // print_heap(stderr);
        // print_object_table(stderr);
        // eprintf("-------------------------------------------------\n");
        gc_unmark();
        // eprintf("    garbage_collect done\n");
        gclog('A');
}
void set_bit(object_id ptr, object_kind_t bit) {
        if (ptr == 0) {return;}
        // eprintf("set_bit %d for %d\n", bit, ptr);
        get_object_ptr(ptr)->kind |= bit;
}
void unset_bit(object_id ptr, object_kind_t bit) {
        if (ptr == 0) {return;}
        // eprintf("unset_bit %d for %d\n", bit, ptr);
        get_object_ptr(ptr)->kind &= (~bit);
}
void warn_about_keep_bits() {
        // traverse heap and exit when keep bit is found
        // eprintf("warn_about_keep_bits\n");
        base_object_t * obj = NULL;
        while((obj = next_heap_object(obj)) != NULL) {
                if (obj->kind & keep_bit) {
                        eprintf("ERROR: FOUND KEPT OBJECTS EVEN AFTER INSTRUCTION DONE\n");
                        exit(56);
                }
        }
}
