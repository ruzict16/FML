#pragma once

#include <stdlib.h>
#include <debuglog.h>

#include "common.h"
#include "types.h"

typedef unsigned char byte_t;

#define int_kind    ((unsigned char) 0   /* 0b 00000 000*/ )
#define null_kind   ((unsigned char) 1   /* 0b 00000 001*/ )
#define str_kind    ((unsigned char) 2   /* 0b 00000 010*/ )
#define fun_kind    ((unsigned char) 3   /* 0b 00000 011*/ )
#define bool_kind   ((unsigned char) 4   /* 0b 00000 100*/ )
#define obj_kind    ((unsigned char) 5   /* 0b 00000 101*/ )
#define arr_kind    ((unsigned char) 6   /* 0b 00000 110*/ )
#define mask_kind   ((unsigned char) 7   /* 0b 00000 111*/ )

#define alive_bit   ((unsigned char) 8   /* 0b 00001 000*/ )
#define pad_bit     ((unsigned char) 16  /* 0b 00010 000*/ )
#define keep_bit    ((unsigned char) 32  /* 0b 00100 000*/ )
// #define operand_bit ((unsigned char) 64  /* 0b 01000 000*/ )
#define root_bit    ((unsigned char) 128 /* 0b 10000 000*/ )

// typedef struct {const char * x; const char * y; const char * z;} object_id;
typedef uint32_t object_id;


typedef unsigned char object_kind_t;


typedef struct {
        object_kind_t kind;
        unsigned char object_table_id_a;
        unsigned char object_table_id_b;
        unsigned char object_table_id_c;
} base_object_t;

inline uint32_t _get_object_id(base_object_t * o) {
        // uint32_t id = 0;
        // id |= o->object_table_id_a;
        // id = id << 8;
        // id |= o->object_table_id_b;
        // id = id << 8;
        // id |= o->object_table_id_c;
        uint32_t id = (uint32_t)(o->object_table_id_a) << 16
                    | (uint32_t)(o->object_table_id_b) << 8
                    | (uint32_t)(o->object_table_id_c);
        return id;
}
inline uint32_t get_object_id(base_object_t * o) {
        uint32_t id = _get_object_id(o);
        // eprintf("    get object id: %d of obj: %p\n", id, (void *)o);
        if (id == 0) {
                eprintf("ERROR: Object id returned 0\n");
                exit(94);
        }
        return id;
}

inline void set_object_id(base_object_t * o, uint32_t id) {
        // eprintf("    set object id: %d -> ", id);
        if (id == 0) {
                eprintf("WARNING setting object id to 0\n");
                exit(93);
        }
        o->object_table_id_a = 0xFF & id >> 16;
        o->object_table_id_b = 0xFF & id >> 8;
        o->object_table_id_c = 0xFF & id;
        // eprintf("%d\n", _get_object_id(o));
}

base_object_t * get_object_ptr(object_id id);

inline object_kind_t get_kind(object_id o) {
        return get_object_ptr(o)->kind & mask_kind;
}

inline void set_kind(object_id o, object_kind_t kind) {
        object_kind_t other = get_object_ptr(o)->kind & (~mask_kind);
        get_object_ptr(o)->kind = kind | other;
}
inline object_kind_t get_kindp(base_object_t * o) {
        return o->kind & mask_kind;
}

inline void set_kindp(base_object_t * o, object_kind_t kind) {
        object_kind_t other = o->kind & (~mask_kind);
        o->kind = kind | other;
}


typedef struct {
        base_object_t base;
} nul_t;

typedef struct {
        base_object_t base;
        bool_val_t value;
} bool_t;

typedef struct {
        base_object_t base;
        int_val_t value;
} int_t;

typedef struct {
        base_object_t base;
        arr_size_t size;
        object_id values[];
} arr_t;

typedef struct {
        base_object_t base;
        constant_pool_index_t value;
} str_t;

typedef struct {
        base_object_t base;
        object_id parent;
        constant_pool_index_t class;
        object_id fields[];
} obj_t;


typedef struct {
        base_object_t base;
        param_cnt_t param_cnt;
        local_cnt_t local_cnt;
        fun_len_t instructions_byte_cnt;
        size_t start;
} fun_t;


inline bool is_truthy(object_id value) {
        // debug("truthy");
        /*
        # Truthiness

        Whether a value is deemed true for the purpose of conditions
        in conditional (if) and loop (while) expressions
        is called their truthiness.

        If the condition evaluates to a boolean,
                the condition is true if the runtime object is true and false otherwise.
        If the condition evaluates to null,
                the condition is false.
        If the condition evaluates to any other value,
                the condition is true.

        So only null and false are falsy, all other values are truthy.
        */

        switch(get_kind(value)) {
                break; case null_kind: {
                        // debug("truthy: null_kind");
                        return false;
                }
                break; case bool_kind: {
                        // debug("truthy: boolean_kind");
                        return (((bool_t *)get_object_ptr(value))->value);
                }
                default: {
                        // debug("truthy: default");
                        return true;
                }
        }
}
