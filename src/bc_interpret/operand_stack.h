#pragma once

#include "runtime_types.h"

typedef struct operand_t {
        struct operand_t * prev;
        object_id val;
} operand_t;

void operand_push(object_id val);

object_id operand_pop();

object_id operand_peek();

operand_t * next_operand(operand_t * o);
object_id operand_value(operand_t * o);

void init_operand_stack();
void free_operand_stack();
