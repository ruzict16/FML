#include "operand_stack.h"
#include "heap.h"

#include "builtin_functions.h"

#include "debuglog.h"



operand_t * operand_stack = NULL;
size_t operand_stack_size = 0;


object_id internal_operand_peek() {
        // debug("::OPERAND:: still\n");
        #ifdef ENABLE_CHECKERS
        // debug("::OPERAND:: still\n");
        if (operand_stack_size == 0) {
                eprintf("Cannot peek or pop from an empty stack\n");
                exit(99);
        }
        // debug("::OPERAND:: still\n");
        #endif
        // debug("::OPERAND:: still\n");
        #ifdef ENABLE_CHECKERS
        // debug("::OPERAND:: still\n");
        if (operand_stack == NULL) {
                warn("::WARNING:: Cannot peek NULL\n");
        }
        // debug("::OPERAND:: still\n");
        if (operand_stack->val == NULL) {
                warn("::WARNING:: Peeked found NULL\n");
        }
        // debug("::OPERAND:: still\n");
        if (get_kind(operand_stack->val) > 6) {
                warn("::WARNING:: Peek %d\n", get_kind(operand_stack->val));
        }
        // debug("::OPERAND:: still\n");
        #endif
        // debug("::OPERAND:: still\n");
        return operand_stack->val;
}
void operand_push(object_id val) {
        #ifdef ENABLE_CHECKERS
        if (val == NULL) {
                warn("::WARNING:: Operand Pushing NULL\n");
        }
        if (get_kind(val) > 6) {
                warn("::WARNING:: Operand Push %d\n", get_kind(val));
        }
        #endif
        operand_t * op = malloc(sizeof(operand_t));
        debug("::MALLOC:: %p\n", (void*)op);
        op->prev = operand_stack;
        op->val = val;
        operand_stack = op;
        operand_stack_size++;
        debug("::OPERAND:: push %ld -> %ld (pre: %p, cur: %p) :: ", operand_stack_size - 1, operand_stack_size, (void *)operand_stack->prev, (void *)operand_stack);
        #ifdef DEBUGLOG
        bc_print_value(val, stderr);
        #endif
        debug("\n");
}

object_id operand_pop() {
        debug("::OPERAND:: pop %ld -> %ld", operand_stack_size, operand_stack_size - 1);
        object_id val = internal_operand_peek();
        debug(" :: ");
        #ifdef DEBUGLOG
        bc_print_value(val, stderr);
        #endif
        debug("\n");
        operand_t * op = operand_stack->prev;
        free(operand_stack);
        operand_stack = op;
        operand_stack_size--;
        if (operand_stack == NULL) {
                debug(" (cur: (nil))\n");
        }
        else {
                debug(" (pre: %p, cur: %p)\n", (void *)operand_stack->prev, (void *)operand_stack);
        }
        return val;
}

object_id operand_peek() {
        debug("::OPERAND:: peek");
        debug(" :: ");
        #ifdef DEBUGLOG
        bc_print_value(internal_operand_peek(), stderr);
        #endif
        debug("\n");
        return internal_operand_peek();
}

void init_operand_stack() {
        debug("::OPERAND:: init\n");
        operand_stack = NULL;
        operand_stack_size = 0;
}

void free_operand_stack() {
        while (operand_stack != NULL) {
                operand_pop();
        }
        debug("::OPERAND:: free\n");
        operand_stack = NULL;
        operand_stack_size = 0;
}

operand_t * next_operand(operand_t * o) {
        // :-D
        if (o == NULL) {
                return operand_stack;
        }
        return o->prev;
}
object_id operand_value(operand_t * o) {
        if (o == NULL) {
                return 0;
        }
        return o->val;
}
