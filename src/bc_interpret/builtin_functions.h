#pragma once

#include "runtime_types.h"

#include "debuglog.h"

typedef object_id(*bltn_fn_ptr)(object_id, object_id, object_id);

void check_if_builtin_name(constant_pool_index_t id, uint32_t len, const char * str);
size_t get_builtin_id(constant_pool_index_t id);
bltn_fn_ptr get_builtin_fn_ptr(size_t i);

void bltn_print(size_t format_len, const char * format, size_t argc, object_id* argv);

void bc_print_value(object_id v, FILE *);
