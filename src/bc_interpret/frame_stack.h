#pragma once

#include "common.h"
#include "types.h"

#include "runtime_types.h"

typedef struct frame_t {
        size_t return_ptr;
        struct frame_t * previous_frame;
        size_t locals_cnt;
        object_id locals[];
} frame_t;

void frame_push(size_t args_n, size_t return_ptr);
void frame_pop();

void frame_local_set(frame_index_t i, object_id val);
object_id frame_local_get(frame_index_t i);

size_t frame_return_ptr();

void init_frame_stack(constant_pool_index_t entry_id);

void free_frame_stack();

typedef struct {
        frame_t * frame;
        size_t i;
} frame_stack_iterator;

bool is_end(frame_stack_iterator * f);
frame_stack_iterator next_frame_iterator(frame_stack_iterator * f);
object_id frame_stack_value(frame_stack_iterator * f);
