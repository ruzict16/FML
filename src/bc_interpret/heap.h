#pragma once

#include "runtime_types.h"
#include "types.h"

void init_heap(uint64_t heap_bytes, char * gclogs);
void free_heap();

object_id alloc_null();
object_id alloc_integer();
object_id alloc_boolean();
object_id alloc_array(arr_size_t n);
object_id alloc_object(obj_size_t class);
object_id alloc_function();

base_object_t * get_object_ptr(object_id id);

void garbage_collect();
void set_bit(object_id ptr, object_kind_t bit);
void unset_bit(object_id ptr, object_kind_t bit);
void warn_about_keep_bits();
