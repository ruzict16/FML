#include "magic_checker.h"

#include "common.h"
#include "debuglog.h"
#include "byte_helpers.h"

#define assert_condition(c, msg) if ((c) == false) { eprintf(#c " == false --> " msg "\n"); return 0; }

size_t check_magic(byte_t * bytes, size_t offset, size_t max_bytes) {
        size_t i = offset;
        assert_condition(max_bytes > 4, "File not long enough");
        assert_condition(inc_load(uint8_t, i, bytes) == (uint8_t)0x46, "F");
        assert_condition(inc_load(uint8_t, i, bytes) == (uint8_t)0x4D, "M");
        assert_condition(inc_load(uint8_t, i, bytes) == (uint8_t)0x4C, "L");
        assert_condition(inc_load(uint8_t, i, bytes) == (uint8_t)0x0A, "\\n");
        debug("::MAGIC:: header ok\n");
        return i;
}
