#pragma once

#include "common.h"

size_t check_magic(byte_t * bytes, size_t offset, size_t max_bytes);
