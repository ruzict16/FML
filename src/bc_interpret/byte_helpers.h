#pragma once

#include "common.h"

uint8_t fn_load_uint8_t(size_t offset, byte_t * buffer);
uint16_t fn_load_uint16_t(size_t offset, byte_t * buffer);
uint32_t fn_load_uint32_t(size_t offset, byte_t * buffer);

size_t fn_get_class_field_cnt(size_t class_offset, byte_t * buffer);


#define GLUE(x, y) x##y
#define inc_load(type,base_var,byte_arr) (base_var += sizeof(type), GLUE(fn_load_, type) (base_var - sizeof(type), byte_arr))
#define load(type,base_var,byte_arr) (GLUE(fn_load_, type) (base_var, byte_arr))

