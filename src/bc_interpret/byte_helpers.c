#include "byte_helpers.h"

uint8_t fn_load_uint8_t(size_t offset, byte_t * buffer) {
        return (uint8_t)(buffer[offset]);
}
uint16_t fn_load_uint16_t(size_t offset, byte_t * buffer) {
        return (uint16_t) (
                (buffer[offset]) |
                (buffer[offset + 1]) << 8
        );
}
uint32_t fn_load_uint32_t(size_t offset, byte_t * buffer) {
        return (uint32_t) (
                (buffer[offset]) |
                (buffer[offset + 1]) << 8 |
                (buffer[offset + 2]) << 16 |
                (buffer[offset + 3]) << 24
        );
}

size_t fn_get_class_field_cnt(size_t class_offset, byte_t * buffer) {
        return 0 | (uint16_t) (
                (buffer[class_offset + 1]) << 0 |
                (buffer[class_offset + 2]) << 8
        );
}
