#pragma once

#include "common.h"
#include "runtime_types.h"
#include "types.h"

size_t init_globals(byte_t * bytes, size_t i, size_t max_bytes);

void free_globals();


object_id get_global(constant_pool_index_t name);
void set_global(constant_pool_index_t name, object_id val);

size_t globals_count();
object_id get_global_by_id(size_t id);
