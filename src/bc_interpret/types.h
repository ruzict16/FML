#pragma once

#include "common.h"

typedef enum {
        const_ins = 0x01,
        arr_ins = 0x03,
        obj_ins = 0x04,
        getloc_ins = 0x0a,
        setloc_ins = 0x09,
        getglob_ins = 0x0c,
        setglob_ins = 0x0b,
        getfld_ins = 0x05,
        setfld_ins = 0x06,
        call_ins = 0x08,
        callfld_ins = 0x07,
        retn_ins = 0x0f,
        jmp_ins = 0x0e,
        jmpif_ins = 0x0d,
        print_ins = 0x02,
        drop_ins = 0x00,
} instruction_kind;

typedef enum {
        int_tag = 0x00,
        null_tag = 0x01,
        str_tag = 0x02,
        fn_tag = 0x03,
        bool_tag = 0x04,
        class_tag = 0x05,
        obj_tag = 0x06,
        arr_tag = 0x07,
} object_tag;

typedef uint16_t constant_pool_index_t;
typedef uint16_t constant_pool_size_t;
typedef uint16_t globals_size_t;
typedef uint16_t globals_index_t;
typedef uint8_t param_cnt_t;
typedef uint16_t local_cnt_t;
typedef uint32_t fun_len_t;
typedef size_t arr_size_t;
typedef uint16_t obj_size_t;
typedef int32_t int_val_t;
typedef bool bool_val_t;
typedef uint16_t frame_index_t;
typedef uint8_t opcode_t;
