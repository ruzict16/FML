#include "globals.h"
#include "bc_interpret/const_pool.h"
#include "runtime_types.h"
#include "types.h"
#include "byte_helpers.h"
#include "common.h"
#include "debuglog.h"

constant_pool_index_t * global_names = NULL;
object_id * globals = NULL;
size_t globals_cnt = 0;

size_t init_globals(byte_t * bytes, size_t i, size_t max_bytes) {
        bytes += i;
        max_bytes -= i;
        size_t cursor = i;
        i = 0;

        globals_size_t len = inc_load(uint16_t, i, bytes);

        #ifdef ENABLE_CHECKERS
                if ((i+(len * sizeof(constant_pool_index_t))) > max_bytes) {
                        eprintf("List of globals is reaching (%ld) further than expected (%ld)\n", i, max_bytes );
                        exit(13);
                }
        #else
                is_used(max_bytes);
        #endif

        global_names = malloc(sizeof(constant_pool_index_t) * len);
        globals = malloc(sizeof(object_id) * len);
        globals_cnt = len;

        globals_index_t glob_i;
        for(glob_i = 0; glob_i < globals_cnt; glob_i++) {
                globals[glob_i] = 0;
                global_names[glob_i] = inc_load(uint16_t, i, bytes);
        }

        return cursor + i;
}

void free_globals() {
        debug("::GLOBALS:: Freeing globals\n");
        free(global_names);
        free(globals);
        global_names = NULL;
        globals = NULL;
}

object_id get_global(constant_pool_index_t name) {
        debug("::GLOBALS:: get global %d\n", name);
        #ifdef ENABLE_CHECKERS
                if (constant_tag(name) != str_tag) {
                        eprintf("Attempted to access a global variable with something else than a string name\n");
                        exit(14);
                }
        #endif
        size_t i;
        for (i = 0; i < globals_cnt; i++) {
                if (global_names[i] == name) {
                        return globals[i];
                }
        }
        warn("::WARNING:: Global not found: %d\n", name);
        return 0;
}


void set_global(constant_pool_index_t name, object_id val) {
        debug("::GLOBALS:: set global %d\n", name);
        #ifdef ENABLE_CHECKERS
                if (constant_tag(name) != str_tag) {
                        eprintf("Attempted to access a global variable with something else than a string name\n");
                        exit(14);
                }
        #endif
        size_t i;
        for (i = 0; i < globals_cnt; i++) {
                if (global_names[i] == name) {
                        globals[i] = val;
                        return;
                }
        }
}

size_t globals_count() {
        return globals_cnt;
}
object_id get_global_by_id(size_t id) {
        if (globals_cnt <= id) {
                return 0;
        }
        return globals[id];
}
