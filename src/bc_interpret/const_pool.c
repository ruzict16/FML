#include "const_pool.h"

#include "bc_interpret/runtime_types.h"
#include "common.h"
#include "byte_helpers.h"
#include "debuglog.h"
#include "heap.h"
#include "types.h"
#include "builtin_functions.h"

byte_t * const_pool_bytes = NULL;
byte_t * raw_bytes = NULL;
size_t const_pool_bytes_begin = 0;
size_t * const_pool_lookup_table = NULL;
constant_pool_index_t const_pool_size = 0;





void init_const_pool_raw(size_t size) {
        const_pool_size = size;
        const_pool_lookup_table = malloc(size * sizeof(size_t));
}

void free_const_pool() {
        debug("::CONST_POOL:: Freeing const pool\n");
        free(const_pool_lookup_table);
        const_pool_lookup_table = NULL;
}

size_t const_pool_offset(size_t index) {
        return const_pool_lookup_table[index];
}


void print_const_pool() {
        constant_pool_index_t ii;
        constant_pool_index_t prev_byte_i = 0;
        for (ii = 0; ii < const_pool_size; ii++) {
                size_t byte_i = const_pool_lookup_table[ii];

                debug("::CONST_POOL:: \t%d == %ld: \t", ii, byte_i + const_pool_bytes_begin);
                prev_byte_i = byte_i;
                uint8_t tag = inc_load(uint8_t, byte_i, const_pool_bytes);
                switch (tag) {
                        break; case null_tag: {
                                debug("<nul> null");
                        }
                        break; case int_tag: {
                                debug("<int> ");
                                uint32_t int_value = inc_load(uint32_t, byte_i, const_pool_bytes);
                                debug("%d", int_value);
                                is_used(int_value);
                        }
                        break; case str_tag: {
                                debug("<str> ");
                                uint32_t str_len = inc_load(uint32_t, byte_i, const_pool_bytes);
                                char * str_value = (char *)&const_pool_bytes[byte_i];
                                debug("\"%.*s\"", str_len, str_value);
                                is_used(str_len);
                                is_used(str_value);
                                byte_i+=str_len;
                        }
                        break; case fn_tag: {
                                uint8_t params_cnt = inc_load(uint8_t, byte_i, const_pool_bytes);
                                uint16_t locals_cnt = inc_load(uint16_t, byte_i, const_pool_bytes);
                                uint32_t funbytes_cnt = inc_load(uint32_t, byte_i, const_pool_bytes);
                                debug("<fun> Function(params: %d, locals: %d, length: %d)",
                                        params_cnt, locals_cnt, funbytes_cnt);
                                is_used(locals_cnt);
                                is_used(params_cnt);
                                byte_i += funbytes_cnt;
                        }
                        break; case bool_tag: {
                                debug("<bul> ");
                                uint8_t bool_value = inc_load(uint8_t, byte_i, const_pool_bytes);
                                debug("%s", bool_value ? "true" : "false");
                                is_used(bool_value);
                        }
                        break; case class_tag: {
                                debug("<cls> Class(");
                                obj_size_t cls_len = inc_load(uint16_t, byte_i, const_pool_bytes);
                                size_t fld_i;
                                for (fld_i = 0; fld_i < cls_len; fld_i++) {
                                        constant_pool_index_t field = inc_load(uint16_t, byte_i, const_pool_bytes);
                                        debug("#%d, ", field);
                                        is_used(field);
                                }
                                debug(")");
                        }
                };
                debug(" \t(%ld)\n", byte_i - prev_byte_i);
                is_used(prev_byte_i);
        }
}

/// bytes have to be "alive" as long as you want to use the const_pool
size_t init_const_pool(byte_t * bytes, size_t i, size_t max_bytes) {
        raw_bytes = bytes;
        uint16_t constant_pool_size = inc_load(uint16_t, i, bytes);
        bytes += i;
        max_bytes -= i;
        size_t cursor = i;
        i = 0;
        init_const_pool_raw(constant_pool_size);
        const_pool_bytes = bytes;
        const_pool_bytes_begin = const_pool_bytes - raw_bytes;
        constant_pool_index_t pool_i;
        for (pool_i = 0; pool_i < constant_pool_size; pool_i++) {
                // Check max bytes
                {
                        #ifdef ENABLE_CHECKERS
                                if (max_bytes <= i) {
                                        eprintf("Attempted to read past the constant pool size limit during constant pool initialization\n");
                                        exit(2);
                                }
                        #else
                                is_used(max_bytes);
                        #endif
                }

                // debug("\t%d == %ld: \t", pool_i, i);
                const_pool_lookup_table[pool_i] = i;
                uint8_t tag = inc_load(uint8_t, i, bytes);
                switch (tag) {
                        break; case null_tag: {
                                // debug("<nul>");
                        }
                        break; case int_tag: {
                                // debug("<int>");
                                i += sizeof(uint32_t);
                        }
                        break; case str_tag: {
                                // debug("<str>");
                                uint32_t str_len = inc_load(uint32_t, i, bytes);
                                char * str_value = (char *)&const_pool_bytes[i];
                                check_if_builtin_name(pool_i, str_len, str_value);
                                i+=str_len;
                        }
                        break; case fn_tag: {
                                // debug("<fun>");
                                i += sizeof(uint8_t);
                                i += sizeof(uint16_t);
                                uint32_t funbytes_cnt = inc_load(uint32_t, i, bytes);
                                i += funbytes_cnt;
                        }
                        break; case bool_tag: {
                                // debug("<bul>");
                                i += sizeof(uint8_t);
                        }
                        break; case class_tag: {
                                // debug("<cls>");
                                uint16_t cls_len = inc_load(uint16_t, i, bytes);
                                i += sizeof(uint16_t) * cls_len;
                        }
                };
                // debug("\n");
        }
        // debug("----\n");
        print_const_pool();
        return cursor + i;
}
uint16_t get_class_field_count(constant_pool_index_t i) {
        size_t byte_i = const_pool_lookup_table[i];
        #ifdef ENABLE_CHECKERS
                uint8_t tag = inc_load(uint8_t, byte_i, const_pool_bytes);
                if (tag != class_tag) {
                        eprintf("Class (%d) expected at index %d, got: %d\n", class_tag, i, tag);
                        exit(1);
                }
        #else
                byte_i += sizeof(uint8_t);
        #endif
        return load(uint16_t, byte_i, const_pool_bytes);
}

constant_pool_index_t get_class_fieldname(constant_pool_index_t i, uint16_t field_index) {
        size_t byte_i = const_pool_lookup_table[i];
        #ifdef ENABLE_CHECKERS
                uint8_t tag = inc_load(uint8_t, byte_i, const_pool_bytes);
                if (tag != class_tag) {
                        eprintf("Class (%d) expected at index %d, got: %d\n", class_tag, i, tag);
                        exit(2);
                }
                uint16_t cls_len = inc_load(uint16_t, byte_i, const_pool_bytes);
                if (field_index >= cls_len) {
                        eprintf("Expected a class with more fields\n");
                        exit(9);
                }
        #else
                byte_i += sizeof(uint8_t);
                byte_i += sizeof(uint16_t);
        #endif

        constant_pool_index_t * arr = (constant_pool_index_t *)(const_pool_bytes + byte_i);
        return arr[field_index];
}

uint16_t get_class_field_index(constant_pool_index_t i, constant_pool_index_t field_name, bool * success) {
        size_t byte_i = const_pool_lookup_table[i];
        #ifdef ENABLE_CHECKERS
                uint8_t tag = inc_load(uint8_t, byte_i, const_pool_bytes);
                if (tag != class_tag) {
                        eprintf("Class (%d) expected at index %d, got: %d\n", class_tag, i, tag);
                        exit(3);
                }
        #else
                byte_i += sizeof(uint8_t);
        #endif
        uint16_t cls_len = inc_load(uint16_t, byte_i, const_pool_bytes);

        constant_pool_index_t * arr = (constant_pool_index_t *)(const_pool_bytes + byte_i);
        uint16_t ii;
        for (ii = 0; ii < cls_len; ii++) {
                if (arr[ii] == field_name) {
                        if (success != NULL) {
                                *success = true;
                        }
                        return ii;
                }
        }
        if (success != NULL) {
                *success = false;
                return 0;
        }
        else {
                eprintf("Object does not contain this field\n");
                exit(10);
        }
}

object_id instantiate_class(constant_pool_index_t i) {
        obj_size_t cls_len = get_class_field_count(i);

        object_id val = alloc_object(cls_len);
        ((obj_t *)get_object_ptr(val))->class = i;
        debug("::CONST_POOL:: _instantiate_class_ - class set as: %d\n", i);
        return val;
}

size_t get_function_start(constant_pool_index_t i) {
        size_t byte_i = const_pool_lookup_table[i];
        debug("::CONST_POOL:: byteoffset of function %d: %ld\n", i, byte_i);
        #ifdef ENABLE_CHECKERS
                uint8_t tag = load(uint8_t, byte_i, const_pool_bytes);
                if (tag != fn_tag) {
                        eprintf("Function expected\n");
                        exit(8);
                }
        #endif
        return byte_i + 8 + const_pool_bytes_begin;
}
object_id alloc_runtime_object_from_constant(constant_pool_index_t i) {
        debug("::CONST_POOL:: alloc_rtime_object\n");
        if (i >= const_pool_size) {
                eprintf("Overran const pool\n");
                exit(87);
        }
        size_t byte_i = const_pool_lookup_table[i];
        uint8_t tag = inc_load(uint8_t, byte_i, const_pool_bytes);
        switch (tag) {
                break; case null_tag: {
                        debug("HERE 1\n");
                        object_id val = alloc_null();
                        return val;
                }
                break; case int_tag: {
                        debug("HERE 2\n");
                        object_id val = alloc_integer();
                        ((int_t *)get_object_ptr(val))->value = load(uint32_t, byte_i, const_pool_bytes);
                        debug("Done 2\n");
                        return val;
                }
                break; case fn_tag: {
                        debug("HERE 3\n");
                        uint8_t params_cnt = inc_load(uint8_t, byte_i, const_pool_bytes);
                        uint16_t locals_cnt = inc_load(uint16_t, byte_i, const_pool_bytes);
                        uint32_t funbytes_cnt = inc_load(uint32_t, byte_i, const_pool_bytes);

                        object_id val = alloc_function();
                        ((fun_t *)get_object_ptr(val))->param_cnt = params_cnt;
                        ((fun_t *)get_object_ptr(val))->local_cnt = locals_cnt;
                        ((fun_t *)get_object_ptr(val))->instructions_byte_cnt = funbytes_cnt;
                        ((fun_t *)get_object_ptr(val))->start = byte_i + const_pool_bytes_begin;
                        return val;
                }
                break; case bool_tag: {
                        debug("HERE 4\n");
                        object_id val = alloc_boolean();
                        ((bool_t *)get_object_ptr(val))->value = inc_load(uint8_t, byte_i, const_pool_bytes);
                        return val;
                }
                #ifdef ENABLE_CHECKERS
                break; default:
                        eprintf("Expected null/int/function/bool\n");
                        exit(11);
                #endif
        };
        warn("::WARNING:: alloc_runtime_object_from_constant returning NULL");
        return 0;
}

size_t get_string_length(constant_pool_index_t i) {
        size_t byte_i = const_pool_lookup_table[i];
        #ifdef ENABLE_CHECKERS
        uint8_t tag = inc_load(uint8_t, byte_i, const_pool_bytes);
        if (tag != str_tag) {
                eprintf("Expected a string\n");
                exit(85);
        }
        #else
        byte_i += 1;
        #endif
        return (size_t) inc_load(uint32_t, byte_i, const_pool_bytes);
}

const char * get_string_data(constant_pool_index_t i) {
        size_t byte_i = const_pool_lookup_table[i];
        #ifdef ENABLE_CHECKERS
        uint8_t tag = inc_load(uint8_t, byte_i, const_pool_bytes);
        if (tag != str_tag) {
                eprintf("Expected a string\n");
                exit(85);
        }
        #else
        byte_i += 1;
        #endif
        byte_i += 4;
        return (const char *) (const_pool_bytes + byte_i);
}

local_cnt_t get_function_local_count(constant_pool_index_t i) {
        size_t byte_i = const_pool_lookup_table[i];
        #ifdef ENABLE_CHECKERS
                uint8_t tag = load(uint8_t, byte_i, const_pool_bytes);
                if (tag != fn_tag) {
                        eprintf("Function expected\n");
                        exit(8);
                }
        #endif
        return load(uint16_t, byte_i + 2, const_pool_bytes);
}

param_cnt_t get_function_param_count(constant_pool_index_t i) {
        size_t byte_i = const_pool_lookup_table[i];
        #ifdef ENABLE_CHECKERS
                uint8_t tag = load(uint8_t, byte_i, const_pool_bytes);
                if (tag != fn_tag) {
                        eprintf("Function expected\n");
                        exit(8);
                }
        #endif
        return load(uint8_t, byte_i + 1, const_pool_bytes);
}

object_tag constant_tag(constant_pool_index_t i) {
        size_t byte_i = const_pool_lookup_table[i];
        return load(uint8_t, byte_i, const_pool_bytes);
}
