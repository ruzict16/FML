#pragma once

#include "common.h"

#include "byte_helpers.h"

#include "runtime_types.h"

#include "types.h"

size_t                init_const_pool                    (byte_t * bytes,size_t begin_index, size_t max_bytes);
void                  free_const_pool                    ();


object_id             instantiate_class                  (constant_pool_index_t i);
object_id             alloc_runtime_object_from_constant (constant_pool_index_t i);

uint16_t              get_class_field_count              (constant_pool_index_t i);
constant_pool_index_t get_class_fieldname                (constant_pool_index_t i, uint16_t field_index);
uint16_t              get_class_field_index              (constant_pool_index_t i, constant_pool_index_t field_name, bool * success);

size_t                get_function_start                 (constant_pool_index_t i);
local_cnt_t           get_function_local_count           (constant_pool_index_t i);
param_cnt_t           get_function_param_count           (constant_pool_index_t i);

object_tag            constant_tag                       (constant_pool_index_t i);

size_t                get_string_length                  (constant_pool_index_t i);
const char *          get_string_data                    (constant_pool_index_t i);

void print_const_pool();
