#pragma once


#include "common.h"

#include "types.h"

int bc_interpret(const char * src, uint64_t heap_bytes, char * gclogs);

