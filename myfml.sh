#!/bin/bash

preprocess() {
        mkdir -p out/preprocessed
        cp $1 out/preprocessed/first_pass
        gcc -E -x c -P -w -include src/resources/import.pp $1 -o out/preprocessed/first_pass
        cat out/preprocessed/first_pass | sed 's/{/begin/' | sed 's/}/end/' > out/preprocessed/second_pass
        cp out/preprocessed/second_pass out/preprocessed/third_pass
        gcc -E -x c -P -w -include src/resources/import.pp out/preprocessed/second_pass -o out/preprocessed/third_pass
}

if [ "$1" == "reference" ]; then
        preprocess "$2"
        ./reference/fml run out/preprocessed/third_pass
elif [ "$1" == "run" ]; then
        preprocess "$2"
        ./out/fml run out/preprocessed/third_pass
elif [ "$1" == "preprocess" ]; then
        preprocess "$2"
        cat out/preprocessed/third_pass
else
        echo "Usage: ./fml.sh {run,reference,preprocess} <file_path>"
        exit 2
fi
