# NI-RUN

[GitLab Repo](https://gitlab.fit.cvut.cz/ruzict16/FML)

## Usage

### Build

- `make build`

### Test

- install `gitlab-runner`
- `make test`

or

- run gitlab pipeline `test`

### Run FML

- For reference implementation run
  - `./fml.sh reference path_to_FML_file`
- For my implementation run
  - `./fml.sh run path_to_FML_file`
- For getting raw FML run
  - `./fml.sh preprocess path_to_FML_file`

## Tasks

### Task 00

- in fml_examples directory there are FML files
- run `make task00` to see the output of the subtasks