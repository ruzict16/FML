.PHONY: all
all: build

out/meson-info: meson.build
	meson setup out --buildtype release

.PHONY: build_dbg
build_dbg:
	meson setup --wipe out --optimization=g
	meson compile -C out
	echo
	echo "Done compiling"
	echo

.PHONY: build
build: out/meson-info
	meson compile -C out
	echo
	echo "Done compiling"
	echo

.PHONY: dockerd_run
dockerd_run:
	systemctl start docker

.PHONY: dockerd_kill
dockerd_kill:
	systemctl stop docker

.PHONY: test
test:
	gitlab-runner exec docker --docker-volumes '/home/truzicka/School/RUN/FML/FMLtest:/FMLtest/output' --env CI_REGISTRY=gitlab.fit.cvut.cz:5000 test

.PHONY: bench_test
bench_test: build_dbg
	cp out/fml ./benchmarks/fml_local
	./benchmarks/bc_run_local.sh run --heap-size 100 --heap-log "./benchmarks/sudoku.csv" "./artifacts/tests/fml/sudoku.fml" > ./benchmarks/sudoku.out.txt 2> ./benchmarks/sudoku.err.txt

.PHONY: bench
bench: build
	cp out/fml ./benchmarks/fml_local
	./benchmarks/cfml bc_disassemble "./benchmarks/temporary/fml/fml.bc" > ./benchmarks/bc.txt
	./benchmarks/bc_run_local.sh run --heap-size 100 --heap-log "./benchmarks/sudoku.csv" "./artifacts/tests/fml/sudoku.fml" > ./benchmarks/sudoku.out.txt 2> ./benchmarks/sudoku.err.txt

.PHONY: example
example: build
	./out/fml bc_compile test_examples/fml/arrays_basic.fml > out.bc 2> out.err
	./reference/fml bc_disassemble out.bc > out.bc.txt
	./reference/fml bc_verify out.bc
	./reference/fml bc_interpret out.bc > out.run


.PHONY: clean
clean:
	rm -rf out

.PHONY: task00
task00:
	@echo
	@echo HelloWorld:
	@echo
	@./fml.sh reference fml_examples/helloworld.fml
	@echo
	@echo Triangle:
	@echo
	@./fml.sh reference fml_examples/tree.fml
	@echo
	@echo Fibonacci:
	@echo
	@./fml.sh reference fml_examples/fib_seq.fml
	@echo
	@echo Stack:
	@echo
	@./fml.sh reference fml_examples/stack.fml
	@echo
	@echo Roman numerals:
	@echo
	@./fml.sh reference fml_examples/roman.fml

.PHONY: unit
unit:
	meson test -C out --print-errorlogs

.PHONY: memcheck-unit
memcheck-unit:
	meson test -C out --wrap='valgrind -v --leak-check=full --show-leak-kinds=all --error-exitcode=1' --print-errorlogs