#!/bin/bash
# valgrind --leak-check=no -s $(readlink -f ./build/fml) "$@"

MYFML="$(readlink -f ./build/fml)"
REFML="$(readlink -f /cfml/build/fml)"

# cp $REFML /FMLtest/fml

TMPDIR="/tmp/fml/"

mkdir -p "$TMPDIR"

ARGS=( "$@" )
POSARGS=()
OPTARGS=()

for (( i=0; i < ${#ARGS[@]}; i++ ))
do
        if [[ "${ARGS[$i]}" == "--"* ]]; then
                OPTARGS+=( "${ARGS[$i]}" )
                (( i++ ))
                OPTARGS+=( "${ARGS[$i]}" )
                continue
        fi
        POSARGS+=( "${ARGS[$i]}" )
done

if [ "${POSARGS[0]}" == "reference" ]; then
        "$REFML" run "${POSARGS[@]:1}" "${OPTARGS[@]}"
elif [ "${POSARGS[0]}" == "ast_interpret" ]; then
        # "$REFML" ast_interpret "${POSARGS[@]:1}" "${OPTARGS[@]}"
        "$MYFML" ast_interpret "${POSARGS[@]:1}" "${OPTARGS[@]}"
elif [ "${POSARGS[0]}" == "bc_interpret" ]; then
        # "$REFML" bc_interpret "${POSARGS[@]:1}" "${OPTARGS[@]}"
        "$MYFML" bc_interpret "${POSARGS[@]:1}" "${OPTARGS[@]}"
        # valgrind --track-origins=yes "$MYFML" bc_interpret "${POSARGS[@]:1}" "${OPTARGS[@]}"
elif [ "${POSARGS[0]}" == "bc_compile" ]; then
        # "$REFML" bc_compile "${POSARGS[@]:1}" "${OPTARGS[@]}"
        "$MYFML" bc_compile "${POSARGS[@]:1}" "${OPTARGS[@]}"
elif [ "${POSARGS[0]}" == "run" ]; then
        "$REFML" bc_compile "${POSARGS[@]:1}" "${OPTARGS[@]}" > "$TMPDIR"/fml.bc
        # "$MYFML" bc_compile "${POSARGS[@]:1}" "${OPTARGS[@]}" > "$TMPDIR"/fml.bc
        "$REFML" bc_interpret "$TMPDIR"/fml.bc "${OPTARGS[@]}"
        # "$MYFML" bc_interpret "$TMPDIR"/fml.bc "${OPTARGS[@]}"
else
        echo Unknown subcommand "$1"
        echo "Usage: $0 {reference,ast_interpret,bc_interpret,bc_compile,run} <file_path> [<option> <value> ...]"
        exit 2
fi
